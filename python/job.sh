#!/bin/bash
 
#SBATCH --partition=maxwell
#SBATCH --time=7:00:00                 # Maximum time request
#SBATCH --nodes=1                       # Number of nodes
#SBATCH --workdir   /home/livelyke/
#SBATCH --job-name  icwf-40
#SBATCH --output    icwf-40-%N-%j.out  # File to which STDOUT will be written
#SBATCH --error     icwf-40-%N-%j.err  # File to which STDERR will be written
#SBATCH --mail-type END                 # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user kevin.lively01@gmail.com  # Email to which notifications will be sent

python3 /home/livelyke/src/run.py
