import numpy as np
import matplotlib.pyplot as plt


gs_t = np.load('./psi-t-.05/gs(t).npy')
ex_t = np.load('./psi-t-.05/ex(t).npy')

time = np.arange(0,50.005,.005*50)

#Filter out DC mode
gs_t = gs_t - np.average(gs_t)
ex_t = ex_t - np.average(ex_t)
freq = np.fft.fftshift(np.fft.fftfreq(len(time),.005*50))
ft_gs = np.fft.fftshift(np.fft.fft(gs_t))
ft_ex = np.fft.fftshift(np.fft.fft(ex_t))

#plt.plot(time,gs_t)
#plt.plot(time,ex_t)
plt.plot(freq,np.abs(ft_ex))
plt.show()
#print(freq)
