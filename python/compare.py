import os
import glob
from subprocess import call
import numpy as np
import matplotlib.pyplot as plt
import moviepy.editor as mpy 

two_e=False
one_e=False
load1=True
load2=False
loadpsi = True
density=False
wavefunction=False
wavefunction_recon=False
rel_err=False
int_err=True
anim = False
icwf=True
direc_2e = './dump/2e/'
direc_1e = './dump/1e/'
N = [40]#,1600]
#direcs = ['./backup/CWF/nuclear-splitting/1e-'+str(N[i]) +'/' for i in range(len(N)) ]
direcs = ['./dump/1e/']
direc_psi = './sym-psi-t-form-sin2-A-0.35-tf-laser-116.43540008755943-tf-tot-250-nu-0.1288268/'
#direc_psi = './sym-psi-t-form-sin2-A-0.35-tf-laser-103.06090899721735-tf-tot-250-nu-0.145545/'
#direc_psi = './dump/1e-icwf-comp/'
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#########################CHANGE THESE PARAMS TO MATCH THE README IN DIREC############
e_spacing = .4
n_spacing = .08
box_l = 18
box_R = 3.2
r1x = [x for x in np.arange(-box_l/2.,box_l/2.,e_spacing)]
r2x = [x for x in np.arange(-box_l/2. - e_spacing/2, box_l/2.- e_spacing/2, e_spacing)]
Rx = [x for x in np.arange(.3,3.5,n_spacing)]
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(two_e==True):
    if(load2):
        time1 = []
        #dircon = os.listdir('./dump/2e-comp')
        dircon = os.listdir(direc_2e)
        for item in dircon:
            if (item[:2]=='2e' and item[-4:]  == '.npy'):
                time1.append(float(item[11:-4]))
        time1.sort()
        cwf_R = [0 for i in range(len(time1))]
        cwf_2e = [0 for i in range(len(time1))]
        for item in dircon:
            if (item[:2]=='R-' and item[-4:] =='.npy'):
                time1_index = time1.index(float(item[10:-4]))
                R_wf = np.load(direc_2e+item)
                cwf_R[time1_index] = R_wf
            if (item[:2]=='2e' and item[-4:] =='.npy'):
                time1_index = time1.index(float(item[11:-4]))
                #cwf_2e[time1_index] = (np.load('./dump/2e-comp/'+item))
                cwf_2e[time1_index] = (np.load(direc_2e+item))
 
        #np.save('./dump/2e-comp/400-loaded-2e',cwf_2e)
        #np.save('./dump/2e-comp/400-loaded-R',cwf_R)
        np.save(direc_2e+'200-loaded-2e',cwf_2e)
        np.save(direc_2e+'200-loaded-R',cwf_R)

if(loadpsi==True):
    dircon = os.listdir(direc_psi)
    psi_t = []
    time3 = []
    for item in dircon:
        if (item[:4]=='psi-' and item[-4:]  == '.npy'):
            time3.append(float(item[4:-4]))
    time3.sort()
    print(time3)
    psi_t = [0 for x in range(len(time3))]
    for item in dircon:
        if (item[:4]=='psi-' and item[-4:]=='.npy'):
            time3_index = time3.index(float(item[4:-4]))
            psi_t[time3_index] = (np.load(direc_psi+item))
    #np.save('./dump/2e-comp/200-loaded-psi',psi_t)
    np.save(direc_psi+'loaded-RK4-psi',psi_t)

def load_psi(direc):
    time = []
    dircon = os.listdir(direc)
    for item in dircon:
        if (item[:4]=='psi-' and item[-4:]  == '.npy'):
            time.append(float(item[4:-4]))
        time.sort()
    psi_t = [0 for x in range(len(time))]
    for item in dircon:
        if (item[:4]=='psi-' and item[-4:]=='.npy'):
            time_index = time.index(float(item[4:-4]))
            psi_t[time_index] = (np.load(direc+item))
    return time, psi_t


        
if(one_e==True):
    if(load1):
        time = []
        dircon = os.listdir(direc_1e)
        for item in dircon:
            if (item[:2]=='e1' and item[-4:]  == '.npy'):
                time.append(float(item[7:-4]))
        time.sort()
        cwf_R = [0 for i in range(len(time))]
        cwf_e1 = [0 for i in range(len(time))]
        cwf_e2 = [0 for i in range(len(time))]
        ansatz = [0 for i in range(len(time))]
        for item in dircon:
            if (item[:2]=='R-' and item[-4:] =='.npy'):
                time_index = time.index(float(item[6:-4]))
                cwf_R[time_index] = (np.load(direc_1e+item))
            if (item[:2]=='e1' and item[-4:] =='.npy'):
                time_index = time.index(float(item[7:-4]))
                cwf_e1[time_index] = (np.load(direc_1e+item))
            if (item[:2]=='e2' and item[-4:] =='.npy'):
                time_index = time.index(float(item[7:-4]))
                cwf_e2[time_index] = (np.load(direc_1e+item))
            if (item[:2]=='an' and item[-4:] == '.npy'):
                time_index = time.index(float(item[6:-4]))
                ansatz[time_index] = np.load(direc_1e+item)
                
 
        np.save(direc_1e+'40-loaded-e1',cwf_e1)
        np.save(direc_1e+ '40-loaded-e2',cwf_e2)
        np.save(direc_1e+'40-loaded-R',cwf_R)
        np.save(direc_1e+'40-loaded-ansatz',ansatz)




#@jit(nogil=True)
def cwf_2e_density(two_e_wfs):
    rho_e = np.zeros(two_e_wfs.shape[-1])
    for i in range(two_e_wfs.shape[0]):
        rho_e += .5*np.sum(np.abs(two_e_wfs[i,:,:])**2,0)*e_spacing \
                 + .5*np.sum(np.abs(two_e_wfs[i,:,:])**2,1)*e_spacing
    return 2*rho_e/(np.sum(rho_e)*e_spacing)

#@jit(nogil=True)
def cwf_electronic_density(psi_e1,psi_e2):
    rho_e = np.zeros(psi_e1.shape[-1])
    for i in range(psi_e1.shape[0]):
        rho_e += np.abs(psi_e1[i])**2 #+ np.abs(psi_e2[i])**2
    return rho_e/(np.sum(rho_e)*e_spacing)

#@jit(nogil = True)
def one_electron_density(psi):
    psi2 = np.abs(psi)**2
    #out = np.sum(np.sum(psi2,0),1)
    out = np.zeros(psi.shape[0])
    #for r1i in range(psi.shape[0]):
    #    out[r1i] = np.sum(np.sum(psi2[r1i,:,:],0))
    #for r2i in range(psi.shape[0]):
    #    out[r2i] += np.sum(np.sum(psi2[:,r2i,:],0))
    #return out*e_spacing*n_spacing
    return np.sum(np.sum(psi2,2),1)*e_spacing*n_spacing


#@jit(parallel=True,nogil=True)
def cwf_nuclear_density(psi_n):
    rho_R = np.zeros(psi_n.shape[-1])
    for i in range(psi_n.shape[0]):
        rho_R += np.abs(psi_n[i])**2
    return rho_R/(np.sum(rho_R)*n_spacing)

#@jit(nogil=True)
def integrate_over_electrons(psi):
    return e_spacing**2*sum(sum(np.abs(psi)**2))

def find_index(L, val):
    if (type(L)=='list' and (type(val)==int or type(val) ==float or type(val) ==np.float64)):
        return L.index(min(L, key=lambda x:abs(x-val)))
    if (type(L)==np.ndarray and (type(val)==int or type(val) ==float or type(val) ==np.float64)):
        return int(np.abs(L-val).argmin())
    elif (type(L)==list and ((type(val)==np.ndarray) or (type(val)==list))):
        a = [0 for x in range(len(val))]
        for i in range(len(val)):
            a[i] = int((L.index(min(L, key=lambda x:abs(x-val[i])))))
        return a
    elif (type(L)==np.ndarray and ((type(val)==np.ndarray) or (type(val)==list))):
        a = [0 for x in range(len(val))]
        for i in range(len(val)):
            a[i] = int(np.abs(L - val[i]).argmin())
        return a

#psi = np.load(direc+'loaded-RK4-psi.npy')
psi = np.load(direc_psi+'loaded-RK4-psi.npy')
if(two_e==True):
    cwf_2e_1 = np.load(direc_2e+'loaded-2e.npy')
    cwf_R_1 = np.load(direc_2e+'loaded-R.npy')
    #cwf_2e_10 = np.load(direc+'10-smaller-loaded-2e.npy')
    #cwf_R_10 = np.load(direc+'10-smaller-loaded-R.npy')
    #cwf_2e_1600 = np.load(direc+'1600-loaded-2e.npy')
    #cwf_R_1600 = np.load(direc+'1600-loaded-R.npy')
    if(density==True):
        error1_e = []
        error1_R= []
        error10_e = []
        error10_R= []
        error1600_e = []
        error1600_R= []
        for i in range(psi.shape[0]):
            print(i)
            rho_cwf_1 = cwf_2e_density(cwf_2e_1[i])
            R_rho_cwf_1 = cwf_nuclear_density(cwf_R_1[i])
            #rho_cwf_10 = cwf_2e_density(cwf_2e_10[i])
            #R_rho_cwf_10 = cwf_nuclear_density(cwf_R_10[i])
            #rho_cwf_1600 = cwf_2e_density(cwf_2e_1600[i])
            #R_rho_cwf_1600 = cwf_nuclear_density(cwf_R_1600[i])
            R_rho_psi = integrate_over_electrons(psi[i])
            rho_psi = one_electron_density(psi[i])
            diff_1_e = np.abs(rho_cwf_1 - rho_psi)
            if(rel_err==True):
                maxdiff_1_e = diff_1_e.max()
                mdiff_1_e_index = find_index(diff_1_e,maxdiff_1_e)
                maxdiff_1_e = maxdiff_1_e/rho_psi[mdiff_1_e_index]
                diff1_R = np.abs(R_rho_cwf_1 - R_rho_psi)
                maxdiff1_R = diff1_R.max()
                mdiff_index1_R = find_index(diff1_R,maxdiff1_R)
                maxdiff1_R = maxdiff1_R/R_rho_psi[mdiff_index1_R]
                if(maxdiff1_R > 10):
                    plt.plot(Rx,R_rho_cwf_1,'.')
                    plt.plot(Rx,R_rho_psi,'.')
                    plt.show()
                error1_e.append((maxdiff_1_e))
                error1_R.append(maxdiff1_R)
            if(int_err==True):
                error1_e.append(np.sum(np.abs(rho_cwf_1 - rho_psi))*e_spacing)
                error1_R.append(np.sum(np.abs(R_rho_cwf_1 - R_rho_psi))*n_spacing)
            #diff_10_e = np.abs(rho_cwf_10 - rho_psi)
            #maxdiff_10_e = diff_10_e.max()
            #mdiff_10_e_index = find_index(diff_10_e,maxdiff_10_e)
            #maxdiff_10_e = maxdiff_10_e/rho_psi[mdiff_10_e_index]
            #diff10_R = np.abs(R_rho_cwf_10 - R_rho_psi)
            #maxdiff10_R = diff10_R.max()
            #mdiff_index10_R = find_index(diff10_R,maxdiff10_R)
            #maxdiff10_R = maxdiff10_R/R_rho_psi[mdiff_index10_R]
            #error10_e.append((maxdiff_10_e))
            #error10_R.append(maxdiff10_R)
            #if(i ==498):
            #    plt.plot(R_rho_cwf_1, label='1 R rho')
                #plt.plot(R_rho_cwf_10, label='10 R rho')
            #    plt.plot(R_rho_psi, label='R exact')
                #plt.plot(rho_cwf_10, label='10 rho')
                #plt.plot(rho_cwf_1, label='1 rho')
                #plt.plot(rho_psi, label='rho exact')
            #    plt.legend()
            #    plt.show()
        plt.plot(np.arange(.5,250.5,.5),np.array(error1_e), label=r'2e wf $\rho_e$')
        plt.plot(np.arange(.5,250.5,.5),np.array(error1_R), label=r'2e wf $\rho_R$')
        #plt.plot(np.array(error10_e), label=r'10 $\rho_e$')
        #plt.plot(np.array(error10_R), label=r'10 $\rho_R$')
        plt.legend()
        if(one_e==False):
            plt.ylabel(r'max($|\rho_{cwf} - \rho_{Exact}|$)/$\rho_{Exact}|_{max difference}$')
            plt.xlabel('Time steps of .001')
            plt.show()
    if(wavefunction==True):
        if(wavefunction_recon==True):
            cwf_1_wf_t = []
            #cwf_10_wf_t = []
            #cwf_1600_wf_t = []
            for i in range(psi.shape[0]):
                cwf_1_wf = np.zeros(psi[0].shape) +0j
                #cwf_10_wf = np.zeros(psi[0].shape) +0j
                #cwf_1600_wf = np.zeros(psi[0].shape) +0j
                for Ri in range(cwf_R_1[0].shape[-1]):
                    for t in range(1):
                        cwf_1_wf[:,:,Ri] += cwf_2e_1[i,t,:,:]*cwf_R_1[i,t,Ri]
                    #for t in range(10):
                    #    cwf_10_wf[:,:,Ri] += cwf_2e_10[i,t,:,:]*cwf_R_10[i,t,Ri]
                    #for t in range(1600):
                    #    cwf_1600_wf[:,:,Ri] += cwf_2e_1600[i,t,:,:]*cwf_R_1600[i,t,Ri]
                #cwf_1_wf /=1
                #cwf_10_wf /=10
                cwf_1_wf = cwf_1_wf/np.sqrt((np.sum(np.sum(np.sum(np.abs(cwf_1_wf)**2)))*e_spacing**2*n_spacing))
                #cwf_10_wf = cwf_10_wf/np.sqrt((np.sum(np.sum(np.sum(np.abs(cwf_10_wf)**2)))*e_spacing**2*n_spacing))
                #cwf_1600_wf = cwf_1600_wf/np.sqrt((np.sum(np.sum(np.sum(np.abs(cwf_1600_wf)**2)))*e_spacing**2*n_spacing))
                cwf_1_wf_t.append(cwf_1_wf)
                #cwf_10_wf_t.append(cwf_10_wf)
                #cwf_1600_wf_t.append(cwf_1600_wf)
                print(i)
            np.save(direc+'200-recon-psi',cwf_1_wf_t)
            #np.save(direc+'10-recon-psi',cwf_10_wf_t)
            #np.save(direc+'1600-recon-psi',cwf_1600_wf_t)
        error1 = []
        error10 = []
        #error1600 = []
        cwf_1_wf = np.load(direc+'200-recon-psi.npy')
        #cwf_10_wf = np.load(direc+'10-recon-psi.npy')
        #cwf_1600_wf = np.load(direc+'1600-recon-psi.npy')
        for i in range(psi.shape[0]):
            print(i)
            diff_1 = np.abs(np.abs(psi[i]) - np.abs(cwf_1_wf[i]))
            #diff_10 = np.abs(np.abs(psi[i]) - np.abs(cwf_10_wf[i]))
            #diff_1600 = np.abs(np.abs(psi[i]) - np.abs(cwf_1600_wf[i]))
            mdiff_1 = diff_1.max()
            #mdiff_10 = diff_10.max()
            #mdiff_1600 = diff_1600.max()
            #print(np.abs(np.average(cwf_10_wf[i])))
            #print(np.abs(np.average(psi[i])))
            error1.append(mdiff_1/np.abs(psi[i])[np.unravel_index(np.argmax(diff_1),psi[i].shape)])
            #error10.append(mdiff_10/np.abs(psi[i])[np.unravel_index(np.argmax(diff_10),psi[i].shape)])
            #error1600.append(mdiff_1600/np.abs(psi[i])[np.unravel_index(np.argmax(diff_1600),psi[i].shape)])
            #print('Min psi = ', np.abs(psi[i]).min())
            #print('psi at 1 maxdiff = ', np.abs(psi[i])[np.unravel_index(np.argmax(diff_1),psi[i].shape)])
            #print('psi at 10 maxdiff = ', np.abs(psi[i])[np.unravel_index(np.argmax(diff_10),psi[i].shape)])
            #print('psi at 1600 maxdiff = ', np.abs(psi[i])[np.unravel_index(np.argmax(diff_1600),psi[i].shape)])
            #plt.pcolormesh(r1x,r2x,np.abs(psi[i,:,:,0]))
            #plt.show()
            #plt.pcolormesh(r1x,r2x,np.abs(cwf_10_wf[i,:,:,0]))
            #plt.show()
            if(error1[-1] > 10):
                print(np.abs(psi[i])[np.unravel_index(np.argmax(diff_1),psi[i].shape)])
        #print(np.average(error1))
        #print(np.average(error10))
        plt.plot(np.array(error1), label='N=1, avg='+"{:.2f}".format(np.average(error1)))
        #plt.plot(np.array(error10), label='N=10, avg='+"{:.2f}".format(100*np.average(error10)))
        #plt.plot(np.arange(0,100,.5),100*np.array(error1600), label='N=1600, avg='+"{:.2f}".format(100*np.average(error1600)))
        #plt.plot(np.arange(0,1,.5),np.array(time)-np.array(time2),'.')
        plt.legend()
        #plt.title(r'Laser hit simulation, Percent error of maximum magnitude difference between $\Psi_{exact}$ and $\Psi_{reconstructed}$')
        plt.xlabel('Saved Time steps, in .5 a.u. increments from simulation with dt=.01')
        plt.ylabel(r'max($|\Psi_{Exact}| - |\Psi_{reconstructed}|$)/$|\Psi_{Exact}|_{At\ Numerator}$')
        plt.show()

time = np.arange(.5,250.5,.5)
cwf_sets = [None for x in N]
if(one_e==True):
    for i in range(len(N)):
        cwf_sets[i] = [np.load(direcs[i]+str(N[i])+'-loaded-e1.npy'), \
                       np.load(direcs[i]+str(N[i])+'-loaded-e2.npy'), \
                       np.load(direcs[i]+str(N[i])+'-loaded-R.npy')   ]
        if(icwf==True):
            ansatz = (np.load(direcs[i]+str(N[i])+'-loaded-ansatz.npy'))
 
    errors_R = [([]) for n in N]
    errors_el = [([]) for n in N]
    rho_Rs = [([]) for n in N]
    rho_es = [([]) for n in N]
    rho_R_psi = []
    images = []
    for i in range(psi.shape[0]):
        for n in range(len(N)):
            rho_Rs[n].append(cwf_nuclear_density(cwf_sets[n][2][i]))
            rho_es[n].append(cwf_electronic_density(cwf_sets[n][0][i],0))

        #rho_cwf_1 = cwf_electronic_density(cwf_e1_1[i],cwf_e2_1[i])
        #R_rho_psi = integrate_over_electrons(psi[i])
        #rho_psi = one_electron_density(psi[i])
        if(anim==True):
           # for n in range(len(N)):
           #     plt.plot(Rx,rho_Rs[n][-1],label=r'N='+str(N[n])+r'$\rho_R^{cwf}$')
            #plt.plot(Rx,R_rho_psi, label=r'$\rho_R^{exact}$')
            if(icwf==True):
                if (i%50==0):
                    print(np.sqrt(np.sum(np.sum(np.sum(np.abs(ansatz[i])**2)))*e_spacing**2*n_spacing))
                #plt.plot(Rx,integrate_over_electrons(ansatz_1[i]/(np.sqrt(np.sum(np.sum(np.sum(np.abs(ansatz_1[i])**2)))*e_spacing**2*n_spacing))), label=r'$\rho_R^{ansatz}$')
                    plt.plot(Rx,integrate_over_electrons(ansatz[i]), label=r'$\rho_R^{ansatz}$')
                    plt.xlabel('Nuclear Separation, R [a.u]')
                    plt.title(r'HCWF $\rho_R$, Time '+str(time[i]))
                    plt.legend()
                    plt.savefig('./compare-pngs/'+str(time[i])+'.png')
            #plt.show()
                    plt.clf()
        if(rel_err==True):
            diff_1_e = np.abs(rho_cwf_1 - rho_psi)
            maxdiff_1_e = diff_1_e.max()
            mdiff_1_e_index = find_index(diff_1_e,maxdiff_1_e)
            maxdiff_1_e = maxdiff_1_e/rho_psi[mdiff_1_e_index]
            diff1_R = np.abs(R_rho_cwf_1 - R_rho_psi)
            maxdiff1_R = diff1_R.max()
            mdiff_index1_R = find_index(diff1_R,maxdiff1_R)
            maxdiff1_R = maxdiff1_R/R_rho_psi[mdiff_index1_R]
            error1_e.append((maxdiff_1_e))
            error1_R.append(maxdiff1_R)
        if(int_err==True):
            for n in range(len(N)):
                errors_el[n].append(np.sum(np.abs(rho_es[n][-1] - rho_psi))*e_spacing)
                errors_R[n].append(np.sum(np.abs(rho_Rs[n][-1] - R_rho_psi))*n_spacing)
    plt.plot(errors_el[0])
    plt.plot(errors_R[0])
    if(int_err==True and two_e==False):
        plt.title('Density Integrated Error for One Electron CWF')
        plt.ylabel(r'$\int dx \left| \rho_{exact}(x) - \rho_{exact}(x)\right|$')
        plt.xlabel('Timesteps of .001')
        plt.legend()
        plt.show()
    if(rel_err==True):
        plt.title(r'Density Max relative error for one and two electron cwf at $N_{traj} = 200$')
        plt.ylabel(r'Max Relative Error')
        plt.xlabel('Timesteps of .001')
        plt.legend()
        plt.show()
    if(anim==True):
        gif_name = 'outputName'
        fps =12
        #checkdir = call(['ls', direc_1e+'pngs/'])
        #if(checkdir!=0)`:
        #    raise 'Check png directory'
        os.chdir('./compare-pngs/')
        file_list = glob.glob('*.png') # Get all the pngs in the current directory
        #list.sort(file_list, key=lambda x: int(x.split('_')[1].split('.png')[0])) # Sort the images by #, this may need to be tweaked for your use case
        list.sort(file_list,key=lambda x: float(x.split('.p')[0]))
        #with open('image_list.txt', 'w') as file:
        #    for item in file_list:
        #        file.write("%s\n" % item)
        #os.system('convert -verbose @image_list.txt {}.gif'.format(gif_name)) # On windows convert is 'magick'
        clip = mpy.ImageSequenceClip(file_list, fps=fps)
        clip.write_gif('{}.gif'.format(gif_name), fps=fps)

