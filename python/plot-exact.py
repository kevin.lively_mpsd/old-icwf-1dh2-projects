#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 16:49:56 2018

@author: livelyke
"""

import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import moviepy.editor as mpy 
import matplotlib.gridspec as gridspec

electrons = True

e_spacing = .4
n_spacing = .08
box_l = 18
box_R = 3.8
r1x = [x for x in np.arange(-box_l/2.,box_l/2.,e_spacing)]
r2x = [x for x in np.arange(-box_l/2. - e_spacing/2, box_l/2.- e_spacing/2, e_spacing)]
Rx = [x for x in np.arange(.3,3.5,n_spacing)]
#@jit(nogil=True)
def integrate_over_electrons(psi):
    return e_spacing**2*sum(sum(np.abs(psi)**2))

time = []
#direc = './backup/CWF/static-evolution/11Dec/data/'
direc = './exact-splitting/coarse/sym-psi-t-form-sin2-A-0.01-tf-laser-103.060908997-tf-tot-300-nu-0.145545/'
dircon = os.listdir(direc)

for item in dircon:
    if (item[:4]=='psi-' and item[-4:]  == '.npy'):
        
        time.append(float(item[4:-4]))
        time.sort()

for t in time:
    print(t)
    psi = np.load(direc + 'psi-'+"{:.1f}".format(t)+'.npy')
    pos_e1 = np.load(direc+'pos_e1-'+"{:.1f}".format(t)+'.npy')
    pos_e2 = np.load(direc+'pos_e2-'+"{:.1f}".format(t)+'.npy')
    pos_R = np.load(direc+'pos_R-'+"{:.1f}".format(t)+'.npy')
    #rho_R = integrate_over_electrons(psi[i])
    rho_R = integrate_over_electrons(psi)

    gs = gridspec.GridSpec(3,2)
    plt.figure(figsize=(6,6),dpi=200)
    ax = plt.subplot(gs[0,:])
    #plt.plot(Rx, cwf_nuclear_density(cwf_R), label=r'\rho_R^{icwf}')
    plt.plot(Rx,rho_R, label=r'$\rho_R^{exact}$')
    plt.title(r'ICWF $\rho_R$, Time '+str(t)+', N='+str(len(pos_e1)))
    plt.legend()
    ax = plt.subplot(gs[1,0])
    ax = plt.subplot(gs[1,1])
    plt.pcolormesh(r1x,r2x,np.sum(np.abs(psi)**2,2)*n_spacing)
    plt.xlabel('r2x')
    plt.ylabel('r1x')
    plt.title(r'$\rho^{exact}(r_1,r_2)$')
    ax = plt.subplot(gs[2,0])
    plt.plot(pos_e1, '.',label=r'$r_1^{\alpha}$ position')
    plt.plot(pos_e2, '.',label=r'$r_2^{\alpha}$ position')
    plt.plot(r1x[0]*np.ones(len(pos_e1)), 'k')
    plt.plot(r1x[-1]*np.ones(len(pos_e1)),'k')
    plt.legend(loc='upper right')
    ax = plt.subplot(gs[2,1])
    plt.plot(pos_R, '.',label=r'$R^{\alpha}$ position')
    plt.plot(Rx[0]*np.ones(len(pos_R)),'k')
    plt.plot(Rx[-1]*np.ones(len(pos_R)),'k')
    plt.legend(loc='upper right')
    #fig.suptitle('Trajectory positions')
    plt.savefig('./exact-pngs/'+str(t)+'.png')
    plt.clf()
    
gif_name = 'outputName'
fps =12
os.chdir('./exact-pngs/')
file_list = glob.glob('*.png') # Get all the pngs in the current directory
#list.sort(file_list, key=lambda x: int(x.split('_')[1].split('.png')[0])) # Sort the images by #, this may need to be tweaked for your use case
list.sort(file_list,key=lambda x: float(x.split('.p')[0]))
clip = mpy.ImageSequenceClip(file_list, fps=fps)
clip.write_gif('{}.gif'.format(gif_name), fps=fps)
