from numba import njit, prange,jit
import numpy as np
import time
import cwf
from importlib import reload
import fileinput
import params as p
reload(p)
import matplotlib.pyplot as plt
from functions import integrate_over_electrons


class Timer:
    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self,*args):
        self.stop = time.time()
        self.interval = self.stop - self.start

@jit(parallel=True, nogil=True)
def sum_complex():
    a = np.zeros((100,100))+0j
    b = np.zeros((100,100))+0j
    #sum1 = np.sum(a,0)
    for i in prange(0,100):
        for j in prange(0,100):
            a[i,j] = i+1j*j
            sum1 = np.sum(a,0)
            b[i,j] += np.sum(sum1)
    return b
psi = np.ones((100,100,100))+2j
V = np.ones((100,100,100))
@jit(nopython=True,parallel=True)
def product(psi,V):
    return psi*V

def timing(psi, E_form):
    timer = []
    for N in range(10,200,50):
        with fileinput.FileInput('./params.py', inplace=True,backup='.bak') as file:
                for line in file:
                        print(line.replace('num_trajs = '+str(N-10), 'num_trajs = '+str(N)),end='')
        reload(cwf)
        e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,el,R,C = cwf.initialize_test(psi,N)
        with Timer() as t:
                el1,R1,pos_e11,pos_e21,pos_R1,C1 = cwf.T_dep_RK4_2e_ICWF(el,R,e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,C,1000,E_form)
        with Timer() as tt:
                el1,R1,pos_e11,pos_e21,pos_R1,C1 = cwf.T_dep_RK4_2e_ICWF(el,R,e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,C,1000,E_form)
        timer.append(tt.interval)
        print("{:.2f}".format(100*N/200) + '% done !!!!!!!!!!!!!!!!')
    return timer
 
def timing_1e(psi, E_form):
    timer = []
    for N in range(10,200,50):
        with fileinput.FileInput('./params.py', inplace=True,backup='.bak') as file:
                for line in file:
                        print(line.replace('num_trajs = '+str(N-50), 'num_trajs = '+str(N)),end='')
        reload(cwf)
        e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,el1,el2,R,C = cwf.initialize_1e_test(psi,N)
        with Timer() as t:
                el11,el21,R1,pos_e11,pos_e21,pos_R1,C1 = cwf.T_dep_RK4_1e_ICWF(el1,el2,R,e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,1000,E_form,C)
        with Timer() as tt:
                el11,el21,R1,pos_e11,pos_e21,pos_R1,C1 = cwf.T_dep_RK4_1e_ICWF(el1,el2,R,e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,1000,E_form,C)
        timer.append(tt.interval)
        print("{:.2f}".format(100*N/200) + '% done !!!!!!!!!!!!!!!!')
    return timer

def overlay_mesh(psi,e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R):
    for i in range(len(p.Rx)):
        indices = [j for j,x in enumerate(R_mesh) if x==i]
        if len(indices)>0:
            fig = plt.figure()
            ax1 = plt.subplot(211)
            ax2 = plt.subplot(212)
            ax1.pcolormesh(p.r1x, p.r2x, np.abs(psi[:,:,i])**2)
            #plt.scatter(pos_e1[indices],pos_e2[indices],color='red')
            ax2.hist2d(pos_e1[indices], pos_e2[indices], bins=len(pos_e1[indices]))
            ax1.get_shared_x_axes().join(ax1,ax2)
            ax1.get_shared_y_axes().join(ax1,ax2)
            ax2.autoscale()
            plt.title('Rx = '+"{:.3f}".format(p.Rx[i]) + ', with '+str(len(indices))+' Nuclear samples')
            plt.show() 


#@jit(nogil=True)
def Cdot_timing(psi):
    timer = []
    step = 30
    for N in range(10,500,step):
        with fileinput.FileInput('./params.py', inplace=True,backup='.bak') as file:
                for line in file:
                        print(line.replace('num_trajs = '+str(N), 'num_trajs = '+str(N+step)),end='')
        reload(cwf)
        e1_mesh,e2_mesh,R_mesh,pos_e1,pos_e2,pos_R,el1,el2,R,C = cwf.initialize_1e_test(psi,N+step)
        with Timer() as t:
            dummy_var = cwf.C_dot_1e_t(C,el1,el2,R,cwf.V1[:,e2_mesh,R_mesh],cwf.V2[e1_mesh,:,R_mesh], cwf.VR[e1_mesh,e2_mesh,:],0,cwf.epos[:,e2_mesh],cwf.epos[e1_mesh,:])
        with Timer() as tt:
            dummy_var = cwf.C_dot_1e_t(C,el1,el2,R,cwf.V1[:,e2_mesh,R_mesh],cwf.V2[e1_mesh,:,R_mesh], cwf.VR[e1_mesh,e2_mesh,:],0,cwf.epos[:,e2_mesh],cwf.epos[e1_mesh,:])
        timer.append(tt.interval)
        print("{:.2f}".format(100*N/200) + '% done !!!!!!!!!!!!!!!!')
    return timer

@jit(nopython=True, nogil=True)
def MM(el1,el2,R):
    M_e1 = np.dot(np.conj(el1),el1.transpose())
    M_e2 = np.dot(np.conj(el2),el2.transpose())
    M_R = np.dot(np.conj(R),R.transpose())
    M = M_e1*M_e2*M_R
    M *= p.n_spacing*p.e_spacing**2
    return M,M_e1,M_e2,M_R

@jit(nopython=True, nogil=True)
def WW(el1,el2,R,Vlong):
    flat_cwf = cwf.flatten_cwf(el1,el2,R)
    W = np.dot(np.conj(flat_cwf),(Vlong*flat_cwf).transpose())
    W*=p.n_spacing*p.e_spacing**2
    return W


@jit(nopython=True, nogil=True)
def WRR(R,V,M_e1,M_e2):
    WR = np.dot(np.conj(R),(R*V).transpose())*M_e1*M_e2
    WR *=p.e_spacing**2*p.n_spacing
    return WR

@jit(nopython=True,nogil=True)
def inv(M):
    return np.linalg.pinv(M)
@jit(nopython=True,nogil=True)
def dot(Minv,W,WR,C):
    return np.dot(Minv,np.dot(W-WR-WR-WR,C))
def C_timing(el1,el2,R,Vlong,V,C):
    time_M = []
    time_W = []
    time_W1 = []
    time_inv = []
    time_dot = []
    for i in range(20):
        with Timer() as tm:
            M,M_e1,M_e2,M_R = MM(el1,el2,R)
        time_M.append(tm.interval)
        with Timer() as tw:
            W = WW(el1,el2,R,Vlong)
        time_W.append(tw.interval)
        with Timer() as tw1:
            WR = WRR(R,V,M_e1,M_e2)
        time_W1.append(tw1.interval)
        with Timer() as tinv:
            Minv = inv(M)
        time_inv.append(tinv.interval)
        with Timer() as tdot:
            dummy = dot(Minv,W,WR,C)
        time_dot.append(tdot.interval)

    print('Average time to construct M, ', np.average(time_M))
    print('Average time to construct W, ', np.average(time_W))
    print('Average time to construct WR, ', np.average(time_W1))
    print('Average time to invert M, ', np.average(time_inv))
    print('Average time to dot invM with WC, ', np.average(time_dot))

#@jit(nopython=True,nogil=True)
def compare_full(el1,el2,R):
    time1 = []
    for i in range(20):
        with Timer() as t:
            flat_cwf = cwf.flatten_cwf(el1,el2,R)
            W_1 = np.dot(np.conj(flat_cwf),(cwf.V_flat2*flat_cwf).transpose())
            W_1*=p.n_spacing*p.e_spacing**2
        time1.append(t.interval)

    M_e1 = np.dot(np.conj(el1),el1.transpose())
    M_e2 = np.dot(np.conj(el2),el2.transpose())
    M_R = np.dot(np.conj(R),R.transpose())
    time2 = []
    for j in range(20):
        with Timer() as t:
            W_2 = M_e1*M_e2*np.dot(np.conj(R),(R*cwf.Rinv_flat).transpose())
            for i in range(el1.shape[0]):
                W_2[i,:] += M_e1[i,:]*np.sum(np.tile(np.conj(el2[i]),(p.num_trajs,1))*el2*np.dot(np.tile(np.conj(R[i]),(p.num_trajs,1))*R,cwf.Ve2R.transpose()),1)
                W_2[i,:] += M_e2[i,:]*np.sum(np.tile(np.conj(el1[i]),(p.num_trajs,1))*el1*np.dot(np.tile(np.conj(R[i]),(p.num_trajs,1))*R,cwf.Ve1R.transpose()),1)
                W_2[i,:] += M_R[i,:]*np.sum(np.tile(np.conj(el1[i]),(p.num_trajs,1))*el1*np.dot(np.tile(np.conj(el2[i]),(p.num_trajs,1))*el2,cwf.Vee.transpose()),1)
            W_2 *= p.n_spacing*p.e_spacing**2
        time2.append(t.interval)

    return np.average(time1), np.average(time2)
