import numpy as np
from numba import jit
from matplotlib import pyplot as plt
from params import *
from functions import *
import cluster_gpu_cwf as cwf

tau = .0001
tol = .00001
plotDensity = False
psi0 = np.load('./gs_wavefunctions/sym-SD-.00001/psi-gs.npy')
cwf.ICWF_propagate(tf_tot,tf_laser,new_mesh=True, GPU=True)
"""
cwf.ICWF_restart_propagate(tf_tot,tf_laser,form,29.0,np.load('./dump/1e/e1-cwf-29.000000.npy'),
    np.load('./dump/1e/e2-cwf-29.000000.npy'),
    np.load('./dump/1e/R-cwf-29.000000.npy'),
    np.load('./dump/1e/pos_e1-29.000000.npy'),
    np.load('./dump/1e/pos_e2-29.000000.npy'),
    np.load('./dump/1e/pos_R-29.000000.npy'),
    np.load('./dump/1e/e1_mesh-29.000000.npy'),
    np.load('./dump/1e/e2_mesh-29.000000.npy'),
    np.load('./dump/1e/R_mesh-29.000000.npy'),
    np.load('./dump/1e/C-29.000000.npy') )
    
with Timer() as t:
    T_dep_RK4_routine(psi0,tf_tot,tf_laser,dt, Amplitude,form,False,None)
print(str(int(tf_tot/dt)) + ' steps took ', t.interval)
#psi_t_file = './psi-t-form-ramp-A-0.2-tf-laser-300-tf-tot-600-nu-0.1406/'
#psi_t = np.load(psi_t_file + '/psi-164.50999999999536.npy')
#with Timer() as t:
#    Time_propagate(psi_t,dt,tf_tot,tf_laser,order,plotDensity, BOs, 164.51, psi_t_file)
#print(str(int(tf_tot/dt)) + ' steps took ', t.interval)

#New gs wavefunction generation routines.
with Timer() as t:
    BOe, BOs = sd_BO(tol,1)
print('convergance across ', len(Rx), ' different nucelar positions took ', t.interval)
#cg_BO(tol)
BOe = np.array(BOe)
BOs = np.array(BOs)
np.save('./gs_wavefunctions/sym-huge-fine-SD-.0001/BO_energies', BOe)
np.save('./gs_wavefunctions/sym-huge-fine-SD-.0001/BO_states', BOs)

E_tot, psi_tot = SD(.0001, False, None)
np.save('./gs_wavefunctions/sym-huge-fine-SD-.0001/E_tot', E_tot)
np.save('./gs_wavefunctions/sym-huge-fine-SD-.0001/psi-gs', psi_tot)
"""
