from numba import jit, prange
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
import params as p


M1 = 1836
M2 = M1

e_spacing = 1.3
n_spacing = 0.0925
dim_e = 101
dim_R = 201
dt = 0.08
num_trajs = 1500

r1x = np.arange(-65,65+e_spacing,e_spacing)
Rx = np.arange(-9.25,9.25+.99*n_spacing,n_spacing)

#{{{

@jit(nogil=True)
def find_index(L, val):
    a = np.zeros(val.shape,dtype=int)
    for i in range(val.shape[0]):
        a[i] = int(round(np.abs(L - val[i]).argmin()))
    return a
@jit(nogil=True)
def initialize_mesh_mc(psi0, threshold):
    pdf = np.ravel(np.abs(psi0)**2)/np.sqrt(np.sum(np.abs(psi0)**2)*e_spacing**2*n_spacing)
    pdf[pdf<threshold] = (1e-12)*max(pdf)
    cdf = np.cumsum(pdf)
    e1_mesh = []
    e2_mesh = []
    R_mesh = []
    pos_e1 = []
    pos_e2 = []
    pos_R = []
    for t in range(num_trajs):
        rand = np.random.rand()
        #f.find_index requires an array input and returns one, so take the array's single value
        full_index = find_index(cdf,np.array([rand]))[0]
        #r1,r2 and R_dim are defined below, and will be part of the global namespace within cwf in any script where
        #   cwf is imported.
        unraveled_index = np.unravel_index(full_index,(dim_e,dim_e,dim_R))
        e1_mesh.append(unraveled_index[0])
        e2_mesh.append(unraveled_index[1])
        R_mesh.append(unraveled_index[2])
        #now initialize the positions corresponding to the mesh coordinates through a random uniform sampling 
        #   around the position the mesh corresponds to
        #Do this through choosing a uniform random number and interpolating it's value onto the range
        #   spanning the left and right midpoint length of the position   |<--*-->|<--*-->| where * are the positions
        #   of the mesh points.
        rand = np.random.rand()
        pos_e1.append((rand-.5)*p.e_spacing + r1x[unraveled_index[0]])
        rand = np.random.rand()
        pos_e2.append((rand-.5)*p.e_spacing + r1x[unraveled_index[1]])
        rand = np.random.rand()
        pos_R.append((rand-.5)*p.n_spacing + Rx[unraveled_index[2]])

    return np.array(e1_mesh),np.array(e2_mesh), np.array(R_mesh), np.array(pos_e1),np.array(pos_e2),np.array(pos_R)

@jit( nogil=True)
def initialize_C_1e(psi, psi_el1, psi_el2, psi_R, num_trajs):
    G = np.zeros(num_trajs,dtype=np.complex128)
    C = np.zeros(num_trajs,dtype=np.complex128)
    M = (np.dot(np.conj(psi_el1), psi_el1.transpose()))\
        *(np.dot(np.conj(psi_el2), psi_el2.transpose()))\
        *(np.dot(np.conj(psi_R), psi_R.transpose()))\
        *n_spacing*e_spacing**2
    for i in range(num_trajs):
        integral = 0j
        for Ri in range(R_dim):
            for r2i in range(r2_dim):
                integral += np.dot(np.conj(psi_el1[i])*np.conj(psi_el2[i,r2i])*np.conj(psi_R[i,Ri]),psi[:,r2i,Ri])

        G[i] = integral
    G *= p.n_spacing*p.e_spacing**2
    C = np.linalg.pinv(M).dot(G)
    #C = np.dot(np.linalg.pinv(M),G)
    return C

@jit(nogil=True)
def ICWF_1e_vel(C,psi_el1,psi_el2,psi_R, e1_mesh, e2_mesh, R_mesh, num_trajs):
    el1_num = np.zeros(num_trajs,dtype=np.complex128)
    el2_num = np.zeros(num_trajs,dtype=np.complex128)
    #tester = np.zeros(r1_dim,dtype=np.complex128)
    R_num = np.zeros(num_trajs,dtype=np.complex128)
    denom = np.zeros(num_trajs,dtype=np.complex128)
    grad_el1 = np.zeros((num_trajs,r1_dim),dtype=np.complex128)
    grad_el2 = np.zeros((num_trajs,r2_dim),dtype=np.complex128)
    grad_Rs = np.zeros((num_trajs,R_dim),dtype=np.complex128)
    for i in range(num_trajs):
        grad_el1[i] = grad_e1.dot(psi_el1[i,:])
        grad_el2[i] = grad_e2.dot(psi_el2[i,:])
        grad_Rs[i] = grad_R.dot(psi_R[i,:])
    for i in range(num_trajs):
        el1_num[i] = np.dot(C,psi_el2[:,e2_mesh[i]]*psi_R[:,R_mesh[i]]*grad_el1[:,e1_mesh[i]])
        el2_num[i] = np.dot(C,psi_el1[:,e1_mesh[i]]*psi_R[:,R_mesh[i]]*grad_el2[:,e2_mesh[i]])
        R_num[i] = np.dot(C,psi_el1[:,e1_mesh[i]]*psi_el2[:,e2_mesh[i]]*grad_Rs[:,R_mesh[i]])
        denom[i] = np.dot(C,psi_el1[:,e1_mesh[i]]*psi_el2[:,e2_mesh[i]]*psi_R[:,R_mesh[i]])
    #for r in range(r1_dim):
    #    tester[r] = np.dot(C,psi_el2[:,e2_mesh[0]]*psi_R[:,R_mesh[0]]*grad_el1[:,r])/denom[0]
    #print('el1_vel[0] = ', (el1_num[0]/denom[0]).imag)
    #plt.plot(tester.imag)
    #plt.title('ICWF')
    #plt.show()
    return (el1_num/(denom)).imag, (el2_num/(denom)).imag, (R_num/(p.mu*denom)).imag
#}}}
Lap_e = np.zeros((len(r1x), len(r1x)))
grad_e = np.zeros((len(r1x), len(r1x)))

Lap_n = np.zeros((len(Rx),len(Rx)))
grad_n = np.zeros((len(Rx),len(Rx)))
#{{{
for i in range(0,len(r1x)):
    for j in range(0,len(r1x)):
        if i==j:
            grad_e[i,j] = 0
            Lap_e[i,j] = -73766.0/25200
        if j==i+1:
            grad_e[i,j] = 5.0/6.0 
            Lap_e[i,j] = 5.0/3
        if j==i-1:
            grad_e[i,j] = -5.0/6.0
            Lap_e[i,j] = 5.0/3
        if j==i+2:
            grad_e[i,j] = -5.0/21
            Lap_e[i,j] = -5.0/21
        if j==i-2:
            grad_e[i,j] = 5.0/21
            Lap_e[i,j] = -5.0/21
        if j==i+3:
            grad_e[i,j] = 5.0/84
            Lap_e[i,j] = 5.0/126
        if j==i-3:
            grad_e[i,j] = -5.0/84
            Lap_e[i,j] = 5.0/126
        if j==i+4:
            grad_e[i,j] = -5.0/504
            Lap_e[i,j] = -5.0/1008
        if j==i-4:
            grad_e[i,j] = 5.0/504
            Lap_e[i,j] = -5.0/1008
        if j==i+5:
            grad_e[i,j] = 1.0/1260
            Lap_e[i,j] = 1.0/3150
        if j==i-5:
            grad_e[i,j] = -1.0/1260
            Lap_e[i,j] = 1.0/3150
for i in range(0,len(Rx)):
    for j in range(0,len(Rx)):
        if i==j:
            grad_n[i,j] = 0
            Lap_n[i,j] = -73766.0/25200
        if j==i+1:
            grad_n[i,j] = 5.0/6.0 
            Lap_n[i,j] = 5.0/3
        if j==i-1:
            grad_n[i,j] = -5.0/6.0
            Lap_n[i,j] = 5.0/3
        if j==i+2:
            grad_n[i,j] = -5.0/21
            Lap_n[i,j] = -5.0/21
        if j==i-2:
            grad_n[i,j] = 5.0/21
            Lap_n[i,j] = -5.0/21
        if j==i+3:
            grad_n[i,j] = 5.0/84
            Lap_n[i,j] = 5.0/126
        if j==i-3:
            grad_n[i,j] = -5.0/84
            Lap_n[i,j] = 5.0/126
        if j==i+4:
            grad_n[i,j] = -5.0/504
            Lap_n[i,j] = -5.0/1008
        if j==i-4:
            grad_n[i,j] = 5.0/504
            Lap_n[i,j] = -5.0/1008
        if j==i+5:
            grad_n[i,j] = 1.0/1260
            Lap_n[i,j] = 1.0/3150
        if j==i-5:
            grad_n[i,j] = -1.0/1260
            Lap_n[i,j] = 1.0/3150
#}}}
grad_e = csr_matrix(grad_e/e_spacing)
grad_n = csr_matrix(grad_n/n_spacing)

#Complete calculation of the lapalacian and divide by twice the mass, multiplying by -1
T_e = csr_matrix((-1/(2*e_spacing**2))*Lap_e )
T_n = csr_matrix((-1/(2*n_spacing**2))*Lap_n)

#define the hamiltonian
def error_function_over_r(x,rc):
    val = np.erf(x/rc)/x
    scalar = False
    try:
        dummy = len(x)
    except:
        scalar = True
    if(scalar==False):
        indices = np.where(x==0)[0]
        val[indices] = 2/(np.sqrt(np.pi) * rc) - 2*x[indices]**2/(3*(np.sqrt(np.pi)*rc**3)) \
                   + 2*x[indices]**4/(10*np.sqrt(np.pi)*rc**5) - 2*x[indices]**6/(42*np.sqrt(np.pi)*rc**7)
    else:
        if(x==0):
            val = 2/(np.sqrt(np.pi) * rc) - 2**2/(3*(np.sqrt(np.pi)*rc**3)) \
                   + 2*x**4/(10*np.sqrt(np.pi)*rc**5) - 2*x**6/(42*np.sqrt(np.pi)*rc**7)
    return val
R_c = 5
R_c_fixed_l = 3.1
R_c_fixed_r = 4
R_0 = -9.5
R_2 = 9.5

n_u0 = -4.5
n_dispersion = 1/np.sqrt(2.85)
n_cte0 = np.sqrt(np.sqrt(2)/n_dispersion*np.sqrt(np.pi))

phi_n = n_cte0*np.exp(-(Rx-n_u0)**2/n_dispersion**2)
new=False
if(new==True):
    phi_el = np.loadtxt('/home/livelyke/Documents/Projects/MPSD/ICWF/code/3D-Shin-matlab/BO_eigst_gr.txt')

    phi_ini = phi_el*np.tile(phi_n,dim_e**2)
    phi_ini = phi_ini/np.sqrt(np.sum(np.abs(phi_ini)**2)*e_spacing**2*n_spacing)
    psi0 = phi_ini.reshape(dim_e,dim_e,dim_R)
    np.save('sm-psi0',psi0)
else:
    psi0 = np.load('sm-psi0.npy')

#e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R = initialize_mesh_mc(psi0, p.threshold)
e1_mesh = np.loadtxt('/home/livelyke/Documents/Projects/MPSD/ICWF/code/3D-Shin-matlab/e1_mesh.txt')
e2_mesh = np.loadtxt('/home/livelyke/Documents/Projects/MPSD/ICWF/code/3D-Shin-matlab/e2_mesh.txt')
R_mesh = np.loadtxt('/home/livelyke/Documents/Projects/MPSD/ICWF/code/3D-Shin-matlab/n_mesh.txt')

e1_mesh = e1_mesh.astype(int)
e2_mesh = e2_mesh.astype(int)
R_mesh = R_mesh.astype(int)

el1 = (psi0[:, e2_mesh,R_mesh]/np.sqrt(np.sum(np.abs(psi0[:,e2_mesh,R_mesh])**2)*e_spacing)).transpose()
el2 = psi0[e1_mesh, :,R_mesh]/np.sqrt(np.sum(np.abs(psi0[e1_mesh, :,R_mesh])**2)*e_spacing)
R = psi0[e1_mesh, e2_mesh, :]/np.sqrt(np.sum(np.abs(psi0[e1_mesh, e2_mesh, :])**2)*n_spacing)

#C = initialize_C_1e(psi0,el1,el2,R, num_trajs)

Vn1n0 = 1./np.abs(Rx - R_0);
Vn1n2 = 1./np.abs(Rx - R_2);
Ve1n0 = error_function_over_r(np.abs(r1x - R_0),R_c_fixed_l);
Ve1n2 = error_function_over_r(np.abs(r1x - R_2),R_c_fixed_r);

Veext = -Ve1n0 - Ve1n2
Vnext = Vn1n0 + Vn1n2

[e_grid, R_grid] = np.meshgrid(r1x,Rx)
Ven = -error_function_over_r(np.abs(e_grid, R_grid), R_c)

Vee = np.zeros((dim_e,dim_e))
for ii in range(dim_e):
    for jj in range(dim_e):
        Vee[ii,jj] = error_function_over_r(np.abs(r1x[ii] - Rx[jj]),R_c)

VH = 0.5*np.dot(Vee,(np.abs(el1)**2+np.abs(el2)**2).transpose())*e_spacing
VH = VH.transpose()
#@jit(nogil=True)
#def cwf_potentials(e1_mesh,e2_mesh,R_mesh):
#    return Ven[

@jit(nogil=True)
def c_Ht(psi_e1,psi_e2, psi_R, pos_e1, pos_e2,pos_R, E_t, V1s, V2s, VRs,num_trajs):
    dt_psi_R = np.zeros((num_trajs, R_dim),dtype=np.complex128) 
    dt_psi_e1 = np.zeros((num_trajs, r1_dim),dtype=np.complex128) 
    dt_psi_e2 = np.zeros((num_trajs, r2_dim),dtype=np.complex128) 
    #If there is a mass imbalence between the atoms there is an additional term
    for i in prange(num_trajs):
        dt_psi_e1[i] = T_e.dot(psi_e1[i]) + V1s[i]*psi_e1[i] 
        dt_psi_e2[i] = T_e.dot(psi_e2[i]) + V2s[i]*psi_e2[i] 
        dt_psi_R[i] = T_n.dot(psi_R[i]) + VRs[i]*psi_R[i]
    return -1.0j*dt_psi_e1, -1.0j*dt_psi_e2, -1.0j*dt_psi_R



"""
Ve1n1 = np.zeros((dim_R,dim_e,dim_e))
Ve2n1 = np.zeros((dim_R,dim_e,dim_e))
Ve1e2 = np.zeros((dim_R,dim_e,dim_e))

for kk in range(dim_e):
    for ii in range(dim_e):
        Ve1e2[:,ii,kk] = error_function_over_r(np.abs(r1x[kk] - r1x[ii]),R_c)
        #for jj in range(dim_R):
        Ve1n1[:,ii,kk] = error_function_over_r(np.abs(r1x[ii] - Rx[:]),R_c)
        Ve2n1[:,ii,kk] = error_function_over_r(np.abs(r1x[kk] - Rx[:]),R_c)

Ve1n1 = csr_matrix(np.ravel(Ve1n1).reshape(dim_e**2*dim_R,dim_e**2*dim_R))
Ve2n1 = spdiags(Ve2n1(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc)
Ve1e2 = spdiags(Ve1e2(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc)
Vn1n0 = kron(e_unit_nx,kron(e_unit_nx,sparse(diag(Vn1n0))))
Vn1n2 = kron(e_unit_nx,kron(e_unit_nx,sparse(diag(Vn1n2))))
Ve2n0 = kron(e_unit_nx,kron(sparse(diag(Ve1n0)),n_unit_nx))
Ve2n2 = kron(e_unit_nx,kron(sparse(diag(Ve1n2)),n_unit_nx))
Ve1n0 = kron(sparse(diag(Ve1n0)),kron(e_unit_nx,n_unit_nx))
Ve1n2 = kron(sparse(diag(Ve1n2)),kron(e_unit_nx,n_unit_nx)) 
#Ve1n1 = spdiags(Ve1n1(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
#Ve2n1 = spdiags(Ve2n1(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
#Ve1e2 = spdiags(Ve1e2(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
"""
