import pyopencl as cl
import pyopencl.array as cl_array
from cwf import C_dot_1e, Rinv, Ve1R, Ve2R, Vee, restart_from_time, mu_e, T_e1
from test import Timer
import numpy as np
from matplotlib import pyplot as plt
import params as p
from numba import jit

kernel64 = """//CL//
//Order the cwf matrices in Row major,
//i.e.   R[i,x] = R_device[i*N + x]   where i is the trajectory and x is the position index
//each
//2 component vector to hold the real and imaginary parts of a complex number:
#define PYOPENCL_DEFINE_CDOUBLE
#include <pyopencl-complex.h>

//This kernel should be called as a square matrix of workgroups
// Each workgroup consists of r1_dim workitems (or whichever species has the largest space)

//This one works perfectly for float, taking only CWF and full interactions 
//Takes Full W and populates 
__kernel void Cdot(const __global cdouble_t* el1, const __global cdouble_t *el2,  
        const __global cdouble_t *R, const __global double *Ve1R, const __global double *Ve2R, 
        const __global double *Vee, const __global double *Rinv, \
        const int dimr1, const int  dimr2, const int dimR, 
        __global cdouble_t *W, const int N, const __global int *glob_index_x, const __global int *glob_index_y)
{
        const int Wi = get_group_id(0); 
        const int Wx = glob_index_x[Wi];
        const int Wy = glob_index_y[Wi];
        //const int Wy = (get_group_id(0) - Wx)/N;
        const int thread = get_local_id(0);
        const int size = get_local_size(0);

        __private cdouble_t sum, sum1, sum2;
        sum.real = 0.0;
        sum.imag = 0.0;
        sum1.real = 0.0;
        sum1.real = 0.0;
        sum2.imag = 0.0;
        sum2.imag = 0.0;
       
        //Each thread computes one entry in the VeiR RR mat-vec
        __private cdouble_t Rint1, Rint2, eint;
        Rint1.real = 0.0;
        Rint1.imag = 0.0;
        Rint2.real = 0.0;
        Rint2.imag = 0.0;
        eint.real = 0.0;
        eint.imag = 0.0;


        //Local product arrays of species CWFs multiplied by complex conjugates
        __local cdouble_t Rloc[152];
        __local cdouble_t el1loc[150]; 
        __local cdouble_t el2loc[150];//[dimr2]
        __local cdouble_t R1buff[150];//[dimr1]
        __local cdouble_t R2buff[150];//[dimr2]
        __local cdouble_t R3buff[152];//[dimR]
        __local cdouble_t ebuff[150];//[dimr1]

        if(thread < dimr1)
        {
            el1loc[thread] = cdouble_mul(el1[Wx*dimr1 + thread], cdouble_conj(el1[Wy*dimr1+thread]));
        }
        if(thread < dimr2)
        {
            el2loc[thread] = cdouble_mul(el2[Wx*dimr2 + thread], cdouble_conj(el2[Wy*dimr2+thread]));
        }
        if(thread < dimR)
        {
            Rloc[thread] =  cdouble_mul(R[Wx*dimR + thread], cdouble_conj(R[Wy*dimR+thread]));
        } 

        barrier(CLK_LOCAL_MEM_FENCE);

        //Ve1R is row major Ve1R[e1x,Rx] = Ve1R[e1x*dimR + Rx]
        for (int i=0; i < size; i++)
        {
            if(i<dimR)
            {
                Rint1 = cdouble_add(Rint1, cdouble_mulr(Rloc[i], Ve1R[thread*dimR + i]));
                Rint2 = cdouble_add(Rint2, cdouble_mulr(Rloc[i], Ve2R[thread*dimR + i]));
            }
            if(i<dimr2)
            {
                eint = cdouble_add(eint, cdouble_mulr(el2loc[i], Vee[thread*dimr1+i]));
            }
        }

        //Have each thread load it's portion of the vec-vec product into the buffers
        if(thread < dimr1)
        {
            R1buff[thread] = cdouble_mul(Rint1, el1loc[thread]);
            R2buff[thread] = cdouble_mul(Rint2, el2loc[thread]);
            ebuff[thread] = cdouble_mul(eint, el1loc[thread]);
        }
        if(thread < dimR)
        {
            R3buff[thread] = cdouble_mulr(Rloc[thread], Rinv[thread]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        //Have the 0th thread calc the vec-vec product
        //Relies on r1dim==r2dim
        if(thread == 0)
        {
            __private cdouble_t m1, m2, mr, rint;
            m1.real = 0.0;
            m1.imag = 0.0;
            m2.real = 0.0;
            m2.imag = 0.0;
            mr.real = 0.0;
            mr.imag = 0.0;
            rint.real = 0.0;
            rint.imag = 0.0;

            for(int i=0; i < size; i++)
            {  
                if(i<dimr1)
                {
                    sum = cdouble_add(sum, R2buff[i]);
                    sum1 = cdouble_add(sum1, R1buff[i]);
                    sum2 = cdouble_add(sum2, ebuff[i]);
                    m1 = cdouble_add(m1, el1loc[i]);
                    m2 = cdouble_add(m2, el2loc[i]);
                }
                if(i<dimR)
                {
                    mr = cdouble_add(mr, Rloc[i]);
                    rint = cdouble_add(rint, R3buff[i]);
                }
            }
            //__private const int gi = glob_index[Wi];
            sum1  = cdouble_mul(sum1, m2);
            sum2 = cdouble_mul(sum2, mr);
            sum = cdouble_add(cdouble_mul(sum, m1),  cdouble_mul(cdouble_mul(m1,m2),rint));

            //W is passed as a flattened upper triangular and will be restored out of kernel
            //W[Wi] = cdouble_add(cdouble_add(sum, sum1), sum2);
            //This is for a full W
            W[Wy*N+Wx] = cdouble_add(cdouble_add(sum, sum1), sum2);
            W[Wx*N+Wy] = cdouble_conj(cdouble_add(cdouble_add(sum, sum1), sum2));
            
        }
           
}

"""

kernel32 = """//CL//
#include <pyopencl-complex.h>

//This one works perfectly, taking only CWF and full interactions 
//Takes Full W and populates 
__kernel void Cdot(const __global cfloat_t* el1, const __global cfloat_t *el2,  
        const __global cfloat_t *R, const __global float *Ve1R, const __global float *Ve2R, 
        const __global float *Vee, const __global float *Rinv, 
        const int dimr1, const int  dimr2, const int dimR, 
        __global cfloat_t *W, __global cfloat_t *M, const int N, 
        const __global int *glob_index_x, const __global int *glob_index_y)
{
        const int Wi = get_group_id(0); 
        const int Wx = glob_index_x[Wi];
        const int Wy = glob_index_y[Wi];
        //const int Wy = (get_group_id(0) - Wx)/N;
        const int thread = get_local_id(0);
        const int size = get_local_size(0);

        __private cfloat_t sum, sum1, sum2;
        sum.real = 0.0;
        sum.imag = 0.0;
        sum1.real = 0.0;
        sum1.real = 0.0;
        sum2.imag = 0.0;
        sum2.imag = 0.0;
       
        //Each thread computes one entry in the VeiR RR mat-vec
        __private cfloat_t Rint1, Rint2, eint;
        Rint1.real = 0.0;
        Rint1.imag = 0.0;
        Rint2.real = 0.0;
        Rint2.imag = 0.0;
        eint.real = 0.0;
        eint.imag = 0.0;


        //Local product arrays of species CWFs multiplied by complex conjugates
        __local cfloat_t Rloc[80];
        __local cfloat_t el1loc[90]; 
        __local cfloat_t el2loc[90];//[dimr2]
        __local cfloat_t R1buff[90];//[dimr1]
        __local cfloat_t R2buff[90];//[dimr2]
        __local cfloat_t R3buff[80];//[dimR]
        __local cfloat_t ebuff[90];//[dimr1]

        if(thread < dimr1)
        {
            el1loc[thread] = cfloat_mul(el1[Wx*dimr1 + thread], cfloat_conj(el1[Wy*dimr1+thread]));
        }
        if(thread < dimr2)
        {
            el2loc[thread] = cfloat_mul(el2[Wx*dimr2 + thread], cfloat_conj(el2[Wy*dimr2+thread]));
        }
        if(thread < dimR)
        {
            Rloc[thread] =  cfloat_mul(R[Wx*dimR + thread], cfloat_conj(R[Wy*dimR+thread]));
        } 

        barrier(CLK_LOCAL_MEM_FENCE);

        //Ve1R is row major Ve1R[e1x,Rx] = Ve1R[e1x*dimR + Rx]
        for (int i=0; i < size; i++)
        {
            if(i<dimR)
            {
                Rint1 = cfloat_add(Rint1, cfloat_mulr(Rloc[i], Ve1R[thread*dimR + i]));
                Rint2 = cfloat_add(Rint2, cfloat_mulr(Rloc[i], Ve2R[thread*dimR + i]));
            }
            if(i<dimr2)
            {
                eint = cfloat_add(eint, cfloat_mulr(el2loc[i], Vee[thread*dimr1+i]));
            }
        }

        //Have each thread load it's portion of the vec-vec product into the buffers
        if(thread < dimr1)
        {
            R1buff[thread] = cfloat_mul(Rint1, el1loc[thread]);
            R2buff[thread] = cfloat_mul(Rint2, el2loc[thread]);
            ebuff[thread] = cfloat_mul(eint, el1loc[thread]);
        }
        if(thread < dimR)
        {
            R3buff[thread] = cfloat_mulr(Rloc[thread], Rinv[thread]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        //Have the 0th thread calc the vec-vec product
        //Relies on r1dim==r2dim
        if(thread == 0)
        {
            __private cfloat_t m1, m2, mr, rint;
            m1.real = 0.0;
            m1.imag = 0.0;
            m2.real = 0.0;
            m2.imag = 0.0;
            mr.real = 0.0;
            mr.imag = 0.0;
            rint.real = 0.0;
            rint.imag = 0.0;

            for(int i=0; i < size; i++)
            {  
                if(i<dimr1)
                {
                    sum = cfloat_add(sum, R2buff[i]);
                    sum1 = cfloat_add(sum1, R1buff[i]);
                    sum2 = cfloat_add(sum2, ebuff[i]);
                    m1 = cfloat_add(m1, el1loc[i]);
                    m2 = cfloat_add(m2, el2loc[i]);
                }
                if(i<dimR)
                {
                    mr = cfloat_add(mr, Rloc[i]);
                    rint = cfloat_add(rint, R3buff[i]);
                }
            }
            //__private const int gi = glob_index[Wi];
            sum1  = cfloat_mul(sum1, m2);
            sum2 = cfloat_mul(sum2, mr);
            sum = cfloat_add(cfloat_mul(sum, m1),  cfloat_mul(cfloat_mul(m1,m2),rint));

            //W is passed as a flattened upper triangular and will be restored out of kernel
            //W[Wi] = cfloat_add(cfloat_add(sum, sum1), sum2);
            //This is for a full W
            W[Wy*N+Wx] = cfloat_add(cfloat_add(sum, sum1), sum2);
            W[Wx*N+Wy] = cfloat_conj(cfloat_add(cfloat_add(sum, sum1), sum2));
            M[Wy*N+Wx] = cfloat_mul(cfloat_mul(m1, m2), mr);
            M[Wx*N+Wy] = cfloat_conj(cfloat_mul(cfloat_mul(m1, m2), mr));
            
        }
           
}

//have each thread compute the value of its position's potential
//returns V1s, V2s and VRs at each thread's position
__private float* Vs_thread(const float r1x, const float r2x, const float Rx,
                           const float pos_e1, const float pos_e2, const float pos_R, 
                           const float cr2, const float mu, const float M1, const float M2)
{
    __private float vs[3];
    
    vs[0] = (1.0f/sqrt( pow(r1x - pos_e2, 2) + cr2) - 1.0f/sqrt(pow(r1x - mu*pos_R/M1, 2) + cr2)\
          -1.0f/sqrt(pow(r1x + mu*pos_R/M2, 2) + cr2));
    vs[1] = (1.0f/sqrt( pow(r2x - pos_e1, 2) + cr2) - 1.0f/sqrt(pow(r2x - mu*pos_R/M1, 2) + cr2)\
          -1.0f/sqrt(pow(r2x + mu*pos_R/M2, 2) + cr2));
    vs[2] = (1.0f/Rx - 1.0f/sqrt(pow(pos_e1 - mu*Rx/M1, 2) + cr2)\
          -1.0f/sqrt(pow(pos_e1 + mu*Rx/M2, 2) + cr2) - 1.0f/sqrt(pow(pos_e2 - mu*Rx/M1, 2) + cr2)\
          -1.0f/sqrt(pow(pos_e2 + mu*Rx/M2, 2) + cr2) );
   

    return vs;
}
//Needs to be feed slices of the CWFs with 0s where appropriate for BC
__private cfloat_t* diff_thread(__private cfloat_t *el1, __private cfloat_t *el2,
                           __private cfloat_t *R, const float tce, const float tcr, const float gce, const float gcr)
{
    /* Returns 5 point Kinetic energy and gradient as KE el1, el2, R Grad el1, el2, R*/
    __private cfloat_t ts[6]= {cfloat_new(0.0f,0.0f)};
    __private float lap[11] = {1.0f/3150.0f, -5.0f/1008.0f, 5.0f/126.0f, -5.0f/21.0f, 5.0f/3.0f, -73766.0f/25200.0f,\
                         5.0f/3.0f, -5.0f/21.0f, 5.0f/126.0f, -5.0f/1008.0f, 1.0f/3150.0f};
    __private float grad[11] = {-1.0f/1260.0f, 5.0f/504.0f, -5.0f/84.0f, 5.0f/21.0f, -5.0f/6.0f, 0.0f,\
                         5.0f/6.0f, -5.0f/21.0f, 5.0f/84.0f, -5.0f/504.0f, 1.0f/1260.0f};
    //for(int i=0; i<6; i++) {ts[i] = cfloat_new(0.0f,0.0f);}
    for(int i=0; i<11; i++)
    {
        ts[0] = cfloat_mulr(cfloat_add(ts[0], cfloat_mulr(el1[i],lap[i]) ), tce);
        ts[3] = cfloat_mulr(cfloat_add(ts[3], cfloat_mulr(el1[i],grad[i]) ), gce);
        ts[1] = cfloat_mulr(cfloat_add(ts[1], cfloat_mulr(el2[i],lap[i]) ), tce);
        ts[4] = cfloat_mulr(cfloat_add(ts[4], cfloat_mulr(el2[i],grad[i]) ), gce);
        ts[2] = cfloat_mulr(cfloat_add(ts[2], cfloat_mulr(R[i],lap[i]) ), tcr);
        ts[5] = cfloat_mulr(cfloat_add(ts[5], cfloat_mulr(R[i],grad[i]) ), gcr);
    }
    return ts;
} 
__kernel void test(const __global cfloat_t *el1, const __global cfloat_t *el2, const __global cfloat_t *R,
                   const float d1, const float dr,const float mu, const float mu_e,
                    __global cfloat_t *dump)
{
    const int Wi = get_group_id(0); 
    const int thread = get_local_id(0);
    const int size = get_local_size(0);
    __private const float tce = -1.0f/(pow(d1,2)*mu_e);
    __private const float tcr = -1.0f/(pow(dr,2)*mu);
    __private const float gce = 1.0f/(d1);
    __private const float gcr = 1.0f/(dr*mu);

    __private cfloat_t *ts;
    __private cfloat_t e1[11];
    __private cfloat_t e2[11];
    __private cfloat_t rl[11];
    for (int i=0; i<11; i++)
    {
        if( (thread - i) < 0 || (thread + i) > (size -1)){ 
            e1[i].real = 0.0f;
            e1[i].imag = 0.0f;
            e2[i].real = 0.0f;
            e2[i].imag = 0.0f;
            rl[i].real = 0.0f;
            rl[i].imag = 0.0f;
        }
        else{
            e1[i] = el1[Wi + thread];
            e2[i] = el2[Wi + thread];
            rl[i] = R[Wi+thread];
        }
        
    }
    ts = diff_thread(e1, e2, rl, tce, tcr, gce, gcr);
    dump[Wi] = ts[0];
}
//{{{
/*
        const __global float *r1x,  const __global float *r2x, const __global float *Rx,
        const __global float *pos_e1, const __global float *pos_e2, const __global float *pos_R,
        const float cr2, const float mu, const float M1, float M2)
*/
__kernel void fCdot(const __global cfloat_t* el1, const __global cfloat_t *el2,  
        const __global cfloat_t *R, const __global float *Ve1R, const __global float *Ve2R, 
        const __global float *Vee, const __global float *Rinv, 
        const int dimr1, const int  dimr2, const int dimR, 
        __global cfloat_t *W, const int N, 
        const __global int *glob_index_x, const __global int *glob_index_y)
{
        const int Wi = get_group_id(0); 
        const int Wx = glob_index_x[Wi];
        const int Wy = glob_index_y[Wi];
        //const int Wy = (get_group_id(0) - Wx)/N;
        const int thread = get_local_id(0);
        const int size = get_local_size(0);

        __private cfloat_t sum, sum1, sum2;
        sum.real = 0.0;
        sum.imag = 0.0;
        sum1.real = 0.0;
        sum1.real = 0.0;
        sum2.imag = 0.0;
        sum2.imag = 0.0;
       
        //Each thread computes one entry in the VeiR RR mat-vec
        __private cfloat_t Rint1, Rint2, eint;
        Rint1.real = 0.0;
        Rint1.imag = 0.0;
        Rint2.real = 0.0;
        Rint2.imag = 0.0;
        eint.real = 0.0;
        eint.imag = 0.0;


        //Local product arrays of species CWFs multiplied by complex conjugates
        __local cfloat_t Rloc[80];
        __local cfloat_t el1loc[90]; 
        __local cfloat_t el2loc[90];//[dimr2]
        __local cfloat_t R1buff[90];//[dimr1]
        __local cfloat_t R2buff[90];//[dimr2]
        __local cfloat_t R3buff[80];//[dimR]
        __local cfloat_t ebuff[90];//[dimr1]

        if(thread < dimr1)
        {
            el1loc[thread] = cfloat_mul(el1[Wx*dimr1 + thread], cfloat_conj(el1[Wy*dimr1+thread]));
        }
        if(thread < dimr2)
        {
            el2loc[thread] = cfloat_mul(el2[Wx*dimr2 + thread], cfloat_conj(el2[Wy*dimr2+thread]));
        }
        if(thread < dimR)
        {
            Rloc[thread] =  cfloat_mul(R[Wx*dimR + thread], cfloat_conj(R[Wy*dimR+thread]));
        } 

        barrier(CLK_LOCAL_MEM_FENCE);

        //Ve1R is row major Ve1R[e1x,Rx] = Ve1R[e1x*dimR + Rx]
        for (int i=0; i < size; i++)
        {
            if(i<dimR)
            {
                Rint1 = cfloat_add(Rint1, cfloat_mulr(Rloc[i], Ve1R[thread*dimR + i]));
                Rint2 = cfloat_add(Rint2, cfloat_mulr(Rloc[i], Ve2R[thread*dimR + i]));
            }
            if(i<dimr2)
            {
                eint = cfloat_add(eint, cfloat_mulr(el2loc[i], Vee[thread*dimr1+i]));
            }
        }

        //Have each thread load it's portion of the vec-vec product into the buffers
        if(thread < dimr1)
        {
            R1buff[thread] = cfloat_mul(Rint1, el1loc[thread]);
            R2buff[thread] = cfloat_mul(Rint2, el2loc[thread]);
            ebuff[thread] = cfloat_mul(eint, el1loc[thread]);
        }
        if(thread < dimR)
        {
            R3buff[thread] = cfloat_mulr(Rloc[thread], Rinv[thread]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        //Have the 0th thread calc the vec-vec product
        //Relies on r1dim==r2dim
        if(thread == 0)
        {
            __private cfloat_t m1, m2, mr, rint;
            m1.real = 0.0;
            m1.imag = 0.0;
            m2.real = 0.0;
            m2.imag = 0.0;
            mr.real = 0.0;
            mr.imag = 0.0;
            rint.real = 0.0;
            rint.imag = 0.0;

            for(int i=0; i < size; i++)
            {  
                if(i<dimr1)
                {
                    sum = cfloat_add(sum, R2buff[i]);
                    sum1 = cfloat_add(sum1, R1buff[i]);
                    sum2 = cfloat_add(sum2, ebuff[i]);
                    m1 = cfloat_add(m1, el1loc[i]);
                    m2 = cfloat_add(m2, el2loc[i]);
                }
                if(i<dimR)
                {
                    mr = cfloat_add(mr, Rloc[i]);
                    rint = cfloat_add(rint, R3buff[i]);
                }
            }
            //__private const int gi = glob_index[Wi];
            sum1  = cfloat_mul(sum1, m2);
            sum2 = cfloat_mul(sum2, mr);
            sum = cfloat_add(cfloat_mul(sum, m1),  cfloat_mul(cfloat_mul(m1,m2),rint));

            //W is passed as a flattened upper triangular and will be restored out of kernel
            //W[Wi] = cfloat_add(cfloat_add(sum, sum1), sum2);
            //This is for a full W
            W[Wy*N+Wx] = cfloat_add(cfloat_add(sum, sum1), sum2);
            W[Wx*N+Wy] = cfloat_conj(cfloat_add(cfloat_add(sum, sum1), sum2));
            
        }
           
}
//}}}


"""
#{{{
platforms = cl.get_platforms()
if(len(platforms)>1): #List patforms and devices, and accept user choice
    print('')
    print('Available Platforms')
    print('---------------------------------------------------------------')
    i_ = 0
    for plat in platforms:
        print('@ Platform ',i_)
        i_ += 1
        
        devices = plat.get_devices()
        print('Device(s) on this platform: ')
        if(len(devices)>1):
            j_ = 0
            for dev in devices:
                print('\tDevice ',j_)
                j_ += 1
                print('\tType: ', cl.device_type.to_string(dev.type))
                print('\tAddress bits: ',dev.address_bits)
                print('\tMax compute units: ', dev.max_compute_units)
                print('\tMax Workgroup Size: ',my_device.max_work_group_size)
                print('')
        else:
            my_device = devices[0]
            print('\tDevice 0')
            print('\tType: ', cl.device_type.to_string(my_device.type))
            print('\tAddress bits: ',my_device.address_bits)
            print('\tMax compute units: ', my_device.max_compute_units)
            print('\tMax Workgroup Size: ',my_device.max_work_group_size)
            print('')
    print('---------------------------------------------------------------')
    pform = input("Which of these compute platforms do you wish to use? ")
    pform = int(pform)
    devices = platforms[pform].get_devices()
    if(len(devices)>1):
        print('Device(s) on this platform: ')
        j_ = 0
        for dev in devices:
            print('\tDevice ',j_)
            j_ += 1
            print('\tType: ', cl.device_type.to_string(dev.type))
            print('\tAddress bits: ',dev.address_bits)
            print('\tMax compute units: ', dev.max_compute_units)
            print('\tMax Workgroup Size: ',my_device.max_work_group_size)
            print('')
        my_device_id = input("Which device on this platform do you wish to use? ")
        my_device_id = int(my_device_id)
        my_device = devices[my_device_id]
        dtype_ = devices[my_device_id].address_bits
        if(dtype_ == 32):
            dtype_ = 'float32'
        if(dtype_ == 64):
            dtype_ = 'float64'

    else: #If our chosen platform only has one device
        my_device = devices[0]
        dtype_ = devices[0].address_bits
        if(dtype_ == 32):
            dtype_ = 'float32'
        if(dtype_ == 64):
            dtype_ = 'float64'
        print('\nInformation on Device in use')
        print('\tType: ', cl.device_type.to_string(my_device.type))
        print('\tAddress bits: ',my_device.address_bits)
        print('\tMax compute units: ', my_device.max_compute_units)
        print('\tMax Workgroup Size: ',my_device.max_work_group_size)
        print('')

else: #If there's only one platform, list devices and accept user choice
    devices = platforms[0].get_devices()
    if(len(devices)>1):
        print('Device(s) on this platform: ')
        j_ = 0
        for dev in devices:
            print('\tDevice ',j_)
            j_ += 1
            print('\tType: ', cl.device_type.to_string(dev.type))
            print('\tAddress bits: ',dev.address_bits)
            print('\tMax compute units: ', dev.max_compute_units)
            print('\tMax Workgroup Size: ',my_device.max_work_group_size)
            print('')
        my_device_id = input("Which device on this platform do you wish to use? ")
        my_device_id = int(my_device_id)
        my_device = devices[my_device_id]
        dtype_ = devices[my_device_id].address_bits
        if(dtype_ == 32):
            dtype_ = 'float32'
        if(dtype_ == 64):
            dtype_ = 'float64'

    else: #If our chosen platform only has one device, go ahead and set it up, printing the device info again for clarity.
        my_device = devices[0]
        dtype_ = devices[0].address_bits
        if(dtype_ == 32):
            dtype_ = 'float32'
        if(dtype_ == 64):
            dtype_ = 'float64'
        print('\nInformation on Device in use')
        print('\tType: ', cl.device_type.to_string(my_device.type))
        print('\tAddress bits: ',my_device.address_bits)
        print('\tMax compute units: ', my_device.max_compute_units)
        print('\tMax Workgroup Size: ',my_device.max_work_group_size)
        print('')
print('\n')
#}}}
#Set up the context, mem flags and queue
ctx = cl.Context([my_device])
mf = cl.mem_flags
queue = cl.CommandQueue(ctx)
C, el1, el2, R, pos_e1,pos_e2,pos_R,mesh_e1,mesh_e2,mesh_R,ansatz = restart_from_time(0.5,'./coarse-start/')
N = np.int32(C.shape[0])
#W2 = Cd1e(C,el1,el2,R,500)
#W2 = np.load('./Wtest.npy')
if(dtype_ == 'float64'):
    prg = cl.Program(ctx, kernel64).build()
    Cdot = prg.Cdot
    dtype_ = np.complex128
elif(dtype_ == 'float32'):
    prg = cl.Program(ctx, kernel32).build()
    Cdot = prg.fCdot
    test = prg.test
    dtype_ = np.complex64
    C = C.astype(np.complex64)
    el1 = el1.astype(np.complex64)
    el2 = el2.astype(np.complex64)
    R = R.astype(np.complex64)
    Ve1R = Ve1R.astype(np.float32)
    Ve2R = Ve2R.astype(np.float32)
    Vee = Vee.astype(np.float32)
    Rinv = Rinv.astype(np.float32)
    cr2 = np.float32(p.Cr2)
    mu = np.float32(p.mu)
    mu_e = np.float32(mu_e)
    mass1 = np.float32(p.M1)
    mass2 = np.float32(p.M2)
    r1x = p.r1x_array.astype(np.float32)
    r2x = p.r2x_array.astype(np.float32)
    Rx = p.Rx_array.astype(np.float32)
    pos_e1 = np.float32(pos_e1)
    pos_e2 = np.float32(pos_e2)
    pos_R = np.float32(pos_R)
    de1 = np.float32(p.e_spacing)
    dr = np.float32(p.n_spacing)
wk_grp_size = int(my_device.max_work_group_size)


r1_dim = np.int32(el1.shape[-1])
r2_dim = np.int32(el2.shape[-1])
R_dim = np.int32(R.shape[-1])

M1 = np.dot(np.conj(el1),el1.transpose())
M2 = np.dot(np.conj(el2),el2.transpose())
MR = np.dot(np.conj(R),R.transpose())
mem_pool = cl.tools.MemoryPool(cl.tools.ImmediateAllocator(queue))
Ve1R_dev = cl_array.to_device(queue,np.ravel(Ve1R), allocator=mem_pool)
Ve2R_dev = cl_array.to_device(queue,np.ravel(Ve2R),allocator=mem_pool)
Vee_dev = cl_array.to_device(queue,np.ravel(Vee),allocator=mem_pool)

r1x_dev = cl_array.to_device(queue,r1x, allocator=mem_pool)
r2x_dev = cl_array.to_device(queue,r2x, allocator=mem_pool)
Rx_dev = cl_array.to_device(queue,Rx, allocator=mem_pool)

#To map between the flattened upper triangular matrix and the global flattened matrix
#There are N*(N+1)/2 workgroups, each one will reference the global position of the flattened W matrix
# additionally it needs to know which elements of the CWF matrices to use.
indices = np.ravel_multi_index(np.triu_indices(N), (N,N)).astype(np.int32)
index_x, index_y = np.triu_indices(N)
glob_index = cl_array.to_device(queue,indices)
#Flip around the y and x arrays
glob_index_y = cl_array.to_device(queue, index_x.astype(np.int32), allocator=mem_pool)
glob_index_x = cl_array.to_device(queue, index_y.astype(np.int32), allocator=mem_pool)

Rinv_dev = cl_array.to_device(queue, Rinv)

grp_size = max([r1_dim,r2_dim,R_dim])
glob_size = np.int32(grp_size*N*(N+1)/2)

#Wtemp = np.ravel(np.zeros(int(N*(N+1)/2),dtype=dtype_))
Wtemp = np.zeros(int(N**2),dtype=dtype_)
W_dev = cl_array.to_device(queue,Wtemp,allocator=mem_pool)
Mtemp = np.zeros(int(N**2),dtype=dtype_)
M_dev = cl_array.to_device(queue,Mtemp,allocator=mem_pool)
el1_dev = cl_array.to_device(queue,np.ravel(el1), allocator=mem_pool)
el2_dev = cl_array.to_device(queue,np.ravel(el2), allocator=mem_pool)
R_dev = cl_array.to_device(queue,np.ravel(R), allocator=mem_pool)
pos_e1 = cl_array.to_device(queue,pos_e1,allocator=mem_pool)
pos_e2 = cl_array.to_device(queue,pos_e2,allocator=mem_pool)
pos_R = cl_array.to_device(queue,pos_R,allocator=mem_pool)
time = []
@jit(nopython=True,nogil=True)
def cdot(M,W,C, V1s, V2s, VRs):
    M_e1 = np.dot(np.conj(el1),el1.transpose())
    M_e2 = np.dot(np.conj(el2),el2.transpose())
    M_R = np.dot(np.conj(R),R.transpose())
    W1 = np.dot(np.conj(el1),(el1*V1s).transpose())*M_e2*M_R
    W2 = np.dot(np.conj(el2),(el2*V2s).transpose())*M_e1*M_R
    WR = np.dot(np.conj(R),(R*VRs).transpose())*M_e1*M_e2
    W -= (W1 + W2 + WR)*p.e_spacing**2*p.n_spacing
    return -1j*np.dot(np.linalg.pinv(M),np.dot(W,C))

for i in range(10):
    with Timer() as t:
        #M1_dev = cl_array.to_device(queue,np.ravel(M1))
        #M2_dev = cl_array.to_device(queue,np.ravel(M2))
        #MR_dev = cl_array.to_device(queue,np.ravel(MR))
        el1_dev.set(np.ravel(el1),queue=queue) 
        el2_dev.set(np.ravel(el2),queue=queue) 
        R_dev.set(np.ravel(R),queue=queue) 
        """
        W_dev = cl.Buffer(ctx, mf.WRITE_ONLY, W.nbytes)
        Wtemp = np.ravel(np.zeros(W.shape,dtype=dtype_))
        W += np.reshape(Wtemp,W.shape)
        W_dev.get(queue=queue,ary=Wtemp)
        W = np.reshape(Wtemp,(N,N)) #+ M1*M2*np.dot(np.conj(R),(R*Rinv).transpose())
        Cdot(queue,(glob_size,),(grp_size,), el1_dev.data, el2_dev.data, R_dev.data, \
                Ve1R_dev.data, Ve2R_dev.data, Vee_dev.data, Rinv_dev.data,\
                r1x_dev.data, r2x_dev.data, Rx_dev.data, pos_e1.data, pos_e2.data, pos_R.data,\
                r1_dim, r2_dim, R_dim, W_dev.data, N, glob_index_x.data, glob_index_y.data,\
                cr2, mu, mass1, mass2)
        """
        Cdot(queue,(glob_size,),(grp_size,), el1_dev.data, el2_dev.data, R_dev.data, \
                  Ve1R_dev.data, Ve2R_dev.data, Vee_dev.data, Rinv_dev.data,\
                  r1_dim, r2_dim, R_dim, W_dev.data, M_dev.data, N, glob_index_x.data, glob_index_y.data)
        queue.finish()
        W_dev.get(queue=queue,ary=Wtemp)
        W = np.reshape(Wtemp,(N,N)) 
        M_dev.get(queue=queue,ary=Mtemp)
        M = np.reshape(Mtemp,(N,N)) 
        Cdot = cdot(M*de1**2*dr,W,C)
        #dump = cl_array.to_device(queue,np.zeros(N*grp_size).astype(np.complex64), allocator=mem_pool)
        
        #W = W + np.conj(W-np.diag(np.diag(W))).transpose()
        #tack on solely nuclear part

    time.append(t.interval)
time2 = []
for i in range(10):
    with Timer() as t:
        Cdot = C_dot_1e(C,el1,el2,R,V1s,V2s,VRs,N)
    time2.append(t.interval)
print('GPU ', np.average(time))
print('CPU ', np.average(time2))

"""
dummy_var = np.zeros(N*grp_size).astype(np.complex64)
dump.get(queue=queue,ary=dummy_var)
print('\n')
#for i in range(len(dummy_var)):
#    print('i:',i,' ',dummy_var[i])
print('\n')
print('Avg time = ',np.average(time))
Te1 = np.zeros((N,r1_dim),dtype=dtype_)
for i in range(N):
    Te1[i] = T_e1.dot(el1[i])
dummy_var = np.reshape(dummy_var,(N,grp_size))
print(np.abs(dummy_var-Te1).max())
np.save('Te1',Te1)
np.save('oTe1',dummy_var)
#np.save('./openWsans',W)
#W2 = np.load('./Wsans.npy')
#W2 = np.load('./Wtest.npy')
#print((W-W2).max())
"""
