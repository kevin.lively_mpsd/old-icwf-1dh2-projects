import numpy as np
from numba import jit
from matplotlib import pyplot as plt
from params import *
from functions import *
import cluster_gpu_cwf as cwf

cwf.ICWF_propagate(tf_tot,tf_laser,new_mesh=True,runGPU=True)
