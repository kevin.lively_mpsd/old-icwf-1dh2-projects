import numpy as np
import sys
sys.path.insert(0,'./src')
from numba import jit
from params import *
from functions import *
import cwf


t_init = 60.500000
init_num_trajs = num_trajs
el1 = np.load('./dump/1e/e1-cwf-'+"{:.6f}".format(t_init)+'.npy')
el2 = np.load('./dump/1e/e2-cwf-'+"{:.6f}".format(t_init)+'.npy')
R = np.load('./dump/1e/R-cwf-'+"{:.6f}".format(t_init)+'.npy')
pos_e1 = np.load('./dump/1e/pos_e1-'+"{:.6f}".format(t_init)+'.npy')
pos_e2 = np.load('./dump/1e/pos_e2-'+"{:.6f}".format(t_init)+'.npy')
pos_R = np.load('./dump/1e/pos_R-'+"{:.6f}".format(t_init)+'.npy')
e1_mesh = np.load('./dump/1e/e1_mesh-'+"{:.6f}".format(t_init)+'.npy')
e2_mesh = np.load('./dump/1e/e2_mesh-'+"{:.6f}".format(t_init)+'.npy')
R_mesh = np.load('./dump/1e/R_mesh-'+"{:.6f}".format(t_init)+'.npy')
C = np.load('./dump/1e/C-'+"{:.6f}".format(t_init)+'.npy')

cwf.ICWF_restart_propagate(tf_tot,tf_laser,'1e', t_init,el1,el2,R,pos_e1,pos_e2,pos_R,e1_mesh,e2_mesh,R_mesh,C)
