from importlib import reload
import functions as f
reload(f)
import params as p
reload(p)
import numpy as np
from matplotlib import pyplot as plt
from numba import jit, prange, njit#, vectorize, float32, complex64, int32, complex128, float64, int64
from math import floor
#import os
from subprocess import call
from test import print_norm
from decimal import Decimal
from scipy.linalg import eig


num_trajs = p.num_trajs
@jit( nogil=True)
def e2_separable_correlated_wf():
    out = np.zeros((len(p.r1x), len(p.r2x), len(p.Rx)))+0j
    for r1i in range(len(p.r1x)):
        for r2i in range(len(p.r2x)):
            for Ri in range(len(p.Rx)):
                #out[r1i, r2i, Ri] = (np.exp(-(p.r1x[r1i]-1)**2)*np.exp(-(p.r2x[r2i]+1)**2) \
                #    + np.exp(-(p.r1x[r2i] - 1)**2)*np.exp(-(p.r2x[r1i]+1)**2))*np.exp(-p.Rx[Ri]**2)
                out[r1i, r2i, Ri] = (np.exp(-(p.r1x[r1i]-1)**2)*np.exp(-(p.r2x[r2i]+1)**2))\
                                        *np.exp(-p.Rx[Ri]**2)
    return out/np.sqrt(np.sum(np.sum(np.sum(np.abs(out)**2)))*p.e_spacing**2*p.n_spacing)


@jit( nogil=True)
def initialize_2e_mesh(threshold,psi0, Unique):
    pdf = np.abs(psi0)**2#*e_spacing**2*n_spacing
    r1_mesh = []
    r2_mesh = []
    R_mesh = []
    #for making sure the slice is unique
    triplets = []
    #start with nuclear conditional slices
    for i in range(num_trajs):
        valR = 0
        val_e = 0
        val_array = [val_e, valR]
        #check all three indices at once, assures value sampling
        #but there's no way to tell how long this will take
        #doing the indices separately would also work.
        iterations = 0
        while (min(val_array)<threshold):#(len(r1_mesh)<num_trajs):
            iterations += 1
            if(iterations%100000==0):
                print('Saturated Unique positions at 1 million guesses.')
                Unique=False
            r1_alpha = floor(len(p.r1x)*np.random.rand())
            r2_alpha = floor(len(p.r2x)*np.random.rand())
            R_alpha = floor(len(p.Rx)*np.random.rand())
            if(Unique==True):
                #make sure that the slice found is unique
                if([r1_alpha, r2_alpha, R_alpha] in triplets[:i]):
                    val_array[0] = i+threshold
                    continue
            #check if the integral over the conditional slice's pdf is within the tolerance  
            val_array[0] = np.sum(np.sum(pdf[:,:,R_alpha]))*p.e_spacing**2
            val_array[1] = np.sum(pdf[r1_alpha,r2_alpha,:])*p.n_spacing
        r1_mesh.append(r1_alpha)
        r2_mesh.append(r2_alpha)
        R_mesh.append(R_alpha)
        triplets.append([r1_alpha, r2_alpha, R_alpha])
        print('initialize mesh is ' + str(100*float(i)/num_trajs) +'% done')
    return r1_mesh, r2_mesh, R_mesh


@jit( nogil=True)
def check_uniqueness(r1,r2,R):
    triplets = []
    for i in range(len(r1)):
        sli = [r1[i],r2[i],R[i]]
        triplets.append(sli)
        if (sli in triplets[:i] ):
            original = triplets.index(sli)
            return 'repeated value ' + str(i) + ' original value ' + str(original)
    return 'COOL!'

#psi_e2 will come in as (num_trajs, dim_e1, dim_e2)
@jit( parallel=True,nogil=True)
def calc_2e_vel(psi_2e, e1_mesh, e2_mesh):
    #the shape should be (num_traj, dim_e1)
    vel = np.zeros((2, num_trajs))
    for traj in prange(num_trajs):
        grad_psi_1 = np.gradient(psi_2e[traj,:,e2_mesh[traj]],p.e_spacing)
        grad_psi_2 = np.gradient(psi_2e[traj,e1_mesh[traj],:],p.e_spacing)
        #the below does an element by element division
        vel[0,traj] = (grad_psi_1/psi_2e[traj,:,e2_mesh[traj]])[e1_mesh[traj]].imag
        vel[1,traj] = (grad_psi_2/psi_2e[traj,e1_mesh[traj],:])[e2_mesh[traj]].imag
    return vel[0], vel[1]

@jit( parallel=True,nogil=True)
def ICWF_vel(C,psi_el,psi_R, e1_mesh, e2_mesh, R_mesh):
    el1_num = np.zeros(psi_el.shape[0],dtype=np.complex128)
    el2_num = np.zeros(psi_el.shape[0],dtype=np.complex128)
    R_num = np.zeros(psi_R.shape[0],dtype=np.complex128)
    denom = 0j
    for i in prange(psi_el.shape[0]):
        el1_num[i] = C[i]*psi_R[i,R_mesh[i]]*np.gradient(psi_el[i,:,e2_mesh[i]])[e1_mesh[i]]
        el2_num[i] = C[i]*psi_R[i,R_mesh[i]]*np.gradient(psi_el[i,e1_mesh[i],:])[e2_mesh[i]]
        R_num[i] = C[i]*psi_el[i,e1_mesh[i],e2_mesh[i]]*np.gradient(psi_R[i,:])[R_mesh[i]]
        denom += C[i]*psi_el[i,e1_mesh[i],e2_mesh[i]]*psi_R[i,R_mesh[i]]
    return (el1_num/denom).imag, (el2_num/denom).imag, (R_num/denom).imag


@jit( nogil=True)
def cwf_nuclear_density(psi_n):
    rho_R = np.zeros(p.Rx_array.shape)
    for i in range(num_trajs):
        rho_R += np.abs(psi_n[i])**2
    return rho_R/(np.sum(rho_R)*p.n_spacing)


@jit( nogil=True)
def cwf_2e_density(two_e_wfs):
    rho_e = np.zeros(len(p.r1x))
    for i in range(num_trajs):
        rho_e += .5*np.sum(np.abs(two_e_wfs[i,:,:])**2,1)*p.e_spacing \
                + .5*np.sum(np.abs(two_e_wfs[i,:,:])**2,0)*p.e_spacing
    return 2*rho_e/(np.sum(rho_e)*p.e_spacing)


r1x_a = p.r1x_array
r2x_a = p.r2x_array
Rx_a = p.Rx_array
r1_dim = r1x_a.shape[0]
r2_dim = r2x_a.shape[0]
R_dim = Rx_a.shape[0]
Cr2 = p.Cr2
mu = p.mu
M1 = p.M1
M2 = p.M2


@jit( nogil=True)
def two_e_T(two_e_wf):
    out = np.zeros(two_e_wf.shape)+0j
    for r1i in range(len(p.r1x)):
        out[r1i,:] += f.T_e1.dot(two_e_wf[r1i,:])
        out[:,r1i] += f.T_e1.dot(two_e_wf[:,r1i])
    return out

@jit( nogil=True)
def construct_2e_V():
    V2e = np.zeros((len(p.r1x),len(p.r2x),len(p.Rx)))
    for Ri in range(len(p.Rx)):
        for r2i in range(len(p.r2x)):
            V2e[:,r2i,Ri] = 1.0/(np.sqrt( (p.r1x - p.r2x[r2i])**2 + Cr2)) + 1.0/p.Rx[Ri]\
                        - 1.0/(np.sqrt( (p.r1x - mu*p.Rx[Ri]/M1)**2 +Cr2))\
                        - 1.0/np.sqrt( (p.r1x + mu*p.Rx[Ri]/M2)**2 + Cr2)\
                        - 1.0/(np.sqrt( (p.r2x[r2i] - mu*p.Rx[Ri]/M1)**2 +Cr2))\
                        - 1.0/np.sqrt( (p.r2x[r2i] + mu*p.Rx[Ri]/M2)**2 + Cr2)
    return V2e
V2e = construct_2e_V()

@jit( nogil=True)
def construct_cwf_VR():
    VR = np.zeros((len(p.r1x),len(p.r2x),len(p.Rx)))
    for r1i in range(len(p.r1x)):
        for r2i in range(len(p.r2x)):
            VR[r1i,r2i,:] = 1.0/p.Rx_array- 1.0/(np.sqrt( (p.r2x[r2i] - mu*p.Rx_array/M1)**2 +Cr2))\
                        - 1.0/np.sqrt( (p.r2x[r2i] + mu*p.Rx_array/M2)**2 + Cr2)\
                        - 1.0/(np.sqrt( (p.r1x[r1i] - mu*p.Rx_array/M1)**2 +Cr2))\
                        - 1.0/np.sqrt( (p.r1x[r1i] + mu*p.Rx_array/M2)**2 + Cr2)
                        #+ 1.0/np.sqrt( (p.r1x[r1i] - p.r2x[r2i])**2 + Cr2)
    return VR

@jit( nogil=True)
def construct_VeR():
    Ve1R = np.zeros((len(p.r1x),len(p.Rx)))
    Ve2R = np.zeros((len(p.r2x),len(p.Rx)))
    for r1i in range(len(p.r1x)):
        Ve1R[r1i,:] = - 1.0/np.sqrt( (p.r1x[r1i] + mu*p.Rx_array/M2)**2 + Cr2)\
              - 1.0/(np.sqrt( (p.r1x[r1i] - mu*p.Rx_array/M1)**2 +Cr2))
        Ve2R[r1i,:] = - 1.0/np.sqrt( (p.r2x[r1i] + mu*p.Rx_array/M2)**2 + Cr2)\
              - 1.0/(np.sqrt( (p.r2x[r1i] - mu*p.Rx_array/M1)**2 +Cr2))
    return Ve1R, Ve2R
@jit( nogil=True)
def construct_Vee():
    Vee = np.zeros((len(p.r1x),len(p.r2x)))
    for r1i in range(len(p.r1x)):
        Vee[r1i,:] = 1.0/np.sqrt( (p.r1x[r1i] - p.r2x_array)**2 + Cr2)
    return Vee

Ve1R,Ve2R = construct_VeR()
Vee = construct_Vee()
Rinv_flat = np.array([1/p.Rx_array for x in range(num_trajs)])


@jit( nogil=True)
def construct_e_pos_grid():
    epos_grid = np.zeros((len(p.r1x),len(p.r2x)))
    for i in range(len(p.r1x)):
        epos_grid[i,:] = p.r1x[i] + p.r2x_array
    return -1*epos_grid

epos = construct_e_pos_grid()

mu_e = (M1+M2)/(M1+M2+1)
mu = p.mu

@jit( nogil=True)
def conditional_H_2e(two_e_wfs, psi_R, e1_mesh, e2_mesh,R_mesh):
    dt_psi_R = np.zeros((num_trajs, R_dim)) +0j
    dt_psi_2e = np.zeros((num_trajs, r1_dim, r2_dim)) +0j
    #If there is a mass imbalence between the atoms there is an additional term
    for i in range(num_trajs):
        dt_psi_2e[i] = two_e_T(two_e_wfs[i,:,:]) + V2e[:,:,R_mesh[i]]*two_e_wfs[i,:,:]
        dt_psi_R[i] = f.T_n.dot(psi_R[i]) + VR[e1_mesh[i],e2_mesh[i],:]*psi_R[i]
    return -1.0j*dt_psi_2e, -1.0j*dt_psi_R

@jit( nogil=True)
def conditional_Ht_2e(two_e_wfs, psi_R, e1_mesh, e2_mesh,R_mesh, E_t):
    dt_psi_R = np.zeros((num_trajs, len(Rx_a))) +0j
    dt_psi_2e = np.zeros((num_trajs, len(r1x_a), len(r2x_a))) +0j
    #If there is a mass imbalence between the atoms there is an additional term
    if(M1 == M2):
        for i in range(num_trajs):
            dt_psi_2e[i] = two_e_T(two_e_wfs[i,:,:]) + V2e[:,:,R_mesh[i]]*two_e_wfs[i,:,:] \
                    + E_t*(epos)*two_e_wfs[i,:,:]
            dt_psi_R[i] = f.T_n.dot(psi_R[i]) + VR[e1_mesh[i],e2_mesh[i],:]*psi_R[i]
    else:
        lamb = (M2 - M1)/(M1+M2)
        for i in range(num_trajs):
            dt_psi_2e[i] = two_e_T(two_e_wfs[i,:,:]) + V2e[:,:,R_mesh[i]]*two_e_wfs[i,:,:] \
                    + E_t*(epos)*two_e_wfs[i,:,:]
            dt_psi_R[i] = f.T_n.dot(psi_R[i]) + VR[e1_mesh[i],e2_mesh[i],:]*psi_R[i] \
                    +E_t*lamb*(Rx_a)*psi_R[i]
    return -1.0j*dt_psi_2e, -1.0j*dt_psi_R

#Write in simulataneous RK4 evolution of trajectory positions
@jit( nogil=True)
def RK4_2e(two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R):
    #find first derivative of the wavefunctions
    K1 = conditional_H_2e(two_e_wfs, R_wfs, \
                                    e1_mesh, e2_mesh, R_mesh)
    #find the first derivative of the trajectory positions
    traj_2e_vel_K1 = calc_2e_vel(two_e_wfs,e1_mesh,e2_mesh)
    traj_R_vel_K1 = calc_n_vel(R_wfs,R_mesh)

    #find the first derivative of the wavefunctions at the mid point time step,
    #   via projecting the wf according to the derivative at the original position
    K2 = conditional_H_2e(two_e_wfs + p.dt*K1[0]/2, \
                       R_wfs + p.dt*K1[1]/2, e1_mesh, e2_mesh,R_mesh)

    #Do the same for the trajectory positions, evaluating the velocities at this midpoint
    traj_2e_vel_K2 =calc_2e_vel(two_e_wfs + p.dt*K1[0]/2,\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K2 = calc_n_vel(R_wfs + p.dt*K1[1]/2,R_mesh)

    #Do the third term correction for the wavefunctions    
    K3 = conditional_H_2e(two_e_wfs + p.dt*K2[0]/2, \
                       R_wfs + p.dt*K2[1]/2, e1_mesh, e2_mesh,R_mesh)
    #And now the trajectory positions
    traj_2e_vel_K3= calc_2e_vel(two_e_wfs + p.dt*K2[0]/2,\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K3 = calc_n_vel(R_wfs + p.dt*K2[1]/2,R_mesh)

    #finally get the derivative information at the next timestep
    K4 = conditional_H_2e(two_e_wfs + p.dt*K3[0], \
                       R_wfs + p.dt*K3[1], e1_mesh, e2_mesh,R_mesh)
    #And now the trajectory positions
    traj_2e_vel_K4 = calc_2e_vel(two_e_wfs + p.dt*K3[0],\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K4 = calc_n_vel(R_wfs + p.dt*K3[1],R_mesh)

    #now update the global variables of trajectory positions
    pos_e1 = pos_e1 + (p.dt/6)*(traj_2e_vel_K1[0] + 2*traj_2e_vel_K2[0] +2*traj_2e_vel_K3[0] \
                        + traj_2e_vel_K4[0]) 
    pos_e2 = pos_e2 + (p.dt/6)*(traj_2e_vel_K1[1] + 2*traj_2e_vel_K2[1] +2*traj_2e_vel_K3[1] \
                        + traj_2e_vel_K4[1]) 
    pos_R = pos_R + (p.dt/6)*(traj_R_vel_K1 + 2*traj_R_vel_K2 +2*traj_R_vel_K3 \
                        + traj_R_vel_K4) 

    return two_e_wfs + (p.dt/6)*(K1[0] + 2*K2[0] + 2*K3[0] + K4[0]), \
            R_wfs + (p.dt/6)*(K1[1] + 2*K2[1] + 2*K3[1] + K4[1]), pos_e1, pos_e2, pos_R

@jit( nogil=True)
def T_dep_RK4_2e(two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R,\
                 curr_time_index, E_form):
    E_t = E_form[2*curr_time_index]
    E_t_half = E_form[2*curr_time_index+1]
    E_t_advanced = E_form[2*curr_time_index + 2] 
    #find first derivative of the wavefunctions
    K1 = conditional_Ht_2e(two_e_wfs, R_wfs, \
                                    e1_mesh, e2_mesh, R_mesh, E_t)
    #find the first derivative of the trajectory positions
    traj_2e_vel_K1 = calc_2e_vel(two_e_wfs,e1_mesh,e2_mesh)
    traj_R_vel_K1 = calc_n_vel(R_wfs,R_mesh)

    #find the first derivative of the wavefunctions at the mid point time step,
    #   via projecting the wf according to the derivative at the original position
    K2 = conditional_Ht_2e(two_e_wfs + p.dt*K1[0]/2, \
                       R_wfs + p.dt*K1[1]/2, e1_mesh, e2_mesh,R_mesh, E_t_half)

    #Do the same for the trajectory positions, evaluating the velocities at this midpoint
    traj_2e_vel_K2 =calc_2e_vel(two_e_wfs + p.dt*K1[0]/2,\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K2 = calc_n_vel(R_wfs + p.dt*K1[1]/2,R_mesh)

    #Do the third term correction for the wavefunctions    
    K3 = conditional_Ht_2e(two_e_wfs + p.dt*K2[0]/2, \
                       R_wfs + p.dt*K2[1]/2, e1_mesh, e2_mesh,R_mesh, E_t_half)
    #And now the trajectory positions
    traj_2e_vel_K3= calc_2e_vel(two_e_wfs + p.dt*K2[0]/2,\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K3 = calc_n_vel(R_wfs + p.dt*K2[1]/2,R_mesh)

    #finally get the derivative information at the next timestep
    K4 = conditional_Ht_2e(two_e_wfs + p.dt*K3[0], \
                       R_wfs + p.dt*K3[1], e1_mesh, e2_mesh,R_mesh, E_t_advanced)
    #And now the trajectory positions
    traj_2e_vel_K4 = calc_2e_vel(two_e_wfs + p.dt*K3[0],\
                                    e1_mesh,e2_mesh)
    traj_R_vel_K4 = calc_n_vel(R_wfs + p.dt*K3[1],R_mesh)

    #now update the global variables of trajectory positions
    pos_e1 = pos_e1 + (p.dt/6)*(traj_2e_vel_K1[0] + 2*traj_2e_vel_K2[0] +2*traj_2e_vel_K3[0] \
                        + traj_2e_vel_K4[0]) 
    pos_e2 = pos_e2 + (p.dt/6)*(traj_2e_vel_K1[1] + 2*traj_2e_vel_K2[1] +2*traj_2e_vel_K3[1] \
                        + traj_2e_vel_K4[1]) 
    pos_R = pos_R + (p.dt/6)*(traj_R_vel_K1 + 2*traj_R_vel_K2 +2*traj_R_vel_K3 \
                        + traj_R_vel_K4) 

    return two_e_wfs + (p.dt/6)*(K1[0] + 2*K2[0] + 2*K3[0] + K4[0]), \
            R_wfs + (p.dt/6)*(K1[1] + 2*K2[1] + 2*K3[1] + K4[1]), pos_e1, pos_e2, pos_R


#{{{ 2e ICWF RK4
#Need to remove Ets
@jit( nogil=True)
def RK4_2e_ICWF(two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R, C):
    #find first derivative of the wavefunctions
    K1 = conditional_H_2e(two_e_wfs, R_wfs, \
                                    e1_mesh, e2_mesh, R_mesh)

    #find the first derivative of the trajectory positions
    e1_K1, e2_K1, R_K1 = ICWF_vel(C,two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh)
    C_K1 = C_dot(C,two_e_wfs,R_wfs,e1_mesh,e2_mesh,R_mesh)

    #find the first derivative of the wavefunctions at the mid point time step,
    #   via projecting the wf according to the derivative at the original position
    K2 = conditional_H_2e(two_e_wfs + p.dt*K1[0]/2, \
                       R_wfs + p.dt*K1[1]/2, e1_mesh, e2_mesh,R_mesh)

    

    #Do the same for the trajectory positions, evaluating the velocities at this midpoint
    e1_K2, e2_K2, R_K2= ICWF_vel(C,two_e_wfs+ p.dt*K1[0]/2, R_wfs+ p.dt*K1[1]/2, e1_mesh, e2_mesh, R_mesh)
    C_K2 = C_dot(C,two_e_wfs + p.dt*K1[0]/2,R_wfs + p.dt*K1[1]/2,e1_mesh,e2_mesh,R_mesh)

    #Do the third term correction for the wavefunctions    
    K3 = conditional_H_2e(two_e_wfs + p.dt*K2[0]/2, \
                       R_wfs + p.dt*K2[1]/2, e1_mesh, e2_mesh,R_mesh)
    
    #And now the trajectory positions
    e1_K3, e2_K3, R_K3= ICWF_vel(C,two_e_wfs+ p.dt*K2[0]/2, R_wfs+ p.dt*K2[1]/2, e1_mesh, e2_mesh, R_mesh)
    C_K3 = C_dot(C,two_e_wfs + p.dt*K2[0]/2,R_wfs + p.dt*K2[1]/2,e1_mesh,e2_mesh,R_mesh)

    #finally get the derivative information at the next timestep
    K4 = conditional_H_2e(two_e_wfs + p.dt*K3[0], \
                       R_wfs + p.dt*K3[1], e1_mesh, e2_mesh,R_mesh)
    #And now the trajectory positions
    e1_K4, e2_K4, R_K4 = ICWF_vel(C,two_e_wfs+ p.dt*K3[0], R_wfs+ p.dt*K3[1], e1_mesh, e2_mesh, R_mesh)
    C_K4 = C_dot(C,two_e_wfs + p.dt*K3[0],R_wfs + p.dt*K3[1],e1_mesh,e2_mesh,R_mesh)

    #now update the global variables of trajectory positions
    pos_e1 = pos_e1 + (p.dt/6)*(e1_K1 + 2*e1_K2 +2*e1_K3 \
                        + e1_K4) 
    pos_e2 = pos_e2 + (p.dt/6)*(e2_K1 + 2*e2_K2 +2*e2_K3 \
                        + e2_K4) 
    pos_R = pos_R + (p.dt/6)*(R_K1 + 2*R_K2 +2*R_K3 \
                        + R_K4)
    C += (p.dt/6)*(C_K1 + 2*C_K2 + 2*C_K3 + C_K4) 

    return two_e_wfs + (p.dt/6)*(K1[0] + 2*K2[0] + 2*K3[0] + K4[0]), \
            R_wfs + (p.dt/6)*(K1[1] + 2*K2[1] + 2*K3[1] + K4[1]), pos_e1, pos_e2, pos_R, C

@jit( nogil=True)
def T_dep_RK4_2e_ICWF(two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R,\
                     curr_time_index, E_form, C):
    E_t = E_form[2*curr_time_index]
    E_t_half = E_form[2*curr_time_index+1]
    E_t_advanced = E_form[2*curr_time_index + 2] 

    #find first derivative of the wavefunctions
    K1 = conditional_Ht_2e(two_e_wfs, R_wfs, \
                                    e1_mesh, e2_mesh, R_mesh, E_t)

    #find the first derivative of the trajectory positions
    e1_K1, e2_K1, R_K1 = ICWF_vel(C,two_e_wfs, R_wfs, e1_mesh, e2_mesh, R_mesh)
    C_K1 = C_dot_t(C,two_e_wfs,R_wfs,e1_mesh,e2_mesh,R_mesh, E_t)

    #find the first derivative of the wavefunctions at the mid point time step,
    #   via projecting the wf according to the derivative at the original position
    K2 = conditional_Ht_2e(two_e_wfs + p.dt*K1[0]/2, \
                       R_wfs + p.dt*K1[1]/2, e1_mesh, e2_mesh,R_mesh, E_t_half)

    

    #Do the same for the trajectory positions, evaluating the velocities at this midpoint
    e1_K2, e2_K2, R_K2= ICWF_vel(C,two_e_wfs+ p.dt*K1[0]/2, R_wfs+ p.dt*K1[1]/2, \
                    e1_mesh, e2_mesh, R_mesh)
    C_K2 = C_dot_t(C + p.dt*C_K1/2 ,two_e_wfs + p.dt*K1[0]/2,R_wfs + p.dt*K1[1]/2,\
                    e1_mesh,e2_mesh,R_mesh, E_t_half)

    #Do the third term correction for the wavefunctions    
    K3 = conditional_Ht_2e(two_e_wfs + p.dt*K2[0]/2, \
                       R_wfs + p.dt*K2[1]/2, e1_mesh, e2_mesh,R_mesh, E_t_half)
    
    #And now the trajectory positions
    e1_K3, e2_K3, R_K3= ICWF_vel(C,two_e_wfs+ p.dt*K2[0]/2, R_wfs+ p.dt*K2[1]/2, \
                e1_mesh, e2_mesh, R_mesh)
    C_K3 = C_dot_t(C + p.dt*C_K2/2,two_e_wfs + p.dt*K2[0]/2,R_wfs + p.dt*K2[1]/2,\
                e1_mesh,e2_mesh,R_mesh, E_t_half)

    #finally get the derivative information at the next timestep
    K4 = conditional_Ht_2e(two_e_wfs + p.dt*K3[0], \
                       R_wfs + p.dt*K3[1], e1_mesh, e2_mesh,R_mesh, E_t_advanced)
    #And now the trajectory positions
    e1_K4, e2_K4, R_K4 = ICWF_vel(C,two_e_wfs+ p.dt*K3[0], R_wfs+ p.dt*K3[1], \
                    e1_mesh, e2_mesh, R_mesh)
    C_K4 = C_dot_t(C+p.dt*C_K3,two_e_wfs + p.dt*K3[0],R_wfs + p.dt*K3[1],\
                e1_mesh,e2_mesh,R_mesh, E_t_advanced)
    #now update the global variables of trajectory positions
    pos_e1 = pos_e1 + (p.dt/6)*(e1_K1 + 2*e1_K2 +2*e1_K3 \
                        + e1_K4) 
    pos_e2 = pos_e2 + (p.dt/6)*(e2_K1 + 2*e2_K2 +2*e2_K3 \
                        + e2_K4) 
    pos_R = pos_R + (p.dt/6)*(R_K1 + 2*R_K2 +2*R_K3 \
                        + R_K4)
    C += (p.dt/6)*(C_K1 + 2*C_K2 + 2*C_K3 + C_K4) 

    return two_e_wfs + (p.dt/6)*(K1[0] + 2*K2[0] + 2*K3[0] + K4[0]), \
            R_wfs + (p.dt/6)*(K1[1] + 2*K2[1] + 2*K3[1] + K4[1]), pos_e1, pos_e2, pos_R, C
#}}}


#{{{ 2e cwf
#NOTE that H is defined above as -1j*H AKA the time derivative
@jit( nogil=True)
def propagate_2e_hermitian_cwf(psi0, tf_tot, tf_laser, Unique, plotDensity):
    #.5dt because for RK4 need E value at half timestep increments
    #this means that E_form has twice as many elements as there are universal timesteps
    E_form = f.shape_E(tf_laser, .5*p.dt, p.Amplitude, p.form)
    e1_mesh, e2_mesh, R_mesh = initialize_2e_mesh(p.threshold,psi0,Unique)

    #call("rm ./dump/2e/*",shell=True)
    call(['cp', 'params.py', './dump/2e/README'])
    call(['cp', 'run.py', './dump/2e/run_README'])

    two_e_wfs = psi0[:,:, R_mesh].transpose()
    R_wfs = psi0[e1_mesh, e2_mesh, :]
    pos_e1 = r1x_a[e1_mesh]
    pos_e2 = r2x_a[e2_mesh]
    pos_R = Rx_a[R_mesh]

    if (plotDensity==True):
        plt.ion()
        fig1, (ax_e, ax_n, ax_e_mesh, ax_n_mesh) = plt.subplots(4,1)
        plt.pause(.0001)
        fig_rho_e, = ax_e.plot(p.r1x, cwf_2e_density(two_e_wfs),'.')
        fig_rho_n, = ax_n.plot(p.Rx,cwf_nuclear_density(R_wfs),'.')
        fig_e_mesh, =ax_e_mesh.plot(r1x_a[e1_mesh],r2x_a[e2_mesh], '.')
        fig_n_mesh, =ax_n_mesh.plot(Rx_a[R_mesh],'.')

    curr_time_index = 0
    curr_time = curr_time_index*p.dt
    while (curr_time < tf_tot):
        if(curr_time < tf_laser-p.dt):
            two_e_wfs, R_wfs, pos_e1, pos_e2, pos_R = T_dep_RK4_2e(two_e_wfs, R_wfs, e1_mesh, \
                                      e2_mesh, R_mesh, pos_e1, pos_e2, pos_R,\
                                      curr_time_index, E_form)
        else:
            two_e_wfs, R_wfs, pos_e1, pos_e2, pos_R = RK4_2e(two_e_wfs, R_wfs, \
                            e1_mesh,e2_mesh, R_mesh,\
                            pos_e1, pos_e2, pos_R)

        curr_time_index += 1
        curr_time = curr_time_index*p.dt

        e1_mesh  = f.find_index(r1x_a, pos_e1)
        e2_mesh = f.find_index(r2x_a, pos_e2 )
        R_mesh = f.find_index(Rx_a, pos_R )

        if(curr_time_index%p.plot_step==0):
            np.save('./dump/2e/'+'2e-cwf-'+str(num_trajs)+'-'+"{:.6f}".format(curr_time), two_e_wfs)
            np.save('./dump/2e/'+'R-cwf-'+str(num_trajs)+'-'+"{:.6f}".format(curr_time), R_wfs)
            print("{:.1f}".format(100*curr_time/tf_tot)+'% done')
            """
            if(plotDensity==True):
                plt.pause(.001)
                fig_rho_e.set_ydata(cwf_2e_density(two_e_wfs))
                if(BHF==False):
                    fig_rho_n.set_ydata(cwf_nuclear_density(R_wfs))
                if(BHF==True):
                    fig_rho_n.set_ydata(cwf_nuclear_density_BHF(R_wfs))
                fig_e_mesh.set_ydata(r2x_a[e2_mesh])
                fig_e_mesh.set_xdata(r1x_a[e1_mesh])
                fig_n_mesh.set_ydata(Rx_a[R_mesh])
                ax_e.relim()
                ax_e.autoscale()
                ax_n.relim()
                ax_n.autoscale()
                ax_e_mesh.relim()
                ax_e_mesh.autoscale()
                ax_n_mesh.relim()
                ax_n_mesh.autoscale()
                fig1.suptitle('Time ' + str(curr_time)) #+ ' Amplitude ' + str(p.Amplitude))
                fig1.canvas.draw()
                fig1.canvas.flush_events
            """
    plt.show()

#}}}


@jit( nogil=True)
def initialize_C(psi, psi_el, psi_R, num_trajs):
    num_trajs = el1.shape[0]
    M = np.zeros((num_trajs,num_trajs)) + 0j
    G = np.zeros(num_trajs) + 0j
    C = np.zeros(num_trajs) + 0j
    r_integral = np.zeros(R_dim) + 0j
    for i in range(num_trajs):
        integral = 0j
        for Ri in range(R_dim):
            #r_integral[Ri] = np.sum(np.sum(np.conj(psi_el[i,:,:])*psi[:,:,Ri],0),0)
            for r2i in range(r2_dim):
                integral += np.sum(np.conj(psi_el[i,:,r2i])*np.conj(psi_R[i,Ri])*psi[:,r2i,Ri])
        G[i] = integral
        for j in range(i,num_trajs):
            M[i,j] = np.sum(np.sum(psi_el[i,:,:]*np.conj(psi_el[j,:,:])))*np.sum(np.conj(psi_R[j])*psi_R[i])
            M[j,i] = np.conj(M[i,j])
    G *= p.n_spacing*p.e_spacing**2
    M *= p.n_spacing*p.e_spacing**2
   
    C = np.dot(np.linalg.pinv(M),G)
    return C

#{{{ 2e cwf C_dots NEED TO BE UPDATED
#This double counts the interactions by using Vfull!!!!
@njit( parallel=True,nogil=True)
def C_dot(Ct,psi_el,psi_R, e1_mesh, e2_mesh, R_mesh):
    num_trajs = psi_el.shape[0]
    M_e = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    M_R = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    M = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    W = np.zeros((num_trajs,num_trajs),dtype=np.complex128)
    W1 = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    W2 = np.zeros((num_trajs,num_trajs),dtype=np.complex128)
    #because of the way that numba's parallelization works, it can't call numpy.sum twice on 
    # complex arrays, thus there need to be intermediate arrays 
    sum_array = np.zeros((psi_el[0,0].shape[0]),dtype=np.complex128)
    R_array = np.zeros(psi_R[0].shape[0])+0j
    for i in prange(num_trajs):
        for j in prange(i,num_trajs):
            #integrate over one electron cooerdinate
            sum_array = np.sum(psi_el[j,:,:]*np.conj(psi_el[i,:,:]),0)
            #integrate over the other one
            M_e[i,j] = np.sum(sum_array)
            #integrate over nuclear coordinates
            M_R[i,j] = np.sum(np.conj(psi_R[i])*psi_R[j])
            #product of the integrals
            M[i,j] = M_e[i,j]*M_R[i,j]
    for i in prange(num_trajs):
        for j in prange(num_trajs):
            if(j<i):
                M[i,j] = np.conj(M[j,i])
                M_e[i,j] = np.conj(M_e[j,i])
                M_R[i,j] = np.conj(M_R[j,i])
            ##Full potential integral
            #   integrate potential w/rt one electron coordinate
            for Ri in range(R_array.shape[0]):
                sum_array = np.sum(psi_el[j]*np.conj(psi_el[i])*Vfull[:,:,Ri],0)
                #   integrate w/rt the other electron coordinate
                R_array[Ri] = np.sum(sum_array)
            #   integrate w/rt the nuclear coordinates
            W[i,j] = np.sum(R_array*psi_R[j]*np.conj(psi_R[i]))

            ##electron potential Intgral
            #   integrate w/rt one electron coordinate
            sum_array = np.sum(psi_el[j,:,:]*np.conj(psi_el[i,:,:])*V2e[R_mesh[i],:,:],0)
            # integrate e/rt the other electron coordinate and multiply by nuclear CWF norm
            W1[i,j] = np.sum(sum_array)*M_R[i,j]

            ##Nuclear Potential Integral
            W2[i,j] = M_e[i,j]*np.sum(psi_R[j]*np.conj(psi_R[i])*VR[e1_mesh[i],e2_mesh[i],:])

    M*=p.e_spacing**2*p.n_spacing
    W*=p.e_spacing**2*p.n_spacing
    W1*=p.e_spacing**2*p.n_spacing
    W2*=p.e_spacing**2*p.n_spacing

    return (-1j*np.dot(np.linalg.pinv(M),np.dot(W-W1-W2,Ct)))

        

#time dependent version of the above, for external laser field
@njit( parallel=True,nogil=True)
def C_dot_t(Ct,psi_el,psi_R, e1_mesh, e2_mesh, R_mesh,Et):
    num_trajs = psi_el.shape[0]
    M_e = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    M_R = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    M = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    W = np.zeros((num_trajs,num_trajs),dtype=np.complex128)
    W1 = np.zeros((num_trajs,num_trajs),dtype=np.complex128) 
    W2 = np.zeros((num_trajs,num_trajs),dtype=np.complex128)
    #because of the way that numba's parallelization works, it can't call numpy.sum twice on 
    # complex arrays, thus there need to be intermediate arrays 
    sum_array = np.zeros((psi_el[0,0].shape[0]),dtype=np.complex128)
    R_array = np.zeros(psi_R[0].shape[0])+0j
    for i in prange(num_trajs):
        for j in prange(i,num_trajs):
            #integrate over one electron cooerdinate
            sum_array = np.sum(psi_el[j,:,:]*np.conj(psi_el[i,:,:]),0)
            #integrate over the other one
            M_e[i,j] = np.sum(sum_array)
            #integrate over nuclear coordinates
            M_R[i,j] = np.sum(np.conj(psi_R[i])*psi_R[j])
            #product of the integrals
            M[i,j] = M_e[i,j]*M_R[i,j]
    for i in prange(num_trajs):
        for j in prange(num_trajs):
            if(j<i):
                M[j,i] = np.conj(M[i,j])
                M_e[j,i] = np.conj(M_e[i,j])
                M_R[j,i] = np.conj(M_R[i,j])
            ##Full potential integral
            #   integrate potential w/rt one electron coordinate
            for Ri in range(R_array.shape[0]):
                sum_array = np.sum(psi_el[i]*np.conj(psi_el[j])*(Vfull[:,:,Ri]+Et*epos),0)
                #   integrate w/rt the other electron coordinate
                R_array[Ri] = np.sum(sum_array)
            #   integrate w/rt the nuclear coordinates
            W[i,j] = np.sum(R_array*psi_R[i]*np.conj(psi_R[j]))

            ##electron potential Intgral
            #   integrate w/rt one electron coordinate
            sum_array = np.sum(psi_el[i,:,:]*np.conj(psi_el[j,:,:])*V2e[R_mesh[i],:,:],0)
            # integrate e/rt the other electron coordinate and multiply by nuclear CWF norm
            W1[i,j] = np.sum(sum_array)*M_R[i,j]

            ##Nuclear Potential Integral
            W2[i,j] = M_e[i,j]*np.sum(psi_R[i]*np.conj(psi_R[j])*VR[e1_mesh[i],e2_mesh[i],:])

    M*=p.e_spacing**2*p.n_spacing
    W*=p.e_spacing**2*p.n_spacing
    W1*=p.e_spacing**2*p.n_spacing
    W2*=p.e_spacing**2*p.n_spacing

    return (-1j*np.dot(np.linalg.pinv(M),np.dot(W-W1-W2,Ct)))
#}}}

def initialize_test(psi, num_trajs):
    e1_mesh, e2_mesh, R_mesh = initialize_mesh(p.threshold,psi,True, num_trajs)
    pos_e1 = p.r1x_array[e1_mesh]
    pos_e2 = p.r1x_array[e2_mesh]
    pos_R = p.r1x_array[R_mesh]
    el = psi[:,:,R_mesh].transpose()
    R = psi[e1_mesh, e2_mesh,:]
    C = initialize_C(psi,el,R,num_trajs)
    return e1_mesh, e2_mesh, R_mesh, pos_e1, pos_e2, pos_R, el, R, C
