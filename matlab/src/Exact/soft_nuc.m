%electron nuclear interaction
function [ pot ] = soft_nuc( e_ax,n_ax,n_mass)
%SOFT_NUC Takes electron axis and nuclear axis and returns potential in com
%frame
%   returns -1/sqrt((e_ax + M2/mu * R_ax).^2 _+.1) - 1./sqrt((e_ax - M1/mu *
%   R_ax).^2 +.1)
    global n_mass
    pot = -1./sqrt((e_ax + n_mass/(2*n_mass) * n_ax).^2 +1) - 1./sqrt((e_ax - n_mass/(2*n_mass) * n_ax).^2 +1);


