% Solving the TDSE with Runge Kutta:
fprintf('Solving the TDSE... \n');
time_index = 0;

%PES = load('PES.txt');
%PES = reshape(PES,Dim_nuc,Dim_ele,Dim_ele);

phi = phi_ini;

time_start = clock;
time_step = time_start;
D = [];
R = [];
for t = 1:1000
    if (mod(t,time_int) == 0 || t == 1)
        fprintf('Estimated Total Time: %18.6f \n',etime(clock(), time_step)*num_saved_points)
        fprintf('Elapsed Time: %18.6f \n',etime(clock(), time_start))
        fprintf('\n')  
        
        time_step = clock;
        %saving_data
        %imagesc(reshape(abs(phi_ini).^2,Dim_ele,Dim_ele))
        
    end
    if(unmapped && relax)
        J_Propagate_true_exact_relax
    end
    if(unmapped && ~relax)
        J_Propagate_true_exact
    end
    if(mapped && ~relax)
        J_Propagate_mapped_exact
    end
    if(mapped && relax)
        J_Propagate_mapped_exact_relax
    end

    D = [D real(-1*phi'*(e_position_s_1 + e_position_s_2)*phi*dx_e^2*dx_n)];
    R = [R real(phi_ini'*(n_position_s_1)*phi_ini*dx_e^2*dx_n)];
    
    path = './exact_dump/';
    save(strcat(path,'D',int2str(size(pool,2))),'D','-ascii');
    save(strcat(path,'R',int2str(size(pool,2))),'R','-ascii');
    
    str = strcat(path,'xe1_exact',int2str(t),'.txt'); xe1_exact = fopen(str,'w');
    str = strcat(path,'xe2_exact',int2str(t),'.txt'); xe2_exact = fopen(str,'w');
    str = strcat(path,'xn_exact',int2str(t),'.txt'); xn_exact = fopen(str,'w');
    str = strcat(path,'ve1_exact',int2str(t),'.txt'); ve1_exact = fopen(str,'w');
    str = strcat(path,'ve2_exact',int2str(t),'.txt'); ve2_exact = fopen(str,'w');
    str = strcat(path,'vn_exact',int2str(t),'.txt'); vn_exact = fopen(str,'w');
    fwrite(xe1_exact,xe1,'double');
    fwrite(xe2_exact,xe2,'double');
    fwrite(xn_exact,xn,'double');
    fwrite(ve1_exact,ve1,'double');
    fwrite(ve2_exact,ve2,'double');
    fwrite(vn_exact,vn,'double');
    fclose(xe1_exact);
    fclose(xe2_exact);
    fclose(xn_exact);
    fclose(ve1_exact);
    fclose(ve2_exact);
    fclose(vn_exact);
    str = strcat(path,'r_exact_wavepacket',int2str(time_index),'.txt'); real_wavepacket = fopen(str,'w');
    fwrite(real_wavepacket,real(phi(:)),'double');
    fclose(real_wavepacket);

    str = strcat(path,'i_exact_wavepacket',int2str(time_index),'.txt'); imag_wavepacket = fopen(str,'w');
    fwrite(imag_wavepacket,imag(phi(:)),'double');
    fclose(imag_wavepacket);


end
fprintf('Total Scho Time: %18.6f \n',etime(clock(), time_start));


