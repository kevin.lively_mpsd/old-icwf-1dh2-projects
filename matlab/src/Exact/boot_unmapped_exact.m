%% Load up precalculated quantites, necessary to start H_exact

clear all;
close all;
%delete *.txt

%% ---------%
%   MODEL   %
% ----------%
IPW = false;
run_gpu = false;
mapped = false;
unmapped = ~mapped;
relax=false;

n_mass = 1836;%1;%1836;          % 1.67E-27;
e_mass = 1;            % 9.1E-31; 

mu_n = n_mass/2;
m1 = n_mass;
m2 = n_mass;
mu_e = 2*n_mass/(2*n_mass + 1);

Dim_ele = 201;
Dim_nuc = 201;
dx_e = 0.6*ones(Dim_ele,1);
dx_e(50:150,1) = 0.2;

dx_n = 0.0625;       % 5.29E-11 = 1Bohr
dt = 0.02; %0.1           % 2.4E-17s

t_end = 30;          %femtoseconds
t_end = t_end*1E-15/(dt*2.4E-17);

num_saved_points = 200;
time_int = floor(t_end/num_saved_points)+1;
num_saved_points = floor(t_end/time_int);
aux = (1:time_int:t_end)*dt*2.4E-17;
save time.txt aux -ascii
clear aux


% TDSE CONSTANTS %
e_cte_kinetic = -1/(2*mu_e);
n_cte_kinetic = -1/(2*mu_n);

% OPTIONS:
save_data = true;
plot_PES = false;
plot_STATE0 = false;

%Sampling
threshold = 1E-10;

% Wave Function Propagation:
euler = false;
runge_kutta = true;
splitting_op = false;

figure_index = 1;

if(relax==true)
    B_set_operators_excited
else
    B_set_operators
end
set_wf =true;
if(set_wf==true)
    C_set_initial_wavefunction
end


phi_ini = load('phi_en_ini.txt');
BOPES = load('BOPES_ordered.txt');
xn_axis = (dx_n:dx_n:201*dx_n).';
red_n_gr = sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele^2),2)*dx_e^2;
min_R = xn_axis(red_n_gr == max(red_n_gr));
mesh_gr = floor(min_R/dx_n - xn_axis(1)/dx_n) + 1;

omega = BOPES(mesh_gr,2) - BOPES(mesh_gr,1);

rho_app = sum(abs(reshape(phi_ini,Dim_nuc,Dim_ele,Dim_ele)).^2,3)*dx_e;

nvec = 4;
ah(2,1) = 0.5;
ah(3,2) = 0.5;
ah(4,3) = 1.0;
bh(1) = 1.0/6.0;
bh(2) = 1.0/3.0;
bh(3) = 1.0/3.0;
bh(4) = 1.0/6.0;
ch(1) = 0.0;
ch(2) = 0.5;
ch(3) = 0.5;
ch(4) = 1.0;

E0 = 0.2235;
t_arrow = 0:t_end-1;
duration = 10*1E-15/(dt*2.4E-17);
envelope = sin(pi*t_arrow/duration).^2; 
envelope(floor(duration):end) = 0;
Efield = E0*envelope.*cos(omega*dt*t_arrow);
Efield(end+1) = Efield(end); 

eta = 0.01;
W=zeros(size(xe_axis));
L_ind = 30;
W(end-L_ind:end) = -1i*eta*(xe_axis(end-L_ind:end) - xe_axis(end-L_ind)).^2;
W(1:L_ind+1) = -1i*eta*(xe_axis(1:L_ind+1) - xe_axis(L_ind+1)).^2;

e_unit_nx = speye(Dim_ele);
n_unit_nx = speye(Dim_nuc);
Wfull = kron(diag(W),kron(e_unit_nx,n_unit_nx)) + kron(e_unit_nx,kron(diag(W),n_unit_nx));
hamiltonian = hamiltonian + Wfull;

clear e_unit_nx; clear n_unit_nx;
