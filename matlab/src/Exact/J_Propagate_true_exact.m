%% Direct Integration Bohmian Trajectory Evolution:
% ------------------------------------------------        
      
four_point_stencil = imag(e_gradent_s_1*phi./phi);
ve1 = real(four_point_stencil((mesh_e1 - 1)*Dim_nuc*Dim_ele + (mesh_e2 - 1)*Dim_nuc + mesh_n)/mu_e).';

four_point_stencil = imag(e_gradent_s_2*phi./phi);
ve2 = real(four_point_stencil((mesh_e1 - 1)*Dim_nuc*Dim_ele + (mesh_e2 - 1)*Dim_nuc + mesh_n)/mu_e).';

four_point_stencil = imag(n_gradent_s_1*phi./phi);
vn = real(four_point_stencil((mesh_e1 - 1)*Dim_nuc*Dim_ele + (mesh_e2 - 1)*Dim_nuc + mesh_n)/mu_n).';

xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
xe1 = xe1 + dt*ve1.';
xe2 = xe2 + dt*ve2.';
xn = xn + dt*vn.';

indices = (xe1 < xe_axis(1));
xe1(indices) = xe1_old(indices);
indices = (xe1 > xe_axis(end));
xe1(indices) = xe1_old(indices);

indices = (xe2 < xe_axis(1));
xe2(indices) = xe2_old(indices);
indices = (xe2 > xe_axis(end));
xe2(indices) = xe2_old(indices);

indices = (xn < xn_axis(1));
xn(indices) = xn_old(indices);
indices = (xn > xn_axis(end));
xn(indices) = xn_old(indices);

mesh_e1 = floor(xe1/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2 = floor(xe2/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n = floor(xn/dx_n - xn_axis(1)/dx_n) + 1;


%% WaveFunction Propagation:
% --------------------------
AEfield = (Efield(t+1)-Efield(t));

ysize = size(phi);
kv = zeros(ysize(1),nvec);
for jj =1:nvec
    y0 = phi;
    for kk = 1:jj-1
        y0 = y0 + ah(jj,kk)*kv(:,kk);
    end
    kv(:,jj) = -(dt*1i)*(hamiltonian - (e_position_s_1 + e_position_s_2)*(Efield(t)+ch(jj)*AEfield))*y0;
end
y1 = phi;
for jj =1:nvec
    y1 = y1 + bh(jj)*kv(:,jj);
end
phi = y1;


if sqrt(sum(abs(phi(:)).^2)*dx_e^2*dx_n) > 1.01 || sqrt(sum(abs(phi(:)).^2)*dx_e^2*dx_n) < 0.99
    fprintf('norm out of control')
    disp(sqrt(sum(abs(phi(:)).^2)*dx_e^2*dx_n))
    stop
end


