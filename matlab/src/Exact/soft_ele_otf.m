%electron nuclear interaction on the fly for the electrons
function [ pot ] = soft_ele_otf(n_pos, Dim_ele,xe_axis,N_traj)
%SOFT_NUC Takes electron axis and nuclear axis and returns potential in com
%frame
%   returns -1/sqrt((e_ax + M2/mu * R_ax).^2 _+.1) - 1./sqrt((e_ax - M1/mu *
%   R_ax).^2 +.1)
    n_mass=1836;
    pot = zeros(Dim_ele,N_traj);
    for i=1:size(n_pos)
        pot(:,i) = -1./sqrt((xe_axis + n_mass/(2*n_mass) * n_pos(i)).^2 +1) - 1./sqrt((xe_axis - n_mass/(2*n_mass) * n_pos(i)).^2 +1);
    end

