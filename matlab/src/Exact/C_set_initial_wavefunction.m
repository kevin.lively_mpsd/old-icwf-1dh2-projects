% Defining the Initial Wavefunction:

fprintf('\n \n'); fprintf('Defining the Initial Wavefunction... \n');

[eigS,eigV] = eigs(hamiltonian,1,'SA');
phi_ini = eigS(:,1)/sqrt(sum(abs(eigS(:,1)).^2)*dx_e^2*dx_n);
rho_ini = sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3)*dx_e;


eta = 1E-12;%1E-8
epsilon = 0;%1E-6;
alpha = 1E-2;%1E-3;
clear error
error(1) = 1;
VH = zeros(Dim_nuc,Dim_ele,Dim_ele);
VH_1 = zeros(Dim_nuc,Dim_ele,Dim_ele);
VH_2 = zeros(Dim_nuc,Dim_ele,Dim_ele);
VH_aux = zeros(Dim_nuc,Dim_ele,Dim_ele);
rho_app = zeros(Dim_nuc,Dim_ele);
i = 1;
while error(i) > eta
    disp(error(i))
    i = i + 1
    % UPDATE KS-HAMILTONIAN
    PES_aux = Vn - Ve1n - Ve2n;
    hamiltonian_aux = e_cte_kinetic*e_lapl_s_1 + e_cte_kinetic*e_lapl_s_2 + ...
        n_cte_kinetic*n_lapl_s_1 + PES_aux + ...
        spdiags(VH(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);

    [eigS,eigV] = eigs(hamiltonian_aux,1,'SA');
    phi_app = eigS(:,1)/sqrt(sum(abs(eigS(:,1)).^2)*dx_e^2*dx_n);
    rho_old = rho_app;
    rho_app = sum(reshape(abs(phi_app).^2,Dim_nuc,Dim_ele,Dim_ele),3)*dx_e;
    
    error(i) = sum(sum(abs(rho_app - rho_old).^2)*dx_e)*dx_n;
    if error(i) > eta 

        VH_aux = zeros(Dim_nuc,Dim_ele,Dim_ele);
        for kk = 1:Dim_ele
            for ii = 1:Dim_ele
                for jj = 1:Dim_nuc
                    VH_aux(jj,ii,kk) = rho_app(jj,kk)*soft_coulomb(full(xe_axis(kk) - xe_axis(ii)),soft_ee);
                end
            end
        end
%         VH_aux = repmat(sum(sum(VH_aux,3),1)*dx_n*dx_e,Dim_nuc,1);
        VH_aux = sum(VH_aux,3)*dx_e./repmat((sum(rho_app,2)*dx_e),1,Dim_ele);
        for kk = 1:Dim_ele
            for ii = 1:Dim_ele
                for jj = 1:Dim_nuc           
                    VH_1(jj,ii,kk) = VH_aux(jj,ii);
                    VH_2(jj,ii,kk) = VH_aux(jj,kk);
                end
            end
        end
        VH = ((1-error(i))/(1+error(i)))*0.5*(VH_1 + VH_2);
    end   
%    figure(4)
%    plot(error,'ob')
%     
%    figure(3)
%    pcolor(xe_axis,xn_axis,rho_ini)
%    shading interp   
%    
%    figure(2)
%    pcolor(xe_axis,xn_axis,rho_app)
%    shading interp
%    
%    figure(1)
%    pcolor(xe_axis,xn_axis,VH_aux)
%    shading interp  
%    
%    pause
end


if save_data
    aux = phi_ini;
    save phi_en_ini.txt aux -ascii
    clear aux

    aux = VH_aux;
    save VKS.txt aux -ascii
    clear aux
    
    aux = phi_app;
    save phi_en_KS.txt aux -ascii
    clear aux  
    
    aux = error;
    save error_KS.txt aux -ascii
    clear aux        
end

BOPES = load('BOPES.txt');
red_n_gr = sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele^2),2)*dx_e^2;
min_R = xn_axis(red_n_gr == max(red_n_gr));
mesh_gr = floor(min_R/dx_n - xn_axis(1)/dx_n) + 1;

omega = BOPES(mesh_gr,2) - BOPES(mesh_gr,1);

E0 = 0.002;
t_arrow = 0:t_end-1;
duration = 20*1E-15/(dt*2.4E-17);
envelope = sin(pi*t_arrow/duration).^2; 
envelope(floor(duration):end) = 0;
Efield = E0*envelope.*cos(omega*dt*t_arrow);
Efield(end+1) = Efield(end); 
    
disp('omega')
disp(omega) 
disp('E0')
disp(E0)
disp('duration')
disp(duration)



phi_ini = phi_app;

