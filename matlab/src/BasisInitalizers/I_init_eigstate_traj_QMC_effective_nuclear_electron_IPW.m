%% Getting initial wavefunctions from eigenstates of the conditional hermitian wavefunction

xe1 = xe1_ini;
xe2 = xe2_ini;
xn = xn_ini;
xe1_old = xe1_ini;
xe2_old = xe2_ini;
xn_old = xn_ini;
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;
ve1_old = ve1;
ve2_old = ve2;
vn_old = vn;
mesh_e1 = mesh_e1_ini;
mesh_e2 = mesh_e2_ini;
mesh_n = mesh_n_ini;

e_gauss = -1.5*exp(-xe_axis.^2) + exp(-(xe_axis - 1).^2) + exp(-(xe_axis + 1).^2);
e_gauss = e_gauss./sqrt(sum(abs(e_gauss).^2)*dx_e);

phi_n = zeros(Dim_nuc,N_traj);
phi_e1 = zeros(Dim_ele,N_traj);
%phi_e2 = zeros(Dim_ele,N_traj);
for i=1:N_traj
    phi_e1(:,i) = e_gauss;
    %phi_e2(:,i) = e_gauss;
end

VH = Vee*(abs(phi_e1).^2)*dx_e;
red_n = squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3),2))*dx_e^2;

sign = zeros(N_traj,1);
Zn = zeros(N_traj,1);

Ve_n  = zeros(Dim_ele,N_traj);

[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn,xe_axis);
Ve_nbare  = (-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en));
%tic;
[sign,m,a] = mygaussfit(xn_axis,red_n,0);
sign = abs(sign);
for i=1:N_traj

    Zn(i) = sum(exp(-(xn-xn(i)).^2)/(abs(sign)^2));

    Ve_n(:,i)  = (1/Zn(i))*sum(repmat(exp(-(xn-xn(i)).^2/(sign^2)).',Dim_ele,1).*Ve_nbare,2);
end


prop_e = VH + Ve_n;


%SCF initial cycle for electrons
for i=1:N_traj
    [eigstate_old, eigval] = eigs(diag(prop_e(:,i)) + e_T,1,'sa');
    eigstate_old = eigstate_old(:,1)./sqrt(sum(abs(eigstate_old(:,1)).^2)*dx_e);
    h_eig = (diag(prop_e(:,i)) + e_T)*eigstate_old;
    r1 = (h_eig - (eigstate_old'*h_eig*dx_e)*eigstate_old);
    r2 = phi_e1(:,i) - eigstate_old;
    eigstate = eigstate_old;
    mag_r1 = sqrt(sum(abs(r1).^2)*dx_e);
    mag_r2 = sqrt(sum(abs(r2).^2)*dx_e);
%     r = phi_e1(:,i) - eigstate_old;
%     eigstate = eigstate_old;
%     mag_r = sum(abs(r).^2)*dx_e;
    loop = 0;
    while(mag_r1 > 1E-5 || mag_r2 > 1E-5)
        eigstate_old = eigstate;
        %phi_e1 = phi_e2 initially so .5 -> .5 *2eigstate = eigstate
        VH = (Vee*(abs(eigstate(:,1)).^2)*dx_e);
        [eigstate, eigval] = eigs(diag(VH  + Ve_n(:,i)) + e_T,1,'sa');
        eigstate = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
%        eigstate = 0.1*eigstate + 0.9*eigstate_old;
%        eigstate = eigstate./sqrt(sum(abs(eigstate).^2)*dx_e);
        h_eig = (diag(VH  + Ve_n(:,i)) + e_T)*eigstate;
        r1 = (h_eig - (eigstate'*h_eig*dx_e)*eigstate);
        mag_r1 = sqrt(sum(abs(r1).^2)*dx_e);
        r2 = eigstate - eigstate_old;
        mag_r2 = sqrt(sum(abs(r2).^2)*dx_e);
        %r = eigstate - eigstate_old;
        %mag_r = sum(abs(r).^2)*dx_e;
%         plot(abs(eigstate).^2)
%         pause(0.0000001)
%         clf
%         fprintf('magnitue error %f\n',mag_r)
%         fprintf('magnitude error 1 %f, ',mag_r1)
%         fprintf('magnitude error 2 %f\n',mag_r2)
        loop=loop+1;
        if(loop>1000)
            mesh_n(i) = mcsampling(sqrt(red_n),1,threshold);
            rand_vals = rand(1,1);
            xn(i) = (mesh_n(i)-1)*dx_n + xn_axis(1) + (rand_vals - 0.5)*dx_n;

            VH = Vee*(abs(e_gauss).^2)*dx_e;
            prop_e(:,i) = VH + Ve_n(:,i);
            loop=0;
        end
    end
    phi_e1(:,i) = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
    %phi_e2(:,i) = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
    %i
end

%Solve Nuclei for that SCF of electron states w/ QMC potentials
%sig1 = zeros(N_traj,1);
% sign = zeros(N_traj,1);
Z1 = zeros(N_traj,1);
Z2 = zeros(N_traj,1);
Vn_e1 = zeros(Dim_nuc,N_traj);
Vn_e2 = zeros(Dim_nuc,N_traj);

[e1_pos_grid1, n1_axis1] = meshgrid(xe1,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2,(-(m2)/(m1+m2))*xn_axis);
Vn1_bare = (-soft_coulomb(e1_pos_grid1 - n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2 - n1_axis2,soft_en));
Vn2_bare = (-soft_coulomb(e2_pos_grid1 - n2_axis1,soft_en) -soft_coulomb(e2_pos_grid2 - n2_axis2,soft_en));
%tic;
[sig1,m,aa] = mygaussfit(xe_axis,mean(abs(phi_e1).^2,2),0);
for i=1:N_traj
    %[sig1(i),m,a] = mygaussfit(xe_axis,abs(phi_e1(:,i)).^2,0);
    %[sign(i),m,a] = mygaussfit(xn_axis,abs(phi_n(:,i)).^2,0);

    Z1(i) = sum(exp(-(xe1-xe1(i)).^2/(sig1^2)));
    Z2(i) = sum(exp(-(xe2-xe2(i)).^2/(sig1^2)));

    Vn_e1(:,i) = (1/Z1(i))*sum(repmat(exp(-(xe1-xe1(i)).^2/(sig1^2)).',Dim_nuc,1).*Vn1_bare,2);
    Vn_e2(:,i) = (1/Z2(i))*sum(repmat(exp(-(xe2-xe2(i)).^2/(sig1^2)).',Dim_nuc,1).*Vn2_bare,2);
end

Vns = Vn_e1 + Vn_e2;
pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 

red_e = squeeze(sum(sum(reshape(abs(phi_ini(:)).^2,Dim_nuc,Dim_ele,Dim_ele),1),2)*dx_e*dx_n);
for i=1:N_traj
    attempt=true;
%     while(attempt==true)
        [R, R_e] = eig(diag(pot_prop_n(:,i)) + n_T);
        phi_n(:,i) = R(:,1)./sqrt(sum(abs(R(:,1)).^2)*dx_n);
        if(mean(phi_n(:,i))<0)
            phi_n(:,i) = -1*phi_n(:,i);
        end
%         [m,ind] = max(abs(phi_n(:,i)));
%         if(ind<100)
%             attempt = false;
%         else
%             mesh_e1(i) = mcsampling(sqrt(red_e),1,threshold);
%             mesh_e2(i) = mcsampling(sqrt(red_e),1,threshold);
%             rand_vals = rand(1,1);
%             xe1(i) = (mesh_e1(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
%             rand_vals = rand(1,1);
%             xe2(i) = (mesh_e2(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
%             Vns(:,i) = soft_nuc_otf(xe1(i), Dim_nuc, xn_axis, 1) + soft_nuc_otf(xe2(i), Dim_nuc, xn_axis, 1);
%             pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 
%         end
%     end
end

%Redo electrons for the QMC initalized nuclei
VH = Vee*(abs(phi_e1).^2)*dx_e;
red_n = squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3),2))*dx_e^2;

Zn = zeros(N_traj,1);

Ve_n  = zeros(Dim_ele,N_traj);

[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn,xe_axis);
Ve_nbare  = (-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en));
%tic;

for i=1:N_traj
    %[sign(i),m,a] = mygaussfit(xn_axis,abs(phi_n(:,i)).^2,0);
    
    Zn(i) = sum(exp(-(xn-xn(i)).^2)/(sign^2));

    Ve_n(:,i)  = (1/Zn(i))*sum(repmat(exp(-(xn-xn(i)).^2/(sign^2)).',Dim_ele,1).*Ve_nbare,2);
end

prop_e = VH + Ve_n;



%SCF cycle for electrons
for i=1:N_traj
    [eigstate_old, eigval] = eigs(diag(prop_e(:,i)) + e_T,1,'sa');
    eigstate_old = eigstate_old(:,1)./sqrt(sum(abs(eigstate_old(:,1)).^2)*dx_e);
    h_eig = (diag(prop_e(:,i)) + e_T)*eigstate_old;
    r1 = (h_eig - (eigstate_old'*h_eig*dx_e)*eigstate_old);
    r2 = phi_e1(:,i) - eigstate_old;
    eigstate = eigstate_old;
    mag_r1 = sqrt(sum(abs(r1).^2)*dx_e);
    mag_r2 = sqrt(sum(abs(r2).^2)*dx_e);
%     r = phi_e1(:,i) - eigstate_old;
%     eigstate = eigstate_old;
%     mag_r = sum(abs(r).^2)*dx_e;
    loop = 0;
    while(mag_r1 > 1E-5 || mag_r2 > 1E-5)
        eigstate_old = eigstate;
        %phi_e1 = phi_e2 initially so .5 -> .5 *2eigstate = eigstate
        VH = (Vee*(abs(eigstate(:,1)).^2)*dx_e);
        [eigstate, eigval] = eigs(diag(VH  + Ve_n(:,i)) + e_T,1,'sa');
        eigstate = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
%        eigstate = 0.1*eigstate + 0.9*eigstate_old;
%        eigstate = eigstate./sqrt(sum(abs(eigstate).^2)*dx_e);
        h_eig = (diag(VH  + Ve_n(:,i)) + e_T)*eigstate;
        r1 = (h_eig - (eigstate'*h_eig*dx_e)*eigstate);
        mag_r1 = sqrt(sum(abs(r1).^2)*dx_e);
        r2 = eigstate - eigstate_old;
        mag_r2 = sqrt(sum(abs(r2).^2)*dx_e);
        %r = eigstate - eigstate_old;
        %mag_r = sum(abs(r).^2)*dx_e;
%         plot(abs(eigstate).^2)
%         pause(0.0000001)
%         clf
%         fprintf('magnitue error %f\n',mag_r)
%         fprintf('magnitude error 1 %f, ',mag_r1)
%         fprintf('magnitude error 2 %f\n',mag_r2)
        loop=loop+1;
        if(loop>1000)
            mesh_n(i) = mcsampling(sqrt(red_n),1,threshold);
            rand_vals = rand(1,1);
            xn(i) = (mesh_n(i)-1)*dx_n + xn_axis(1) + (rand_vals - 0.5)*dx_n;

            VH = Vee*(abs(phi_e1(:,i)).^2)*dx_e;
            prop_e(:,i) = VH + Ve_n(:,i);
            loop=0;
        end
    end
    phi_e1(:,i) = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
    %phi_e2(:,i) = eigstate(:,1)./sqrt(sum(abs(eigstate(:,1)).^2)*dx_e);
    %i
end

%Redo nuclei for QMC initialized electrons

%Solve Nuclei for that SCF of electron states w/ QMC potentials
%sig1 = zeros(N_traj,1);
%sign = zeros(N_traj,1);
Z1 = zeros(N_traj,1);
Z2 = zeros(N_traj,1);
Vn_e1 = zeros(Dim_nuc,N_traj);
Vn_e2 = zeros(Dim_nuc,N_traj);

[e1_pos_grid1, n1_axis1] = meshgrid(xe1,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2,(-(m2)/(m1+m2))*xn_axis);
Vn1_bare = (-soft_coulomb(e1_pos_grid1 - n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2 - n1_axis2,soft_en));
Vn2_bare = (-soft_coulomb(e2_pos_grid1 - n2_axis1,soft_en) -soft_coulomb(e2_pos_grid2 - n2_axis2,soft_en));
%tic;
[sig1,m,aa] = mygaussfit(xe_axis,mean(abs(phi_e1).^2,2),0);
for i=1:N_traj
    %[sig1(i),m,a] = mygaussfit(xe_axis,abs(phi_e1(:,i)).^2,0);
    %[sign(i),m,a] = mygaussfit(xn_axis,abs(phi_n(:,i)).^2,0);

    Z1(i) = sum(exp(-(xe1-xe1(i)).^2/(sig1^2)));
    Z2(i) = sum(exp(-(xe2-xe2(i)).^2/(sig1^2)));

    Vn_e1(:,i) = (1/Z1(i))*sum(repmat(exp(-(xe1-xe1(i)).^2/(sig1^2)).',Dim_nuc,1).*Vn1_bare,2);
    Vn_e2(:,i) = (1/Z2(i))*sum(repmat(exp(-(xe2-xe2(i)).^2/(sig1^2)).',Dim_nuc,1).*Vn2_bare,2);
end

Vns = Vn_e1 + Vn_e2;
pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 

red_e = squeeze(sum(sum(reshape(abs(phi_ini(:)).^2,Dim_nuc,Dim_ele,Dim_ele),1),2)*dx_e*dx_n);
for i=1:N_traj
    attempt=true;
%     while(attempt==true)
        [R, R_e] = eig(diag(pot_prop_n(:,i)) + n_T);
        phi_n(:,i) = R(:,1)./sqrt(sum(abs(R(:,1)).^2)*dx_n);
        if(mean(phi_n(:,i))<0)
            phi_n(:,i) = -1*phi_n(:,i);
        end
%         [m,ind] = max(abs(phi_n(:,i)));
%         if(ind<100)
%             attempt = false;
%         else
%             mesh_e1(i) = mcsampling(sqrt(red_e),1,threshold);
%             mesh_e2(i) = mcsampling(sqrt(red_e),1,threshold);
%             rand_vals = rand(1,1);
%             xe1(i) = (mesh_e1(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
%             rand_vals = rand(1,1);
%             xe2(i) = (mesh_e2(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
%             Vns(:,i) = soft_nuc_otf(xe1(i), Dim_nuc, xn_axis, 1) + soft_nuc_otf(xe2(i), Dim_nuc, xn_axis, 1);
%             pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 
%         end
%     end
end

M = (phi_n'*phi_n).*((phi_e1'*phi_e1).^2)*dx_n*dx_e*dx_e;
G = zeros(N_traj,1);
for alpha = 1:N_traj
    G(alpha) = phi_n(:,alpha)'*(reshape((reshape(phi_ini,Dim_nuc*Dim_ele,Dim_ele)*conj(phi_e1(:,alpha))),Dim_nuc,Dim_ele)*conj(phi_e1(:,alpha)))*dx_n*dx_e*dx_e;
end
C = pinv(M,10^tol)*G;
C = C./sqrt(C'*M*C);

red_e = zeros(Dim_ele,1);
red_n = zeros(Dim_nuc,1);
phi_ee1 = phi_e1'*phi_e1*dx_e;
phi_nn = phi_n'*phi_n*dx_n;

phi_recon2 = zeros(Dim_nuc*Dim_ele^2,1);
for i = 1:N_traj
    red_n = red_n + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,N_traj)))*(conj(C).*phi_ee1(:,i).*phi_ee1(:,i));
    red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,N_traj)))*(conj(C).*phi_ee1(:,i).*phi_nn(:,i));
    phi_recon2 = phi_recon2 + C(i)*kron(phi_e1(:,i),kron(phi_e1(:,i),phi_n(:,i)));
end



xe1_ini = xe1;
xe2_ini = xe2;
xn_ini = xn;
xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
mesh_e1_ini = mesh_e1;
mesh_e2_ini = mesh_e2;
mesh_n_ini = mesh_n;
phi_ini = phi_ini(:);
four_point_stencil = imag(e_gradent_s_1*phi_ini./phi_ini);
ve1_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(e_gradent_s_2*phi_ini./phi_ini);
ve2_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(n_gradent_s_1*phi_ini./phi_ini);
vn_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_n);
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;
