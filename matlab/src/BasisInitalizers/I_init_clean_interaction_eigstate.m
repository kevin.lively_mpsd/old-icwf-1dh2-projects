%% Getting initial wavefunctions from eigenstates of the conditional hermitian wavefunction

xe1 = xe1_ini;
xe2 = xe2_ini;
xn = xn_ini;
xe1_old = xe1_ini;
xe2_old = xe2_ini;
xn_old = xn_ini;
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;
ve1_old = ve1;
ve2_old = ve2;
vn_old = vn;
mesh_e1 = mesh_e1_ini;
mesh_e2 = mesh_e2_ini;
mesh_n = mesh_n_ini;

phi_n = zeros(Dim_nuc,N_traj);
phi_e1 = zeros(Dim_ele,N_traj);
phi_e2 = zeros(Dim_ele,N_traj);

[x1_grid,x2_pos] = meshgrid(xe2,xe_axis);
prop_e1 = soft_coul(x1_grid,x2_pos,soft_ee) + soft_ele_otf(xn, Dim_ele, xe_axis, N_traj);

[x2_grid,x1_pos] = meshgrid(xe1,xe_axis);
prop_e2 = soft_coul(x2_grid,x1_pos,soft_ee) + soft_ele_otf(xn, Dim_ele, xe_axis, N_traj);

Vns = soft_nuc_otf(xe1, Dim_nuc, xn_axis, N_traj) + soft_nuc_otf(xe2, Dim_nuc, xn_axis, N_traj);

pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 

for i=1:N_traj
    attempt=true;
     while(attempt==true)
        [R, R_e] = eig(diag(pot_prop_n(:,i)) + n_T);
        phi_n(:,i) = R(:,1)./sqrt(sum(abs(R(:,1)).^2)*dx_n);
        [m,ind] = max(abs(phi_n(:,i)));
        if(ind<100)
            attempt = false;
        else
            mesh_e1(i) = mcsampling(sqrt(red_e),1,threshold);
            mesh_e2(i) = mcsampling(sqrt(red_e),1,threshold);
            rand_vals = rand(1,1);
            xe1(i) = (mesh_e1(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
            rand_vals = rand(1,1);
            xe2(i) = (mesh_e2(i)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
            Vns(:,i) = soft_nuc_otf(xe1(i), Dim_nuc, xn_axis, 1) + soft_nuc_otf(xe2(i), Dim_nuc, xn_axis, 1);
            pot_prop_n = repmat(Vnn,1,N_traj) + Vns; 
        end
     end
    if(mean(phi_n(:,i))<0)
        phi_n(:,i) = -1*phi_n(:,i);
    end
    
    [r1, r1_e] = eig(diag(prop_e1(:,i)) + e_T);
    phi_e1(:,i) = r1(:,1)./sqrt(sum(abs(r1(:,1)).^2)*dx_e);
    if(mean(phi_e1(:,i))<0)
        phi_e1(:,i) = -1*phi_e1(:,i);
    end
    
    [r2, r2_e] = eig(diag(prop_e2(:,i)) + e_T);
    phi_e2(:,i) = r2(:,1)./sqrt(sum(abs(r2(:,1)).^2)*dx_e);
    if(mean(phi_e2(:,i))<0)
        phi_e2(:,i) = -1*phi_e2(:,i);
    end
end

gamma_ad = 5E-4; %gamma adaptive, tolerance for accuracy in 1-<psi_l|psi_T>

% Construct full overlap matrix for usage through out.
S_full = ( (phi_e1'*phi_e1*dx_e) .* (phi_e2'*phi_e2*dx_e) .* (phi_n'*phi_n*dx_n) );
overlaps = zeros(N_traj,1);
for alpha = 1:N_traj
    overlaps(alpha) = phi_n(:,alpha)'*(reshape((reshape(phi_ini,Dim_nuc*Dim_ele,Dim_ele)*conj(phi_e1(:,alpha))),Dim_nuc,Dim_ele)*conj(phi_e2(:,alpha)))*dx_n*dx_e*dx_e;
end
overlaps = abs(overlaps);
G = overlaps;

%Now we assume that all the basis is inactive
active = [];

l=0;
eta = 1; %Error
phi_t = phi_ini(:);
while(eta>gamma_ad && l<N_traj); %exits when converging or running out of bases
    l=l+1;
    % Find the inactive basis function which has the greatest overlap with
    %  the target wavefunction, implicitly constructed from C_old, active_old
    %  and S_full
    [mval, ind] = max(overlaps);
    
    %Check if adding this basis would add immense redundency
    [U,S,V] = svd(S_full([active ind],[active ind]));
    %max(diag(S))/min(diag(S));
    if(max(diag(S))/min(diag(S))> 10^8)
        %suppress it for this phi_t
        overlaps(ind) = 0;
        continue
    end
        
    
    %Add this basis index to the active set.
    active = [active ind];
    
    %Invert the active overlap matrix . . .
    S_active_inv = inv(S_full(active,active));
    
    % in order to determine new active coefficients.
    %   C_i = sum_j=1^l Sinv_{ij} <phi_j | Cold_{a}|phi_{a}>  // sum over a
    %       = sum_j=1^l Sinv_{ij} S_active_old_{ja}Cold_{a}   // sum over a
    C = (S_active_inv)*G(active);

    %Normalize
    C = C./sqrt(C'*S_full(active,active)*C);
    
    %Check for convergence
    %   eta = 1 - <psi_l | psi_T>  Where |psi_l> is the current iteration's
    % wf    = 1 - C'*(S_full(active,active_old)*C_old)
    
    phi_recon = zeros(Dim_nuc*Dim_ele^2,1);
    for i=1:size(active,2)
        phi_recon = phi_recon + C(i)*kron(phi_e1(:,active(i)),kron(phi_e2(:,active(i)),phi_n(:,active(i))));
    end
    phi_recon = phi_recon./sqrt(sum(abs(phi_recon(:)).^2)*dx_e^2*dx_n);
    eta = 1-abs(phi_recon'*phi_ini(:)*dx_e^2*dx_n);
    
    %Subtract off current wavefunction within overlaps
    phi_t = phi_t - phi_recon;
    phi_t = phi_t./sqrt(sum(abs(phi_t(:)).^2)*dx_e^2*dx_n);
    
    overlaps = zeros(N_traj,1);
    for alpha = 1:N_traj
        overlaps(alpha) = phi_n(:,alpha)'*(reshape((reshape(phi_t,Dim_nuc*Dim_ele,Dim_ele)*conj(phi_e1(:,alpha))),Dim_nuc,Dim_ele)*conj(phi_e2(:,alpha)))*dx_n*dx_e*dx_e;
    end
    overlaps = abs(overlaps);
    
    overlaps(active) = 0;
    
    red_e = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),1),2))*dx_e*dx_n;
    red_n = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),3),2))*dx_e^2;

    clf
    subplot(2,1,1)
    plot(red_e); hold on;
    plot(squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),1),2)*dx_e*dx_n))
    hold off
    subplot(2,1,2)
    plot(red_n); hold on;
    plot(squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3),2)*dx_e^2))
    hold off
    pause(0.0001)

end

[U,S,V] = svd(S_full(active,active));
max(diag(S))/min(diag(S));

list = 1:N_traj;
remaining = setdiff(list,active);
N_remaining = size(remaining,2);
%Add remaining bases so long as they dont affect the condition number
for i=1:N_remaining
    [U,S,V] = svd(S_full([active remaining(i)],[active remaining(i)]));
    if(max(diag(S))/min(diag(S)) <10^8)
        active = [active remaining(i)];
        C = [C.' 0].';
    end
end
N_active = size(active,2);

phi_recon = zeros(Dim_nuc*Dim_ele^2,1);
for i=1:N_active
    phi_recon = phi_recon + C(i)*kron(phi_e1(:,active(i)),kron(phi_e2(:,active(i)),phi_n(:,active(i))));
end
red_e = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),1),2))*dx_e*dx_n;
red_n = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),3),2))*dx_e^2;

clf
subplot(2,1,1)
plot(red_e); hold on;
plot(squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),1),2)*dx_e*dx_n))
hold off
subplot(2,1,2)
plot(red_n); hold on;
plot(squeeze(sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3),2)*dx_e^2))
hold off

xe1_ini = xe1;
xe2_ini = xe2;
xn_ini = xn;
xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
mesh_e1_ini = mesh_e1;
mesh_e2_ini = mesh_e2;
mesh_n_ini = mesh_n;
