%% Getting initial wavefunctions from eigenstates of the conditional hermitian wavefunction

xe1 = xe1_ini;
xe2 = xe2_ini;
xn = xn_ini;
xe1_old = xe1_ini;
xe2_old = xe2_ini;
xn_old = xn_ini;
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;
ve1_old = ve1;
ve2_old = ve2;
vn_old = vn;
mesh_e1 = mesh_e1_ini;
mesh_e2 = mesh_e2_ini;
mesh_n = mesh_n_ini;

initPerms = []
NPerms = 20;
for p=1:NPerms
    initPerms = [initPerms randperm(N_traj)]

phi_e1 = zeros(Dim_ele,N_traj);
phi_e2 = zeros(Dim_ele,N_traj);
phi_n = zeros(Dim_nuc,N_traj);

phi_rs = reshape(phi_ini,Dim_nuc,Dim_ele,Dim_ele);
for i=1:N_traj
    phi_e1(:,i) = squeeze(phi_rs(mesh_n(i),:,mesh_e2(i))).';
    phi_e1(:,i) = phi_e1(:,i)./sqrt(sum(abs(phi_e1(:,i)).^2)*dx_e);
    
    phi_e2(:,i) = squeeze(phi_rs(mesh_n(i),mesh_e1(i),:));
    phi_e2(:,i) = phi_e2(:,i)./sqrt(sum(abs(phi_e2(:,i)).^2)*dx_e);
    
    phi_n(:,i) = squeeze(phi_rs(:,mesh_e1(i),mesh_e2(i)));
    phi_n(:,i) = phi_n(:,i)./sqrt(sum(abs(phi_n(:,i)).^2*dx_n));
end

M = (phi_n'*phi_n*dx_n).*(phi_e1'*phi_e1*dx_e).*(phi_e2'*phi_e2*dx_e);
G = zeros(N_traj,1);
for alpha = 1:N_traj
    G(alpha) = phi_n(:,alpha)'*(reshape((reshape(phi_ini,Dim_nuc*Dim_ele,Dim_ele)*conj(phi_e2(:,alpha))),Dim_nuc,Dim_ele)*conj(phi_e1(:,alpha)))*dx_n*dx_e*dx_e;
end
C = pinv(M,10^tol)*G;
C = C./sqrt(C'*M*C);

phi_recon = zeros(Dim_nuc*Dim_ele^2,1);
for i = 1:N_traj
    phi_recon = phi_recon + C(i)*kron(phi_e1(:,i),kron(phi_e2(:,i),phi_n(:,i)));
end



red_e = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),1),3)*dx_n*dx_e);
red_n = squeeze(sum(sum(reshape(abs(phi_recon).^2,Dim_nuc,Dim_ele,Dim_ele),3),2)*dx_e^2);

red_e_exact = sum(sum(abs(phi_rs).^2,1),3)*dx_e*dx_n;
red_n_exact = sum(sum(abs(phi_rs).^2,3),2)*dx_e^2;


xe1_ini = xe1;
xe2_ini = xe2;
xn_ini = xn;
xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
mesh_e1_ini = mesh_e1;
mesh_e2_ini = mesh_e2;
mesh_n_ini = mesh_n;
phi_ini = phi_ini(:);
four_point_stencil = imag(e_gradent_s_1*phi_ini./phi_ini);
ve1_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(e_gradent_s_2*phi_ini./phi_ini);
ve2_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(n_gradent_s_1*phi_ini./phi_ini);
vn_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_n);
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;
