%% RUNGE-KUTTA PROPAGATION OF THE IPW
AEfield = (Efield(t+1) -Efield(t));
%N_traj_store = N_traj;
%N_traj = N_traj+N_slices;
%K1
grad_aux_e1 = e_gradient*phi_e1; 
grad_aux_n = n_gradient*phi_n;

aux_tot = (phi_n(mesh_n,active).*phi_e1(mesh_e1,active).*phi_e1(mesh_e2,active))*C;
aux_der = (phi_n(mesh_n,active).*grad_aux_e1(mesh_e1,active).*phi_e1(mesh_e2,active))*C;
v1_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n(mesh_n,active).*phi_e1(mesh_e1,active).*grad_aux_e1(mesh_e2,active))*C;
v1_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n,active).*phi_e1(mesh_e1,active).*phi_e1(mesh_e2,active))*C;
v1_n = imag(aux_der./aux_tot)/mu_n;

VH = (Vee*(abs(phi_e1).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*Efield(t) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)  + Wn;

dphi_e1_1 = -(1i)*(pot_prop_e.*phi_e1 + e_T*phi_e1);
dphi_n_1 = -(1i)*(pot_prop_n.*phi_n + n_T*phi_n);

%% PROPAGATE Cs
Mnn = phi_n(:,active)'*phi_n(:,active)*dx_n;
Mee1 = phi_e1(:,active)'*phi_e1(:,active)*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1(:,active)'*(VH(:,active).*phi_e1(:,active)))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1(:,active)'*((-soft_coulomb(n_pos_grid1(:,active) - e_axis1(:,active),soft_en) - soft_coulomb(n_pos_grid2(:,active) - e_axis2(:,active),soft_en)).*phi_e1(:,active)))*dx_e) ...
       - (Mee1.^2).*((phi_n(:,active)'*((-soft_coulomb(e1_pos_grid1(:,active)-n1_axis1(:,active),soft_en) -soft_coulomb(e1_pos_grid2(:,active)-n1_axis2(:,active),soft_en)).*phi_n(:,active)))*dx_n ...
                     +(phi_n(:,active)'*((-soft_coulomb(e2_pos_grid1(:,active)-n2_axis1(:,active),soft_en) - soft_coulomb(e2_pos_grid2(:,active)-n2_axis2(:,active),soft_en)).*phi_n(:,active)))*dx_n);
for k = 1:N_active
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1(:,active(k))),1,N_active).*phi_e1(:,active)).*(Vee*(repmat(conj(phi_e1(:,active(k))),1,N_active).*phi_e1(:,active))),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n(:,active(k))),1,N_active).*phi_n(:,active)).*(Ven*(repmat(conj(phi_e1(:,active(k))),1,N_active).*phi_e1(:,active))),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_1 = pinv(Mnn.*(Mee1.^2),10^(tol))*(-1i*(Mee + Men)*C);

%% K2
xe1_aux = xe1 + v1_e1*dt/2;
xe2_aux = xe2 + v1_e2*dt/2;
xn_aux = xn + v1_n*dt/2;

indices = (xe1_aux < xe_axis(1));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(1));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(1));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;

phi_e1_aux = phi_e1 + dphi_e1_1*dt/2;
phi_n_aux = phi_n + dphi_n_1*dt/2;
C_aux = C + C_dot_1*dt/2;

grad_aux_e1 = e_gradient*phi_e1_aux;   
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,active).*grad_aux_e1(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v2_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*grad_aux_e1(mesh_e2_aux,active))*C_aux;
v2_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v2_n = imag(aux_der./aux_tot)/mu_n;

VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*(Efield(t) + AEfield/2) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en) + Wn;

dphi_e1_2 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_2 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux(:,active)'*phi_n_aux(:,active)*dx_n;
Mee1 = phi_e1_aux(:,active)'*phi_e1_aux(:,active)*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux(:,active)'*(VH(:,active).*phi_e1_aux(:,active)))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux(:,active)'*((-soft_coulomb(n_pos_grid1(:,active) - e_axis1(:,active),soft_en) - soft_coulomb(n_pos_grid2(:,active) - e_axis2(:,active),soft_en)).*phi_e1_aux(:,active)))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux(:,active)'*((-soft_coulomb(e1_pos_grid1(:,active)-n1_axis1(:,active),soft_en) -soft_coulomb(e1_pos_grid2(:,active)-n1_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n ...
        +(phi_n_aux(:,active)'*((-soft_coulomb(e2_pos_grid1(:,active)-n2_axis1(:,active),soft_en) - soft_coulomb(e2_pos_grid2(:,active)-n2_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n);
for k = 1:N_active
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active)).*(Vee*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,active(k))),1,N_active).*phi_n_aux(:,active)).*(Ven*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_2 = pinv(Mnn.*(Mee1.^2),10^(tol))*(-1i*(Mee + Men)*C_aux);

%% k3
xe1_aux = xe1 + v2_e1*dt/2;
xe2_aux = xe2 + v2_e2*dt/2;
xn_aux = xn + v2_n*dt/2;

indices = (xe1_aux < xe_axis(1));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(1));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(1));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;

phi_e1_aux = phi_e1 + dphi_e1_2*dt/2;
phi_n_aux = phi_n + dphi_n_2*dt/2;

C_aux = C + C_dot_2*dt/2;

grad_aux_e1 = e_gradient*phi_e1_aux;   
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,active).*grad_aux_e1(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v3_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*grad_aux_e1(mesh_e2_aux,active))*C_aux;
v3_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v3_n = imag(aux_der./aux_tot)/mu_n;

VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*(Efield(t) + AEfield/2) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en) + Wn;

dphi_e1_3 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_3 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux(:,active)'*phi_n_aux(:,active)*dx_n;
Mee1 = phi_e1_aux(:,active)'*phi_e1_aux(:,active)*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux(:,active)'*(VH(:,active).*phi_e1_aux(:,active)))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux(:,active)'*((-soft_coulomb(n_pos_grid1(:,active) - e_axis1(:,active),soft_en) - soft_coulomb(n_pos_grid2(:,active) - e_axis2(:,active),soft_en)).*phi_e1_aux(:,active)))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux(:,active)'*((-soft_coulomb(e1_pos_grid1(:,active)-n1_axis1(:,active),soft_en) -soft_coulomb(e1_pos_grid2(:,active)-n1_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n ...
        +(phi_n_aux(:,active)'*((-soft_coulomb(e2_pos_grid1(:,active)-n2_axis1(:,active),soft_en) - soft_coulomb(e2_pos_grid2(:,active)-n2_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n);
for k = 1:N_active
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active)).*(Vee*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,active(k))),1,N_active).*phi_n_aux(:,active)).*(Ven*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_3 = pinv(Mnn.*(Mee1.^2),10^(tol))*(-1i*(Mee + Men)*C_aux);

%% k4
xe1_aux = xe1 + v3_e1*dt;
xe2_aux = xe2 + v3_e2*dt;
xn_aux = xn + v3_n*dt;

indices = (xe1_aux < xe_axis(1));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(1));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(1));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;
phi_e1_aux = phi_e1 + dphi_e1_3*dt;
phi_n_aux = phi_n + dphi_n_3*dt;

C_aux = C + C_dot_3*dt;

grad_aux_e1 = e_gradient*phi_e1_aux;     
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,active).*grad_aux_e1(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v4_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*grad_aux_e1(mesh_e2_aux,active))*C_aux;
v4_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,active).*phi_e1_aux(mesh_e1_aux,active).*phi_e1_aux(mesh_e2_aux,active))*C_aux;
v4_n = imag(aux_der./aux_tot)/mu_n;

VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*(Efield(t) + AEfield) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en) + Wn;

dphi_e1_4 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_4 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux(:,active)'*phi_n_aux(:,active)*dx_n;
Mee1 = phi_e1_aux(:,active)'*phi_e1_aux(:,active)*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux(:,active)'*(VH(:,active).*phi_e1_aux(:,active)))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux(:,active)'*((-soft_coulomb(n_pos_grid1(:,active) - e_axis1(:,active),soft_en) - soft_coulomb(n_pos_grid2(:,active) - e_axis2(:,active),soft_en)).*phi_e1_aux(:,active)))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux(:,active)'*((-soft_coulomb(e1_pos_grid1(:,active)-n1_axis1(:,active),soft_en) -soft_coulomb(e1_pos_grid2(:,active)-n1_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n ...
        +(phi_n_aux(:,active)'*((-soft_coulomb(e2_pos_grid1(:,active)-n2_axis1(:,active),soft_en) - soft_coulomb(e2_pos_grid2(:,active)-n2_axis2(:,active),soft_en)).*phi_n_aux(:,active)))*dx_n);
for k = 1:N_active
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active)).*(Vee*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,active(k))),1,N_active).*phi_n_aux(:,active)).*(Ven*(repmat(conj(phi_e1_aux(:,active(k))),1,N_active).*phi_e1_aux(:,active))),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_4 = pinv(Mnn.*(Mee1.^2),10^(tol))*(-1i*(Mee + Men)*C_aux);


%% Check for Psi dot drift out of active basis

% S = ((phi_e1'*phi_e1*dx_e).^2).*(phi_n'*phi_n*dx_n);
% S_inv = pinv(S([active pool],[active pool]),10^tol);
% 
% S_active = S(active,active);
% S_active_inv = pinv(S_active,10^(tol));
% 
% phi_e1_adv = phi_e1 + (dt/6)*(dphi_e1_1 + 2*dphi_e1_2 + 2*dphi_e1_3 + dphi_e1_4);
% phi_n_adv  = phi_n + (dt/6)*(dphi_n_1 + 2*dphi_n_2 + 2*dphi_n_3 + dphi_n_4);
% 
% S_curr_adv = ((phi_e1'*phi_e1_adv*dx_e).^2).*(phi_n'*phi_n_adv*dx_n);
% 
% C_adv = C + (dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);
% 
% % |r> = (1_full - 1_active) |Psi(t+dt)> = (1_full - 1_active) (C)
% 
% %Cfull = S_inv*((S(:,active)*C_adv + S_curr_adv(:,active)*C));
% %Cprime = S_active_inv*(S_active*C_adv + S_curr_adv(active,active)*C);
% Cfull = S_inv*(S_curr_adv([active pool],active)*C_adv);
% Cprime = S_active_inv*(S_curr_adv(active,active)*C_adv);

phi_e1_adv = phi_e1 + (dt/6)*(dphi_e1_1 + 2*dphi_e1_2 + 2*dphi_e1_3 + dphi_e1_4);
phi_n_adv  = phi_n + (dt/6)*(dphi_n_1 + 2*dphi_n_2 + 2*dphi_n_3 + dphi_n_4);
C_adv = C + (dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);

Sadv = ((phi_e1_adv'*phi_e1_adv*dx_e).^2).*(phi_n_adv'*phi_n_adv*dx_n);
Sadv_inv = pinv(Sadv([active pool],[active pool]),10^tol);

Sadv_active = Sadv(active,active);
Sadv_active_inv = pinv(Sadv_active,10^(tol));

Cfull = Sadv_inv*(Sadv([active pool],active)*C_adv);
Cprime = Sadv_active_inv*(Sadv_active*C_adv);
% || |r> ||
%a = [a sqrt(abs(Cfull'*S([active pool],[active pool])*Cfull - 2*real(Cprime'*S(active,[active pool])*Cfull) + Cprime'*S_active*Cprime))];
errorL = [errorL sqrt(abs(Cfull'*Sadv([active pool],[active pool])*Cfull - 2*real(Cprime'*Sadv(active,[active pool])*Cfull) + Cprime'*Sadv_active*Cprime))];


%% EVOLVED CONDITIONAL WAVEFUNCTION AND TRAJECTORIES
phi_e1 = phi_e1 + (dt/6)*(dphi_e1_1 + 2*dphi_e1_2 + 2*dphi_e1_3 + dphi_e1_4);
phi_n = phi_n + (dt/6)*(dphi_n_1 + 2*dphi_n_2 + 2*dphi_n_3 + dphi_n_4);

xe1 = xe1 + (dt/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1);
xe2 = xe2 + (dt/6)*(v1_e2 + 2*v2_e2 + 2*v3_e2 + v4_e2);
xn = xn + (dt/6)*(v1_n + 2*v2_n + 2*v3_n + v4_n);

%figure(2)
%clf
%plot(abs(((dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4))./abs(C)),'.');
%pause(0.00001)
%max(abs(((dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4))./C))
if(mean(abs(((dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4))./abs(C))) > 0.05)
    adapt=true;
end

C = C + (dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);
%N_traj = N_traj_store;
if(errorL(end)>1E-3)
    %slice_resample
    %Adaptive_degenerate_matching_current
end
%M = ((phi_e1(:,active)'*phi_e1(:,active)*dx_e).^2).*(phi_n(:,active)'*phi_n(:,active)*dx_n);
%sqrt(C'*M*C)

%C = C./sqrt(C'*M*C);

%max((dt/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1))

%Update old particle positions

%%%%Dealing with those that have drifted out of the box
indices = (xe1 < xe_axis(2));
xe1(indices) = xe1_old(indices);
indices = (xe1 > xe_axis(end));
xe1(indices) = xe1_old(indices);

indices = (xe2 < xe_axis(2));
xe2(indices) =xe2_old(indices);
indices = (xe2 > xe_axis(end));
xe2(indices) =xe2_old(indices);

indices = (xn < xn_axis(2));
xn(indices) = xn_old(indices);
indices = (xn > xn_axis(end));
xn(indices) = xn_old(indices);

xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;

