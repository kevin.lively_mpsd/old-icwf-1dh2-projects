%% RUNGE-KUTTA PROPAGATION OF THE IPW
AEfield = (Efield(t+1)-Efield(t));

%K1
grad_aux_e1 = e_gradient*phi_e1; 
grad_aux_n = n_gradient*phi_n;

aux_tot = (phi_n(mesh_n,:).*phi_e1(mesh_e1,:).*phi_e1(mesh_e2,:))*C;
aux_der = (phi_n(mesh_n,:).*grad_aux_e1(mesh_e1,:).*phi_e1(mesh_e2,:))*C;
v1_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n(mesh_n,:).*phi_e1(mesh_e1,:).*grad_aux_e1(mesh_e2,:))*C;
v1_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n,:).*phi_e1(mesh_e1,:).*phi_e1(mesh_e2,:))*C;
v1_n = imag(aux_der./aux_tot)/mu_n;
if(HCWF_field)
aux_tot = (phi_n(mesh_n,:).*phi_e1(mesh_e1,:).*phi_e1(mesh_e2,:));
aux_der = (phi_n(mesh_n,:).*grad_aux_e1(mesh_e1,:).*phi_e1(mesh_e2,:));
v1_e1 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (phi_n(mesh_n,:).*phi_e1(mesh_e1,:).*grad_aux_e1(mesh_e2,:));
v1_e2 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (grad_aux_n(mesh_n,:).*phi_e1(mesh_e1,:).*phi_e1(mesh_e2,:));
v1_n = diag(imag(aux_der./aux_tot)/mu_n);
end
VH = (Vee*(abs(phi_e1).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*Efield(t) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)  + Wn;

dphi_e1_1 = -(1i)*(pot_prop_e.*phi_e1 + e_T*phi_e1);
dphi_n_1 = -(1i)*(pot_prop_n.*phi_n + n_T*phi_n);

%% PROPAGATE Cs
Mnn = phi_n'*phi_n*dx_n;
Mee1 = phi_e1'*phi_e1*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1'*(VH.*phi_e1))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1'*((-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en)).*phi_e1))*dx_e) ...
       - (Mee1.^2).*((phi_n'*((-soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2-n1_axis2,soft_en)).*phi_n))*dx_n ...
                     +(phi_n'*((-soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)).*phi_n))*dx_n);
for k = 1:N_traj
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1(:,k)),1,N_traj).*phi_e1).*(Vee*(repmat(conj(phi_e1(:,k)),1,N_traj).*phi_e1)),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n(:,k)),1,N_traj).*phi_n).*(Ven*(repmat(conj(phi_e1(:,k)),1,N_traj).*phi_e1)),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_1 = pinv(Mnn.*(Mee1.^2),1E-4)*(-1i*(Mee + Men)*C);

%K2
xe1_aux = xe1 + v1_e1*dt/2;
xe2_aux = xe2 + v1_e2*dt/2;
xn_aux = xn + v1_n*dt/2;

indices = (xe1_aux < xe_axis(2));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(2));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(2));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;
phi_e1_aux = phi_e1 + dphi_e1_1*dt/2;
phi_n_aux = phi_n + dphi_n_1*dt/2;
C_aux = C + C_dot_1*dt/2;

grad_aux_e1 = e_gradient*phi_e1_aux;   
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v2_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:))*C_aux;
v2_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v2_n = imag(aux_der./aux_tot)/mu_n;
if(HCWF_field)
aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v2_e1 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:));
v2_e2 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v2_n = diag(imag(aux_der./aux_tot)/mu_n);
end
VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*Efield(t) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en) + Wn;

dphi_e1_2 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_2 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux'*phi_n_aux*dx_n;
Mee1 = phi_e1_aux'*phi_e1_aux*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux'*(VH.*phi_e1_aux))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux'*((-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en)).*phi_e1_aux))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux'*((-soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2-n1_axis2,soft_en)).*phi_n_aux))*dx_n ...
        +(phi_n_aux'*((-soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)).*phi_n_aux))*dx_n);
for k = 1:N_traj
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux).*(Vee*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,k)),1,N_traj).*phi_n_aux).*(Ven*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_2 = pinv(Mnn.*(Mee1.^2),1E-4)*(-1i*(Mee + Men)*C_aux);

%% k3
xe1_aux = xe1 + v2_e1*dt/2;
xe2_aux = xe2 + v2_e2*dt/2;
xn_aux = xn + v2_n*dt/2;

indices = (xe1_aux < xe_axis(2));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(2));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(2));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;

phi_e1_aux = phi_e1 + dphi_e1_2*dt/2;
phi_n_aux = phi_n + dphi_n_2*dt/2;

C_aux = C + C_dot_2*dt/2;

grad_aux_e1 = e_gradient*phi_e1_aux;   
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v3_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:))*C_aux;
v3_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v3_n = imag(aux_der./aux_tot)/mu_n;
if(HCWF_field)
aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v3_e1 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:));
v3_e2 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v3_n = diag(imag(aux_der./aux_tot)/mu_n);
end
VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*Efield(t) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)  + Wn;

dphi_e1_3 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_3 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux'*phi_n_aux*dx_n;
Mee1 = phi_e1_aux'*phi_e1_aux*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux'*(VH.*phi_e1_aux))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux'*((-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en)).*phi_e1_aux))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux'*((-soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2-n1_axis2,soft_en)).*phi_n_aux))*dx_n ...
                     +(phi_n_aux'*((-soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)).*phi_n_aux))*dx_n);
for k = 1:N_traj
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux).*(Vee*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_e*dx_e;
    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,k)),1,N_traj).*phi_n_aux).*(Ven*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_3 = pinv(Mnn.*(Mee1.^2),1E-4)*(-1i*(Mee + Men)*C_aux);

%% k4
xe1_aux = xe1 + v3_e1*dt;
xe2_aux = xe2 + v3_e2*dt;
xn_aux = xn + v3_n*dt;

indices = (xe1_aux < xe_axis(2));
xe1_aux(indices) = xe1_old(indices);
indices = (xe1_aux > xe_axis(end));
xe1_aux(indices) = xe1_old(indices);

indices = (xe2_aux < xe_axis(2));
xe2_aux(indices) =xe2_old(indices);
indices = (xe2_aux > xe_axis(end));
xe2_aux(indices) =xe2_old(indices);

indices = (xn_aux < xn_axis(2));
xn_aux(indices) = xn_old(indices);
indices = (xn_aux > xn_axis(end));
xn_aux(indices) = xn_old(indices);

mesh_e1_aux = floor(xe1_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_e2_aux = floor(xe2_aux/dx_e - xe_axis(1)/dx_e) + 1;
mesh_n_aux = floor(xn_aux/dx_n - xn_axis(1)/dx_n) + 1;
phi_e1_aux = phi_e1 + dphi_e1_3*dt;
phi_n_aux = phi_n + dphi_n_3*dt;

C_aux = C + C_dot_3*dt;

grad_aux_e1 = e_gradient*phi_e1_aux;     
grad_aux_n = n_gradient*phi_n_aux; 

aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v4_e1 = imag(aux_der./aux_tot)/mu_e;

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:))*C_aux;
v4_e2 = imag(aux_der./aux_tot)/mu_e;

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:))*C_aux;
v4_n = imag(aux_der./aux_tot)/mu_n;

if(HCWF_field)
aux_tot = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
aux_der = (phi_n_aux(mesh_n_aux,:).*grad_aux_e1(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v4_e1 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (phi_n_aux(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*grad_aux_e1(mesh_e2_aux,:));
v4_e2 = diag(imag(aux_der./aux_tot)/mu_e);

aux_der = (grad_aux_n(mesh_n_aux,:).*phi_e1_aux(mesh_e1_aux,:).*phi_e1_aux(mesh_e2_aux,:));
v4_n = diag(imag(aux_der./aux_tot)/mu_n);
end
VH = (Vee*(abs(phi_e1_aux).^2))*dx_e;
[n_pos_grid1, e_axis1] = meshgrid(((m1)/(m1+m2))*xn_aux,xe_axis);
[n_pos_grid2, e_axis2] = meshgrid((-(m2)/(m1+m2))*xn_aux,xe_axis);
[e1_pos_grid1, n1_axis1] = meshgrid(xe1_aux,((m1)/(m1+m2))*xn_axis);
[e1_pos_grid2, n1_axis2] = meshgrid(xe1_aux,(-(m2)/(m1+m2))*xn_axis);
[e2_pos_grid1, n2_axis1] = meshgrid(xe2_aux,((m1)/(m1+m2))*xn_axis);
[e2_pos_grid2, n2_axis2] = meshgrid(xe2_aux,(-(m2)/(m1+m2))*xn_axis);

pot_prop_e = VH - soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en) - repmat(xe_axis,1,N_traj)*Efield(t) + W;
pot_prop_n = repmat(Vnn,1,N_traj) - soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) - soft_coulomb(e1_pos_grid2-n1_axis2,soft_en) - ...
                soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en) + Wn;

dphi_e1_4 = -(1i)*(pot_prop_e.*phi_e1_aux + e_T*phi_e1_aux);
dphi_n_4 = -(1i)*(pot_prop_n.*phi_n_aux + n_T*phi_n_aux);

%% PROPAGATE Cs
Mnn = phi_n_aux'*phi_n_aux*dx_n;
Mee1 = phi_e1_aux'*phi_e1_aux*dx_e;
Mee = - 2*Mnn.*Mee1.*(phi_e1_aux'*(VH.*phi_e1_aux))*dx_e;
Men = - 2*Mnn.*(Mee1.*(phi_e1_aux'*((-soft_coulomb(n_pos_grid1 - e_axis1,soft_en) - soft_coulomb(n_pos_grid2 - e_axis2,soft_en)).*phi_e1_aux))*dx_e) ...
       - (Mee1.^2).*((phi_n_aux'*((-soft_coulomb(e1_pos_grid1-n1_axis1,soft_en) -soft_coulomb(e1_pos_grid2-n1_axis2,soft_en)).*phi_n_aux))*dx_n ...
                     +(phi_n_aux'*((-soft_coulomb(e2_pos_grid1-n2_axis1,soft_en) - soft_coulomb(e2_pos_grid2-n2_axis2,soft_en)).*phi_n_aux))*dx_n);
for k = 1:N_traj
    Mee(k,:) = Mee(k,:) + Mnn(k,:).*sum((repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux).*(Vee*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_e*dx_e;

    Men(k,:) = Men(k,:) + 2*Mee1(k,:).*sum((repmat(conj(phi_n_aux(:,k)),1,N_traj).*phi_n_aux).*(Ven*(repmat(conj(phi_e1_aux(:,k)),1,N_traj).*phi_e1_aux)),1)*dx_n*dx_e;
end
%MVn = Mee1.*Mee2.*(((Vnn.*phi_n)')*phi_n)*dx_n;
C_dot_4 = pinv(Mnn.*(Mee1.^2),1E-4)*(-1i*(Mee + Men)*C_aux);

%% EVOLVED CONDITIONAL WAVEFUNCTION AND TRAJECTORIES
phi_e1 = phi_e1 + (dt/6)*(dphi_e1_1 + 2*dphi_e1_2 + 2*dphi_e1_3 + dphi_e1_4);
phi_n = phi_n + (dt/6)*(dphi_n_1 + 2*dphi_n_2 + 2*dphi_n_3 + dphi_n_4);

xe1 = xe1 + (dt/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1);
xe2 = xe2 + (dt/6)*(v1_e2 + 2*v2_e2 + 2*v3_e2 + v4_e2);
xn = xn + (dt/6)*(v1_n + 2*v2_n + 2*v3_n + v4_n);

C = C + (dt/6)*(C_dot_1 + 2*C_dot_2 + 2*C_dot_3 + C_dot_4);

%M = ((phi_e1'*phi_e1*dx_e).^2).*(phi_n'*phi_n*dx_n);
%sqrt(C'*M*C)
%C = C./sqrt(C'*M*C);

%max((dt/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1))

%Update old particle positions

%%%%Dealing with those that have drifted out of the box
indices = (xe1 < xe_axis(2));
xe1(indices) = xe1_old(indices);
indices = (xe1 > xe_axis(end));
xe1(indices) = xe1_old(indices);

indices = (xe2 < xe_axis(2));
xe2(indices) =xe2_old(indices);
indices = (xe2 > xe_axis(end));
xe2(indices) =xe2_old(indices);

indices = (xn < xn_axis(2));
xn(indices) = xn_old(indices);
indices = (xn > xn_axis(end));
xn(indices) = xn_old(indices);

xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
