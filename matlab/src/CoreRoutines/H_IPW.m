
fprintf('\n \n'); fprintf('Solving the TDSE with Conditional wavefunctions... \n');
time_index = 0;

time_start = clock;
time_step = time_start;
if(mapped)
    rho = rho_recon;
end

ind=0;
for t = 1:t_end
    if (mod(t,time_int) == 0 || t == 1) 
        fprintf('Estimated Total Time: %18.6f \n',etime(clock(), time_step)*num_saved_points)
        fprintf('Elapsed Time: %18.6f \n',etime(clock(), time_start))
        fprintf('Total number of trajectories: %18.6f \n',N_traj)
        fprintf('\n')   
        
        time_step = clock;
        exist 'ConfAverage';
        avg = ans;
        if (avg == true && ConfAverage==true)
            saving_parallel_test
        else
            if(adaptive==true)
                saving_ICWF_adaptive
            else
                saving_data_IPW
            end
        end
        clf
        ind=ind+1;
        subplot(2,1,1)
        plot(abs(red_n))
        %plot(real(pot_prop_n(15:end,:)))
%         hold on
        %plot(mean(abs(phi_n(15:end,:)).^2))
%         hold off
        title(num2str(ind))
        subplot(2,1,2)
        %plot(abs(phi_e1).^2)
        plot(abs(red_e))
        pause(0.00001)
       
    end

    if(mapped)        
        runge_kutta_prop_ICWF
    end
    if(unmapped)
        %runge_kutta_prop_unmapped_ICWF_degenerate_electron
        %runge_mean_test
        
        if(adaptive==true)
            runge_adaptive_degenerate_electrons
        else
            runge_kutta_prop_unmapped_ICWF
        end
    end
        
%     if(mod(t,50)==0)
%     clf
%         %ind=ind+1;
%         subplot(2,1,1)
%         %plot(abs(red_n))
%         plot(real(pot_prop_n(15:end,:)))
%          hold on
%         plot(abs(phi_n(15:end,:)).^2)
%          hold off
%         title(num2str(ind))
%         subplot(2,1,2)
%         plot(mean(abs(phi_e1).^2,2))
%         %plot(abs(red_e))
%         pause(0.00001)
%     end
%     

end
fprintf('Total Cond Time: %18.6f \n',etime(clock(), time_start));



