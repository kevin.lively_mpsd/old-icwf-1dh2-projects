

PD = reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele);
PDaux = PD;



%% MONTE CARLO SAMPLING:
[mesh_n_ini,mesh_e1_ini,mesh_e2_ini] = mcsampling(PDaux,N_traj,threshold);
N_traj = length(mesh_n_ini);
rand_vals = rand(N_traj,1);
xe1_ini = (mesh_e1_ini-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
rand_vals = rand(N_traj,1);
xe2_ini = (mesh_e2_ini-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
rand_vals = rand(N_traj,1);
xn_ini = (mesh_n_ini-1)*dx_n + xn_axis(1) + (rand_vals - 0.5)*dx_n;

xe1 = xe1_ini;
xe2 = xe2_ini;
xn = xn_ini;
xe1_old = xe1;
xe2_old = xe2;
xn_old = xn;
mesh_e1 = mesh_e1_ini;
mesh_e2 = mesh_e2_ini;
mesh_n = mesh_n_ini;
four_point_stencil = imag(e_gradent_s_1*phi_ini./phi_ini);
ve1_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(e_gradent_s_2*phi_ini./phi_ini);
ve2_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_e);
four_point_stencil = imag(n_gradent_s_1*phi_ini./phi_ini);
vn_ini = real(four_point_stencil((mesh_e1_ini - 1)*Dim_nuc*Dim_ele + (mesh_e2_ini - 1)*Dim_nuc + mesh_n)/mu_n);
ve1 = ve1_ini;
ve2 = ve2_ini;
vn = vn_ini;

