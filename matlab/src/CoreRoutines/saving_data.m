% SAVING DATA %

time_index = time_index + 1;

%% PROBABILITY DENSITY
str = strcat('exact_wavepacket',int2str(time_index),'.txt'); full_wavepacket = fopen(str,'w');
fwrite(full_wavepacket,abs(phi(:)).^2,'double');
fclose(full_wavepacket);


%% ADIABATIC COMPONENTS
pop = abs(sum(sum(conj(BOEIGSTATE_GR).*reshape(phi,Dim_nuc,Dim_ele,Dim_ele),2),3)).^2;
str = strcat('gr_comp_exact',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
fwrite(CWF_e,pop,'double');
fclose(CWF_e);

pop = abs(sum(sum(conj(BOEIGSTATE_EX1).*reshape(phi,Dim_nuc,Dim_ele,Dim_ele),2),3)).^2;
str = strcat('ex1_comp_exact',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
fwrite(CWF_e,pop,'double');
fclose(CWF_e);

pop = abs(sum(sum(conj(BOEIGSTATE_EX2).*reshape(phi,Dim_nuc,Dim_ele,Dim_ele),2),3)).^2;
str = strcat('ex2_comp_exact',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
fwrite(CWF_e,pop,'double');
fclose(CWF_e);

pop = abs(sum(sum(conj(BOEIGSTATE_EX3).*reshape(phi,Dim_nuc,Dim_ele,Dim_ele),2),3)).^2;
str = strcat('ex3_comp_exact',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
fwrite(CWF_e,pop,'double');
fclose(CWF_e);

pop = abs(sum(sum(conj(BOEIGSTATE_EX4).*reshape(phi,Dim_nuc,Dim_ele,Dim_ele),2),3)).^2;
str = strcat('ex4_comp_exact',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
fwrite(CWF_e,pop,'double');
fclose(CWF_e);





