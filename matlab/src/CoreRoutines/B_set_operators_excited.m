% Defining the Operators:
fprintf('\n \n'); fprintf('Defining the Operators... \n');

%% OPERATORS IN THE ELECTRONIC SPACE:
e_unit_nx = speye(Dim_ele);
e_position = zeros(Dim_ele);
e_gradient = zeros(Dim_ele);
e_laplacian = zeros(Dim_ele);

xh = (Dim_ele-1)/2;
for jj=1:Dim_ele
    e_position(jj,jj) = (jj-1-xh)*dx_e;
end
e_position = sparse(e_position);

for jj=1:Dim_ele
    e_gradient(jj,jj) = 0;
    if (jj < Dim_ele),
        e_gradient(jj,jj+1) = 1/(dx_e)*( +5.0/6.0);
    end
    if (jj > 1),
        e_gradient(jj,jj-1) = 1/(dx_e)*( -5.0/6.0);
    end
    if (jj < Dim_ele - 1),
        e_gradient(jj,jj+2) = 1/(dx_e)*( -5.0/21.0);
    end
    if (jj > 2),
        e_gradient(jj,jj-2) = 1/(dx_e)*( +5.0/21.0);
    end
    if (jj < Dim_ele - 2),
        e_gradient(jj,jj+3) = 1/(dx_e)*( +5.0/84.0);
    end
    if (jj > 3),
        e_gradient(jj,jj-3) = 1/(dx_e)*( -5.0/84.0);
    end
    if (jj < Dim_ele - 3),
        e_gradient(jj,jj+4) = 1/(dx_e)*( -5.0/504.0);
    end
    if (jj > 4),
        e_gradient(jj,jj-4) = 1/(dx_e)*( +5.0/504.0);
    end    
    if (jj < Dim_ele - 4),
        e_gradient(jj,jj+5) = 1/(dx_e)*( +1.0/1260.0);
    end
    if (jj > 5),
        e_gradient(jj,jj-5) = 1/(dx_e)*( -1.0/1260.0);
    end      
end
e_gradient = sparse(e_gradient);

for jj=1:Dim_ele
    e_laplacian(jj,jj) = 1/(dx_e^2)*( -73766.0/25200.0);
    if (jj < Dim_ele),
        e_laplacian(jj,jj+1) = 1/(dx_e^2)*( 5.0/3.0);
    end
    if (jj > 1),
        e_laplacian(jj,jj-1) = 1/(dx_e^2)*( 5.0/3.0);
    end
    if (jj < Dim_ele - 1),
        e_laplacian(jj,jj+2) = 1/(dx_e^2)*( -5.0/21.0);
    end
    if (jj > 2),
        e_laplacian(jj,jj-2) = 1/(dx_e^2)*( -5.0/21.0);
    end
    if (jj < Dim_ele - 2),
        e_laplacian(jj,jj+3) = 1/(dx_e^2)*( 5.0/126.0);
    end
    if (jj > 3),
        e_laplacian(jj,jj-3) = 1/(dx_e^2)*( 5.0/126.0);
    end
    if (jj < Dim_ele - 3),
        e_laplacian(jj,jj+4) = 1/(dx_e^2)*( -5.0/1008.0);
    end
    if (jj > 4),
        e_laplacian(jj,jj-4) = 1/(dx_e^2)*( -5.0/1008.0);
    end
    if (jj < Dim_ele - 4),
        e_laplacian(jj,jj+5) = 1/(dx_e^2)*( +1.0/3150.0);
    end
    if (jj > 5),
        e_laplacian(jj,jj-5) = 1/(dx_e^2)*( +1.0/3150.0);
    end    
end
e_laplacian = sparse(e_laplacian);


%% OPERATORS IN THE NUCLEAR SPACE:
n_unit_nx = speye(Dim_nuc);
n_position = zeros(Dim_nuc);
n_gradient = zeros(Dim_nuc);
n_laplacian = zeros(Dim_nuc);

xh = -1;
for jj=1:Dim_nuc
    n_position(jj,jj) = (jj-1-xh)*dx_n;
end
n_position = sparse(n_position);

for jj=1:Dim_nuc
    n_gradient(jj,jj) = 0;
    if (jj < Dim_nuc),
        n_gradient(jj,jj+1) = 1/(dx_n)*( +5.0/6.0);
    end
    if (jj > 1),
        n_gradient(jj,jj-1) = 1/(dx_n)*( -5.0/6.0);
    end
    if (jj < Dim_nuc - 1),
        n_gradient(jj,jj+2) = 1/(dx_n)*( -5.0/21.0);
    end
    if (jj > 2),
        n_gradient(jj,jj-2) = 1/(dx_n)*( +5.0/21.0);
    end
    if (jj < Dim_nuc - 2),
        n_gradient(jj,jj+3) = 1/(dx_n)*( +5.0/84.0);
    end
    if (jj > 3),
        n_gradient(jj,jj-3) = 1/(dx_n)*( -5.0/84.0);
    end
    if (jj < Dim_nuc - 3),
        n_gradient(jj,jj+4) = 1/(dx_n)*( -5.0/504.0);
    end
    if (jj > 4),
        n_gradient(jj,jj-4) = 1/(dx_n)*( +5.0/504.0);
    end    
    if (jj < Dim_nuc - 4),
        n_gradient(jj,jj+5) = 1/(dx_n)*( +1.0/1260.0);
    end
    if (jj > 5),
        n_gradient(jj,jj-5) = 1/(dx_n)*( -1.0/1260.0);
    end      
end
n_gradient = sparse(n_gradient);

for jj=1:Dim_nuc
    n_laplacian(jj,jj) = 1/(dx_n^2)*( -73766.0/25200.0);
    if (jj < Dim_nuc),
        n_laplacian(jj,jj+1) = 1/(dx_n^2)*( 5.0/3.0);
    end
    if (jj > 1),
        n_laplacian(jj,jj-1) = 1/(dx_n^2)*( 5.0/3.0);
    end
    if (jj < Dim_nuc - 1),
        n_laplacian(jj,jj+2) = 1/(dx_n^2)*( -5.0/21.0);
    end
    if (jj > 2),
        n_laplacian(jj,jj-2) = 1/(dx_n^2)*( -5.0/21.0);
    end
    if (jj < Dim_nuc - 2),
        n_laplacian(jj,jj+3) = 1/(dx_n^2)*( 5.0/126.0);
    end
    if (jj > 3),
        n_laplacian(jj,jj-3) = 1/(dx_n^2)*( 5.0/126.0);
    end
    if (jj < Dim_nuc - 3),
        n_laplacian(jj,jj+4) = 1/(dx_n^2)*( -5.0/1008.0);
    end
    if (jj > 4),
        n_laplacian(jj,jj-4) = 1/(dx_n^2)*( -5.0/1008.0);
    end
    if (jj < Dim_nuc - 4),
        n_laplacian(jj,jj+5) = 1/(dx_n^2)*( +1.0/3150.0);
    end
    if (jj > 5),
        n_laplacian(jj,jj-5) = 1/(dx_n^2)*( +1.0/3150.0);
    end    
end
n_laplacian = sparse(n_laplacian);

xe_axis = full(diag(e_position));
xn_axis = full(diag(n_position));


%% DEFINING OPERATORS IN THE FULL SPACE:
e_position_s_1 = kron(e_position,kron(e_unit_nx,n_unit_nx));
e_position_s_2 = kron(e_unit_nx,kron(e_position,n_unit_nx));
n_position_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_position));

e_gradent_s_1 = kron(e_gradient,kron(e_unit_nx,n_unit_nx));
e_gradent_s_2 = kron(e_unit_nx,kron(e_gradient,n_unit_nx));
n_gradent_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_gradient));

e_lapl_s_1 = kron(e_laplacian,kron(e_unit_nx,n_unit_nx));
e_lapl_s_2 = kron(e_unit_nx,kron(e_laplacian,n_unit_nx));
n_lapl_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_laplacian));

clear e_position
clear n_position
%clear e_gradient
%clear n_gradient


%% DEFINING THE FULL HAMILTONIAN:
soft_en = 1; %0.4
soft_ee = 1; %2


Vn = 1./xn_axis;

Ve1n = zeros(Dim_nuc,Dim_ele,Dim_ele);
Ve2n = zeros(Dim_nuc,Dim_ele,Dim_ele);
Ve1e2 = zeros(Dim_nuc,Dim_ele,Dim_ele);
for kk = 1:Dim_ele
    for ii = 1:Dim_ele
        for jj = 1:Dim_nuc
            Ve1n(jj,ii,kk) = soft_coulomb(full(xe_axis(ii) - xn_axis(jj)/2),soft_en) + soft_coulomb(full(xe_axis(ii) + xn_axis(jj)/2),soft_en);
            Ve2n(jj,ii,kk) = soft_coulomb(full(xe_axis(kk) - xn_axis(jj)/2),soft_en) + soft_coulomb(full(xe_axis(kk) + xn_axis(jj)/2),soft_en);
            Ve1e2(jj,ii,kk) = soft_coulomb(full(xe_axis(kk) - xe_axis(ii)),soft_ee);
        end
    end
end
Ve1n = spdiags(Ve1n(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
Ve2n = spdiags(Ve2n(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
Ve1e2 = spdiags(Ve1e2(:),0,Dim_ele^2*Dim_nuc,Dim_ele^2*Dim_nuc);
Vn = kron(e_unit_nx,kron(e_unit_nx,sparse(diag(Vn))));


PES = Vn - Ve1n - Ve2n + Ve1e2;

% RK4 %
nvec = 4;
ah(2,1) = 0.5;
ah(3,2) = 0.5;
ah(4,3) = 1.0;
bh(1) = 1.0/6.0;
bh(2) = 1.0/3.0;
bh(3) = 1.0/3.0;
bh(4) = 1.0/6.0;
ch(1) = 0.0;
ch(2) = 0.5;
ch(3) = 0.5;
ch(4) = 1.0;

hamiltonian = e_cte_kinetic*e_lapl_s_1 + e_cte_kinetic*e_lapl_s_2 + ...
    n_cte_kinetic*n_lapl_s_1 + PES;

PES_aux = reshape(full(diag(PES)),Dim_nuc,Dim_ele,Dim_ele);

phi_ex = zeros(Dim_nuc,Dim_ele,Dim_ele);

phi_ini = load('phi_en_ini.txt');

gr = sum(sum(reshape(abs(phi_ini).^2,Dim_nuc,Dim_ele,Dim_ele),3),2)*dx_e^2;
%BOEIGSTATE_GR = reshape(load('BO_eigst_gr.txt'),Dim_nuc,Dim_ele,Dim_ele);
%BOEIGSTATE_EX1 = reshape(load('BO_eigst_ex1.txt'),Dim_nuc,Dim_ele,Dim_ele);
%BOEIGSTATE_EX2 = reshape(load('BO_eigst_ex2.txt'),Dim_nuc,Dim_ele,Dim_ele);
%BOEIGSTATE_EX3 = reshape(load('BO_eigst_ex3.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX4 = reshape(load('BO_eigst_ex4_ordered.txt'),Dim_nuc,Dim_ele,Dim_ele);
for R_aux = 1:Dim_nuc
    phi_ex(R_aux,:,:) = sqrt(gr(R_aux))*BOEIGSTATE_EX4(R_aux,:,:);
end
phi_ex = phi_ex./sqrt(sum(abs(phi_ex(:)).^2)*dx_e^2*dx_n);
phi_ex = 1.0*phi_ex + 0.0*reshape(abs(phi_ini),Dim_nuc,Dim_ele,Dim_ele);
phi_ex = phi_ex./sqrt(sum(abs(phi_ex(:)).^2)*dx_e^2*dx_n);

clear eig_e
clear eigv_e
clear PES_aux
clear e_unit_nx
clear n_unit_nx
clear PES_eigen_e
clear hamiltonian_aux
%clear e_laplacian
%clear n_laplacian

aux = phi_ex(:);
save phi_ex.txt aux -ascii

phi_ini = phi_ex(:);

BOEIGSTATE_GR = reshape(load('BO_eigst_gr.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX1 = reshape(load('BO_eigst_ex1.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX2 = reshape(load('BO_eigst_ex2.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX3 = reshape(load('BO_eigst_ex3_ordered.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX4 = reshape(load('BO_eigst_ex4_ordered.txt'),Dim_nuc,Dim_ele,Dim_ele);
BOEIGSTATE_EX5 = reshape(load('BO_eigst_ex5.txt'),Dim_nuc,Dim_ele,Dim_ele);