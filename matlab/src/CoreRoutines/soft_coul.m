%electron electron interaction
function [ potential ] = soft_coul( ax1, ax2, soft_ee)
%SOFT_COUL Returns the soft coulomb interaction with .1 softening
%   returns sign*1./sqrt((ax1-ax2).^2 +.1)
    potential = 1./sqrt((ax1-ax2).^2 + soft_ee);
