% Defining the Operators:
fprintf('\n \n'); fprintf('Defining the Operators... \n');

clear e_position_s_1;
clear e_position_s_2;
clear n_position_s_1;
%clear e_gradent_s_1;
%clear e_gradent_s_2;
%clear n_gradent_s_1;
clear e_lapl_s_1;
clear e_lapl_s_2;
clear n_lapl_s_1;
clear hamiltonian

%% OPERATORS IN THE ELECTRONIC SPACE:
e_unit_nx = speye(Dim_ele);
e_position = zeros(Dim_ele);
e_gradient = zeros(Dim_ele);
e_laplacian = zeros(Dim_ele);

xh = (Dim_ele-1)/2;
for jj=1:Dim_ele
    e_position(jj,jj) = (jj-1-xh)*dx_e;
end
e_position = sparse(e_position);

for jj=1:Dim_ele
    e_gradient(jj,jj) = 0;
    if (jj < Dim_ele),
        e_gradient(jj,jj+1) = 1/(dx_e)*( +5.0/6.0);
    end
    if (jj > 1),
        e_gradient(jj,jj-1) = 1/(dx_e)*( -5.0/6.0);
    end
    if (jj < Dim_ele - 1),
        e_gradient(jj,jj+2) = 1/(dx_e)*( -5.0/21.0);
    end
    if (jj > 2),
        e_gradient(jj,jj-2) = 1/(dx_e)*( +5.0/21.0);
    end
    if (jj < Dim_ele - 2),
        e_gradient(jj,jj+3) = 1/(dx_e)*( +5.0/84.0);
    end
    if (jj > 3),
        e_gradient(jj,jj-3) = 1/(dx_e)*( -5.0/84.0);
    end
    if (jj < Dim_ele - 3),
        e_gradient(jj,jj+4) = 1/(dx_e)*( -5.0/504.0);
    end
    if (jj > 4),
        e_gradient(jj,jj-4) = 1/(dx_e)*( +5.0/504.0);
    end    
    if (jj < Dim_ele - 4),
        e_gradient(jj,jj+5) = 1/(dx_e)*( +1.0/1260.0);
    end
    if (jj > 5),
        e_gradient(jj,jj-5) = 1/(dx_e)*( -1.0/1260.0);
    end      
end
e_gradient = sparse(e_gradient);

for jj=1:Dim_ele
    e_laplacian(jj,jj) = 1/(dx_e^2)*( -73766.0/25200.0);
    if (jj < Dim_ele),
        e_laplacian(jj,jj+1) = 1/(dx_e^2)*( 5.0/3.0);
    end
    if (jj > 1),
        e_laplacian(jj,jj-1) = 1/(dx_e^2)*( 5.0/3.0);
    end
    if (jj < Dim_ele - 1),
        e_laplacian(jj,jj+2) = 1/(dx_e^2)*( -5.0/21.0);
    end
    if (jj > 2),
        e_laplacian(jj,jj-2) = 1/(dx_e^2)*( -5.0/21.0);
    end
    if (jj < Dim_ele - 2),
        e_laplacian(jj,jj+3) = 1/(dx_e^2)*( 5.0/126.0);
    end
    if (jj > 3),
        e_laplacian(jj,jj-3) = 1/(dx_e^2)*( 5.0/126.0);
    end
    if (jj < Dim_ele - 3),
        e_laplacian(jj,jj+4) = 1/(dx_e^2)*( -5.0/1008.0);
    end
    if (jj > 4),
        e_laplacian(jj,jj-4) = 1/(dx_e^2)*( -5.0/1008.0);
    end
    if (jj < Dim_ele - 4),
        e_laplacian(jj,jj+5) = 1/(dx_e^2)*( +1.0/3150.0);
    end
    if (jj > 5),
        e_laplacian(jj,jj-5) = 1/(dx_e^2)*( +1.0/3150.0);
    end    
end
e_laplacian = sparse(e_laplacian);

e_T = e_cte_kinetic*e_laplacian;

%% OPERATORS IN THE NUCLEAR SPACE:
n_unit_nx = speye(Dim_nuc);
n_position = zeros(Dim_nuc);
n_gradient = zeros(Dim_nuc);
n_laplacian = zeros(Dim_nuc);

%xh = (Dim_nuc-1)/2;
for jj=1:Dim_nuc
    n_position(jj,jj) = (jj)*dx_n;
end
n_position = sparse(n_position);

for jj=1:Dim_nuc
    n_gradient(jj,jj) = 0;
    if (jj < Dim_nuc),
        n_gradient(jj,jj+1) = 1/(dx_n)*( +5.0/6.0);
    end
    if (jj > 1),
        n_gradient(jj,jj-1) = 1/(dx_n)*( -5.0/6.0);
    end
    if (jj < Dim_nuc - 1),
        n_gradient(jj,jj+2) = 1/(dx_n)*( -5.0/21.0);
    end
    if (jj > 2),
        n_gradient(jj,jj-2) = 1/(dx_n)*( +5.0/21.0);
    end
    if (jj < Dim_nuc - 2),
        n_gradient(jj,jj+3) = 1/(dx_n)*( +5.0/84.0);
    end
    if (jj > 3),
        n_gradient(jj,jj-3) = 1/(dx_n)*( -5.0/84.0);
    end
    if (jj < Dim_nuc - 3),
        n_gradient(jj,jj+4) = 1/(dx_n)*( -5.0/504.0);
    end
    if (jj > 4),
        n_gradient(jj,jj-4) = 1/(dx_n)*( +5.0/504.0);
    end    
    if (jj < Dim_nuc - 4),
        n_gradient(jj,jj+5) = 1/(dx_n)*( +1.0/1260.0);
    end
    if (jj > 5),
        n_gradient(jj,jj-5) = 1/(dx_n)*( -1.0/1260.0);
    end      
end
n_gradient = sparse(n_gradient);

for jj=1:Dim_nuc
    n_laplacian(jj,jj) = 1/(dx_n^2)*( -73766.0/25200.0);
    if (jj < Dim_nuc),
        n_laplacian(jj,jj+1) = 1/(dx_n^2)*( 5.0/3.0);
    end
    if (jj > 1),
        n_laplacian(jj,jj-1) = 1/(dx_n^2)*( 5.0/3.0);
    end
    if (jj < Dim_nuc - 1),
        n_laplacian(jj,jj+2) = 1/(dx_n^2)*( -5.0/21.0);
    end
    if (jj > 2),
        n_laplacian(jj,jj-2) = 1/(dx_n^2)*( -5.0/21.0);
    end
    if (jj < Dim_nuc - 2),
        n_laplacian(jj,jj+3) = 1/(dx_n^2)*( 5.0/126.0);
    end
    if (jj > 3),
        n_laplacian(jj,jj-3) = 1/(dx_n^2)*( 5.0/126.0);
    end
    if (jj < Dim_nuc - 3),
        n_laplacian(jj,jj+4) = 1/(dx_n^2)*( -5.0/1008.0);
    end
    if (jj > 4),
        n_laplacian(jj,jj-4) = 1/(dx_n^2)*( -5.0/1008.0);
    end
    if (jj < Dim_nuc - 4),
        n_laplacian(jj,jj+5) = 1/(dx_n^2)*( +1.0/3150.0);
    end
    if (jj > 5),
        n_laplacian(jj,jj-5) = 1/(dx_n^2)*( +1.0/3150.0);
    end    
end
n_laplacian = sparse(n_laplacian);
n_T = n_cte_kinetic*n_laplacian;

xe_axis = full(diag(e_position));
xn_axis = full(diag(n_position));



%% DEFINING OPERATORS IN THE FULL SPACE:
e_position_s_1 = kron(e_position,kron(e_unit_nx,n_unit_nx));
e_position_s_2 = kron(e_unit_nx,kron(e_position,n_unit_nx));
n_position_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_position));

e_gradent_s_1 = kron(e_gradient,kron(e_unit_nx,n_unit_nx));
e_gradent_s_2 = kron(e_unit_nx,kron(e_gradient,n_unit_nx));
n_gradent_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_gradient));

e_lapl_s_1 = kron(e_laplacian,kron(e_unit_nx,n_unit_nx));
e_lapl_s_2 = kron(e_unit_nx,kron(e_laplacian,n_unit_nx));
n_lapl_s_1 = kron(e_unit_nx,kron(e_unit_nx,n_laplacian));

Vee = zeros(Dim_ele, Dim_ele);
Ven = zeros(Dim_nuc, Dim_ele);
for i=1:Dim_ele
    for j=1:Dim_ele
        Vee(i,j) = soft_coulomb(xe_axis(i) - xe_axis(j),soft_ee);
    end
    for j=1:Dim_nuc
        Ven(j,i) = -soft_coulomb(xe_axis(i) - (m1/(m1+m2))*xn_axis(j),soft_en) - soft_coulomb(xe_axis(i) - (-1*m2/(m1+m2))*xn_axis(j),soft_en);
    end
end
Vnn = 1./xn_axis;
%Vn = kron(e_unit_nx,kron(e_unit_nx,sparse(diag(Vnn))));
