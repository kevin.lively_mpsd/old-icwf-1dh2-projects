% SAVING DATA %
time_index = time_index + 1;
save(strcat('./dump/',int2str(init_N_traj),'/N_traj_',int2str(time_index)), 'N_traj', '-ascii');
if(unmapped && (relax==false))
    phi_e2 = phi_e1;
end
%% TRAJECTORY POSITIONS AND VELOCITIES
str = strcat('./dump/',int2str(init_N_traj),'/xe1_IPW',int2str(time_index),'.txt'); xe1_cond = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/xe2_IPW',int2str(time_index),'.txt'); xe2_cond = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/xn_IPW',int2str(time_index),'.txt'); xn_cond = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/ve1_IPW',int2str(time_index),'.txt'); ve1_cond = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/ve2_IPW',int2str(time_index),'.txt'); ve2_cond = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/vn_IPW',int2str(time_index),'.txt'); vn_cond = fopen(str,'w');
if(run_gpu==true)
    fwrite(xe1_cond,gather(xe1),'double');
    fwrite(xe2_cond,gather(xe2),'double');
    fwrite(xn_cond,gather(xn),'double');
    if(t==1 || reTime)
        fwrite(ve1_cond,gather(ve1),'double');
        fwrite(ve2_cond,gather(ve2),'double');
        fwrite(vn_cond,gather(vn),'double');
    else
        tmp = (1/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1);
        fwrite(ve1_cond,gather(tmp),'double');
        tmp = (1/6)*(v1_e2 + 2*v2_e2 + 2*v3_e2 + v4_e2);
        fwrite(ve1_cond,gather(tmp),'double');
        tmp = (1/6)*(v1_n + 2*v2_n + 2*v3_n + v4_n);
        fwrite(vn_cond,gather(tmp),'double');
    end
else
    fwrite(xe1_cond,xe1,'double');
    fwrite(xe2_cond,xe2,'double');
    fwrite(xn_cond,xn,'double');
    if(t == 1 || reTime)
        fwrite(ve1_cond,ve1,'double');
        fwrite(ve2_cond,ve2,'double');
        fwrite(vn_cond,vn,'double');
    else
        tmp = (1/6)*(v1_e1 + 2*v2_e1 + 2*v3_e1 + v4_e1);
        fwrite(ve1_cond,tmp,'double');
        tmp = (1/6)*(v1_e2 + 2*v2_e2 + 2*v3_e2 + v4_e2);
        fwrite(ve1_cond,tmp,'double');
        tmp = (1/6)*(v1_n + 2*v2_n + 2*v3_n + v4_n);
        fwrite(vn_cond,tmp,'double');
    end
end
fclose(xe1_cond);
fclose(xe2_cond);
fclose(xn_cond);
fclose(ve1_cond);
fclose(ve2_cond);
fclose(vn_cond);

%% conditional wave functions and C
str = strcat('./dump/',int2str(init_N_traj),'/re1_',int2str(time_index),'.txt'); CWF_re1 = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/ie1_',int2str(time_index),'.txt'); CWF_ie1 = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/re2_',int2str(time_index),'.txt'); CWF_re2 = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/ie2_',int2str(time_index),'.txt'); CWF_ie2 = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/rn_',int2str(time_index),'.txt'); CWF_rn = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/in_',int2str(time_index),'.txt'); CWF_in = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/rC_',int2str(time_index),'.txt'); CWF_rC = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/iC_',int2str(time_index),'.txt'); CWF_iC = fopen(str,'w');
if(run_gpu==true)
    fwrite(CWF_rn,real(gather(phi_n)),'double');
    fwrite(CWF_in,imag(gather(phi_n)),'double');
    fwrite(CWF_re1,real(gather(phi_e1)),'double');
    fwrite(CWF_ie1,imag(gather(phi_e1)),'double');
    fwrite(CWF_re2,real(gather(phi_e2)),'double');
    fwrite(CWF_ie2,imag(gather(phi_e2)),'double');
    fwrite(CWF_rC,real(gather(C)),'double');
    fwrite(CWF_iC,imag(gather(C)),'double');
else
    fwrite(CWF_rn,real(phi_n),'double');
    fwrite(CWF_in,imag(phi_n),'double');
    fwrite(CWF_re1,real(phi_e1),'double');
    fwrite(CWF_ie1,imag(phi_e1),'double');
    fwrite(CWF_re2,real(phi_e2),'double');
    fwrite(CWF_ie2,imag(phi_e2),'double');
    fwrite(CWF_rC,real(C),'double');
    fwrite(CWF_iC,imag(C),'double');
end
fclose(CWF_rn);
fclose(CWF_in);
fclose(CWF_re1);
fclose(CWF_ie1);
fclose(CWF_re2);
fclose(CWF_ie2);
fclose(CWF_rC);
fclose(CWF_iC);


%% REDUCED NUCLEAR/ELECTRONIC DENSITIES 
phi_ee1 = phi_e1'*phi_e1*dx_e;
phi_ee2 = phi_e2'*phi_e2*dx_e;
phi_nn = phi_n'*phi_n*dx_n;
if(run_gpu==true)
    red_n = gpuArray(zeros(Dim_nuc,1));
    red_e = gpuArray(zeros(Dim_ele,1));
else
    red_n = zeros(Dim_nuc,1);
    red_e = zeros(Dim_ele,1);
end

for i = 1:N_traj
    red_n = red_n + C(i)*((conj(phi_n).*repmat(phi_n(:,i),1,N_traj)))*(conj(C).*phi_ee1(:,i).*phi_ee2(:,i));
    red_e = red_e + C(i)*((conj(phi_e1).*repmat(phi_e1(:,i),1,N_traj)))*(conj(C).*phi_ee2(:,i).*phi_nn(:,i));
end
str = strcat('./dump/',int2str(init_N_traj),'/red_n_IPW',int2str(time_index),'.txt'); CWF_n = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/red_e_IPW',int2str(time_index),'.txt'); CWF_e = fopen(str,'w');
str = strcat('./dump/',int2str(init_N_traj),'/rho_IPW',int2str(time_index),'.txt'); CWF_rho = fopen(str,'w');
if(run_gpu==true)
    fwrite(CWF_n,gather(red_n),'double');
    fwrite(CWF_e,gather(red_e),'double');
    if(mapped)
        fwrite(CWF_rho,gather(rho),'double');
    end
else
    fwrite(CWF_n,red_n,'double');
    fwrite(CWF_e,red_e,'double');
    if(mapped)
        fwrite(CWF_rho,rho,'double');
    end
end
fclose(CWF_n);
fclose(CWF_e);
fclose(CWF_rho);


%% REDUCED ADIABATIC QUANTITIES
% if(run_gpu==true)
%     gr_comp = zeros(Dim_nuc,1,'gpuArray');
%     ex1_comp = zeros(Dim_nuc,1,'gpuArray'); 
% else
%     gr_comp = zeros(Dim_nuc,1);
%     ex1_comp = zeros(Dim_nuc,1);
% end
% for i =1:Dim_nuc
%     gr_comp(i) = C.'*((phi_n(i,:).').*diag(((squeeze(BOEIGSTATE_GR(i,:,:))*phi_e2).')*phi_e1)*dx_e^2);
%     ex1_comp(i) = C.'*((phi_n(i,:).').*diag(((squeeze(BOEIGSTATE_EX1(i,:,:))*phi_e2).')*phi_e1)*dx_e^2);
% end
% str = strcat('./dump/',int2str(init_N_traj),'/gr_comp_IPW',int2str(time_index),'.txt'); CWF_n = fopen(str,'w');
% if(run_gpu==true)
%     fwrite(CWF_n,gather(abs(gr_comp).^2),'double');
% else
%     fwrite(CWF_n,abs(gr_comp).^2,'double');
% end
% fclose(CWF_n);
% str = strcat('./dump/',int2str(init_N_traj),'/ex1_comp_IPW',int2str(time_index),'.txt'); CWF_n = fopen(str,'w');
% if(run_gpu==true)
%     fwrite(CWF_n,gather(abs(ex1_comp).^2),'double');
% else
%     fwrite(CWF_n,abs(ex1_comp).^2,'double');
% end
% fclose(CWF_n);
