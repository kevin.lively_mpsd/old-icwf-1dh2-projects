%electron nuclear interaction on the fly for the nuclei
function [ pot ] = soft_nuc_otf(e_pos, Dim_nuc,xn_axis,N_traj)
%SOFT_NUC Takes electron axis and nuclear axis and returns potential in com
%frame
%   returns -1/sqrt((e_ax + M2/mu * R_ax).^2 _+.1) - 1./sqrt((e_ax - M1/mu *
%   R_ax).^2 +.1)
    n_mass = 1836;
    pot = zeros(Dim_nuc,N_traj);
    for i=1:size(e_pos)
        pot(:,i) = -1./sqrt((e_pos(i) + n_mass/(2*n_mass) * xn_axis).^2 +1) - 1./sqrt((e_pos(i) - n_mass/(2*n_mass) * xn_axis).^2 +1);
    end
