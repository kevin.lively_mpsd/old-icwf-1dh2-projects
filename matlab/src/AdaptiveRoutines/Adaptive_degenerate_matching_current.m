%% Adaptive Degenerate Matching Pursuit Algorithm
% Adopted from Habershon, JCP 136 014109 (2012)
% Whereas the Adaptive Matching Pursuit Algorithm projects the entire basis
%   onto the current wavefunction and selects those which are most useful,
%   then subtracts them from consideration for the next loop in a 'greedy' pursuit, this
%   algorithm projects the entire basis onto the current wavefunction and
%   selects the most useful ones until the wavefunction is reconstructed.
%   
%   This algorithm should be run after initialization and then every few 
%   timesteps. Since the active basis may not be the best subset of the
%   full basis to construct the wavefunction after a few timesteps, this
%   algorithm adapts the active subset such that the full wavefunction is
%   being integrated with the 'best' representing active subset. The
%   overlap matrix for this active subset is still singular, but with a
%   Moore-Penrose Pseudoinverse at tolerance 1E-4, that is, doing an SVD of
%   the overlap matrix and tossing singular values less than 1E-4, then
%   inverting, the propagation is still stable. 
%   

fprintf('Adapting. . . ')

%% This code would come after a time step.

gamma_ad = 1E-4; %gamma adaptive, tolerance for accuracy in 1-<psi_l|psi_T>

% Save information necessary to construct wave function
C_old = C;
active_old = active;

% Construct full overlap matrix for usage through out.
S_full = ( ((phi_e1'*phi_e1*dx_e).^2) .* (phi_n'*phi_n*dx_n) );

%With absorbing boundary conditions, the norm is dynamic.
norm = sqrt(real(C_old'*S_full(active_old,active_old)*C_old));

%get overlaps   <psi_j| C_i|psi_i> = S_ji C_i  // sum over i
overlaps = S_full(:,active_old)*C_old;

%Now we assume that all the basis is inactive
active = [];
C = [];

l=0; %Iteration counter
eta = 1; %Error

cycle = 0; %Counter for who many times the whole basis has been iterated through

%   Exits when converging or running through the whole basis 23 times,
%       adding 50 slices every cycle from the third on.
while(eta>gamma_ad && l<N_traj); 
    
    l=l+1;
    % Find the inactive basis function which has the greatest overlap with
    %  the target wavefunction, implicitly constructed from C_old, active_old
    %  and S_full
    [mval, ind] = max(abs(overlaps));
    
    %Add this basis index to the active set.
    active = [active ind];
    
    %Invert the active overlap matrix . . .
    S_active_inv = pinv(S_full(active,active),10^tol);
    
    % in order to determine new active coefficients.
    %   C_i = sum_j=1^l Sinv_{ij} <phi_j | C_old_{a}|phi_{a}>  // sum over a
    C = S_active_inv*(S_full(active,active_old)*C_old);

    %set norm to current wf norm.
    C = C./sqrt(C'*S_full(active,active)*C);
    C = norm*C;
    
    %Check for convergence
    %   eta =| sqrt(<psi_T | psi_T>) - sqrt(<psi_l | psi_T>) | Where |psi_l> is the current iteration's wf 
    %eta = abs(norm-sqrt(C'*(S_full(active,active_old)*C_old)));
    eta = abs(C_old'*S_full(active_old,active_old)*C_old - 2*real(C_old'*S_full(active_old,active)*C) + C'*S_full(active,active)*C);
    
    %Skip step 5b, allowing degeneracy for the sake of convergence. 
    overlaps = abs(S_full(:,active_old)*C_old - S_full(:,active)*C);
    
    %Supress the currently active bases from further consideration
    overlaps(active) = 0;
    
    %If we've run through the whole basis, explicitly construct the current
    %   exact wavefunction, and find the expansion coefficients thorugh
    %   direct inversion, then restart the adaptive match to find a more
    %   ideal basis. 
    %Furthermore, if we've run through the whole basis more than once, add
    %   slice bases.
    if(l==N_traj && eta>gamma_ad)
        fprintf('Entire Basis used in adaptive matching without convergence! Recreating Wavefunction.\n')

        cycle=cycle + 1;
        
        eta=1;
        l=0;
        active=[];

        %Explicitly construct the current wavefunction
        phi_target = zeros(Dim_nuc*Dim_ele^2,1);
        for i=1:size(active_old,2)
            phi_target = phi_target + C_old(i)*kron(phi_e1(:,active_old(i)),kron(phi_e1(:,active_old(i)),phi_n(:,active_old(i))));
        end 
        
        %Directly invert from the overlap of the basis with the current wavefunction.
        M = (phi_n'*phi_n).*(phi_e1'*phi_e1).*(phi_e1'*phi_e1)*dx_n*dx_e*dx_e;
        G = zeros(N_traj,1);
        for alpha = 1:N_traj
            G(alpha) = phi_n(:,alpha)'*(reshape((reshape(phi_target,Dim_nuc*Dim_ele,Dim_ele)*conj(phi_e1(:,alpha))),Dim_nuc,Dim_ele)*conj(phi_e1(:,alpha)))*dx_n*dx_e*dx_e;
        end
        
        %Only use bases that have more overlap than 0.01, in order to
        %   stabilize the pinv for huge N_traj.
        list = 1:N_traj;
        active_old = list(abs(G)>0.01);
        C_old = pinv(M(active_old,active_old),10^tol)*G(active_old);
        C_old = C_old./sqrt(C_old'*M(active_old,active_old)*C_old);
        C_old = norm*C_old;
        
        %If this is the second time or more that the current wavefunction has exhausted
        %   the basis, replace minimal overlap basis elements with slices
        if(cycle>=2)
            red_e = squeeze(sum(sum(reshape(abs(phi_target).^2,Dim_nuc,Dim_ele,Dim_ele),1),2))*dx_e*dx_n;
            red_n = squeeze(sum(sum(reshape(abs(phi_target).^2,Dim_nuc,Dim_ele,Dim_ele),3),2))*dx_e^2;
            list = 1:N_traj;
            
            %Find the indices of the low overlap basis elements
            remaining = setdiff(list,active_old);
            N_remaining = size(remaining,2);
      
            for i=1:N_remaining
                %Resample these indices
                alpha = remaining(i);
                
                %Make sure not to place slices outside box
                attempt =true;
                while(attempt==true)
                    mesh_e1(alpha) = mcsampling(red_e,1,threshold);
                    mesh_e2(alpha) = mcsampling(red_e,1,threshold);
                    mesh_n(alpha) = mcsampling(red_n,1,threshold);
                    if( (mesh_e1<Dim_ele) && (mesh_e1>0) && (mesh_e2<Dim_ele) && (mesh_e2>0) && (mesh_n<Dim_nuc) && (mesh_n>0))
                        attempt=false;
                    end
                end
                rand_vals = rand(1,1);
                xe1(alpha) = (mesh_e1(alpha)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
                rand_vals = rand(1,1);
                xe2(alpha) = (mesh_e2(alpha)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
                rand_vals = rand(1,1);
                xn(alpha) = (mesh_n(alpha)-1)*dx_n + xn_axis(1) + (rand_vals - 0.5)*dx_n;

                phi_e1(:,alpha) = squeeze(phi_target(mesh_n(alpha),:,mesh_e2(alpha)));
                %phi_e2(:,alpha) = squeeze(phi_target(mesh_n(alpha),mesh_e1(alpha),:));
                phi_n(:,alpha) = phi_target(:,mesh_e1(alpha),mesh_e2(alpha));

                phi_e1(:,alpha) = phi_e1(:,alpha)/sqrt(sum(abs(phi_e1(:,alpha)).^2)*dx_e);
                %phi_e2(:,alpha) = phi_e2(:,alpha)/sqrt(sum(abs(phi_e2(:,alpha)).^2)*dx_e);
                phi_n(:,alpha) = phi_n(:,alpha)/sqrt(sum(abs(phi_n(:,alpha)).^2)*dx_n);
            end
            %set the reset particle positions to these new sampled values
            xe1_old = xe1; xe2_old = xe2; xn_old = xn;
            
            %Reconstruct S_full
            S_full = ( ((phi_e1'*phi_e1*dx_e).^2) .* (phi_n'*phi_n*dx_n) );
            
        end
        
        %If we exhaust the basis past a second time, add 50 more slices
        if(cycle>2)
            fprintf('Current Wavefunction has exhausted the basis more than twice!\nAdding 50 more slices . . .');
            for i=(N_traj + 1):(N_traj + 50)
                alpha = i;
                
                %Make sure not to place slices outside box
                attempt = true;
                while(attempt==true)
                    mesh_e1(alpha) = mcsampling(red_e,1,threshold);
                    mesh_e2(alpha) = mcsampling(red_e,1,threshold);
                    mesh_n(alpha) = mcsampling(red_n,1,threshold);
                    if( (mesh_e1(alpha)<Dim_ele) && (mesh_e1(alpha)>0) && (mesh_e2(alpha)<Dim_ele) && (mesh_e2(alpha)>0) && (mesh_n(alpha)<Dim_nuc) && (mesh_n(alpha)>0))
                        attempt=false;
                    end
                end
                rand_vals = rand(1,1);
                xe1(alpha) = (mesh_e1(alpha)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
                rand_vals = rand(1,1);
                xe2(alpha) = (mesh_e2(alpha)-1)*dx_e + xe_axis(1) + (rand_vals - 0.5)*dx_e;
                rand_vals = rand(1,1);
                xn(alpha) = (mesh_n(alpha)-1)*dx_n + xn_axis(1) + (rand_vals - 0.5)*dx_n;

                phi_e1(:,alpha) = squeeze(phi_target(mesh_n(alpha),:,mesh_e2(alpha)));
                %phi_e2(:,alpha) = squeeze(phi_target(mesh_n(alpha),mesh_e1(alpha),:));
                phi_n(:,alpha) = phi_target(:,mesh_e1(alpha),mesh_e2(alpha));

                phi_e1(:,alpha) = phi_e1(:,alpha)/sqrt(sum(abs(phi_e1(:,alpha)).^2)*dx_e);
                %phi_e2(:,alpha) = phi_e2(:,alpha)/sqrt(sum(abs(phi_e2(:,alpha)).^2)*dx_e);
                phi_n(:,alpha) = phi_n(:,alpha)/sqrt(sum(abs(phi_n(:,alpha)).^2)*dx_n);
            end
            %set the reset particle positions to these new sampled values
            xe1_old = xe1; xe2_old = xe2; xn_old = xn;
            
            %Increase the N_traj counter
            N_traj = N_traj + 50;
            
            %Appropriately modify the propagation operators
            eta = 0.01;
            W=zeros(size(xe_axis));
            L_ind = 30;
            W(end-L_ind:end) = -1i*eta*(xe_axis(end-L_ind:end) - xe_axis(end-L_ind)).^2;
            W(1:L_ind+1) = -1i*eta*(xe_axis(1:L_ind+1) - xe_axis(L_ind+1)).^2;

            W = repmat(W,1,N_traj);

            Wn=zeros(size(xn_axis));
            L_ind = 50;
            Wn(end-L_ind:end) = -1i*eta*(xn_axis(end-L_ind:end) - xn_axis(end-L_ind)).^2;

            Wn = repmat(Wn,1,N_traj);

            %Reconstruct S_full
            S_full = ( ((phi_e1'*phi_e1*dx_e).^2) .* (phi_n'*phi_n*dx_n) );
        end
        
        if(cycle > 23)
            fprintf('Despite adding 1000 slices, this will not converge! Crashing now.\n')
            exit
        end
        
        %Get the basis with the target wavefunction.
        overlaps = S_full(:,active_old)*C_old;
        
     end
end
l
remaining = setdiff(1:N_traj,active);


active_phi_n = phi_n(:,active)*C;

% for i=1:size(remaining,2)
%     % Find the inactive basis function which has the greatest overlap with
%     %  the target wavefunction, implicitly constructed from C_old, active_old
%     %  and S_full
%     %[mval, ind] = max(abs(overlaps));
%     ind = remaining(i);
% 
%     
%     if(abs(active_phi_n'*(phi_n(:,remaining(i)))*dx_n) <0.1) %  minimal overlap
%     
%         %Add this basis index to the active set.
%         active = [active ind];
%         
%         C = [C.' 0].';
%     end
%     if(l==N_traj)
%         break
%     end
% end
N_active = size(active,2);
pool = remaining(1:N_traj-N_active);
fprintf('N_active = %f\n',N_active);
